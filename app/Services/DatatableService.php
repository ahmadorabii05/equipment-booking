<?php

namespace App\Services;


use App\Interfaces\DatatableInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;

class DatatableService extends ActionService implements DatatableInterface
{
    /**
     * Regions datatable data
     *
     */
    public static function cityDatatable($regions): array
    {
        $data = [];
        foreach ($regions as $region) {
            $data[] = [
                'id' => $region->id,
                'name' => $region->translate('en')->name ?? '',
                'users_count' => $region->users_count . ' users',
                'lat' => $region->latitude ?? '',
                'lng' => $region->longitude ?? '',
                'users' => $region->users ?? [],
                'center' => $region->center ? self::getGoogleMapUrl($region->center) : '',
                'created_at' => Date::parse($region->created_at)->format('Y-m-d'),
                'actions' => $region
            ];
        }
        $response = self::initResponse($regions);
        $response['data'] = $data;
        return $response;
    }
    public static function categoriesDatatable($categories): array
    {
        $data = [];
        foreach ($categories as $category) {
           $image = $category->image ? (new ActionService)->getImageUrl($category->image) : asset('custom/images.jpg');
            $data[] = [
                'id' => $category->id,
                'image' => $image,
                'status' => $category->status,
                'created_at' => Carbon::parse($category->created_at)->format('Y-m-d'),
                'name' => $category->name ?? '',
                'actions' => (new ActionService)->getItemActions($category, 'category')
            ];
        }
        $response = self::initResponse($categories);
        $response['data'] = $data;
        return $response;
    }

    public static function ordersDatatable($orders): array
    {
        $data = [];
        foreach ($orders as $order) {
            $data[] = [
                'id' => $order->id,
                'code' => $order->code,
                'user' => $order->user ? $order->user->name : '',
                'total_vat' => $order->total_vat ?: '',
                'total_price' => $order->total_price ?: '',
                'is_paid' => $order->is_paid ? 'is paid' : 'is not paid',
                'created_at' => Date::parse($order->created_at)->format('Y-m-d'),
                'status' => $order->status,
            ];
        }

        $response = self::initResponse($orders);

        $response['data'] = $data;
        return $response;
    }
    public static function supplierOrdersDatatable($orders , $id): array
    {
        $data = [];
        foreach ($orders as $order) {
            $data[] = [
                'id' => $order->orderDetails->id,
                'code' => $order->orderDetails->code,
                'user' => $order->client ? $order->client->name : '',
                'total_vat' => $order->total_vat ?: '',
                'total_price' => $order->total_price ?: '',
                'is_paid' => $order->orderDetails->is_paid ? 'is paid' : 'is not paid',
                'created_at' => Date::parse($order->created_at)->format('Y-m-d H:i'),
                'status' => $order->status,
            ];
        }
        $response = self::initResponse($orders);

        $response['data'] = $data;
        return $response;
    }

    public static function equipmentDatatable($equipments): array
    {
        $data = [];
        foreach ($equipments as $equipment) {
            $data[] = [
                'id' => $equipment->id,
                'image' => $equipment->iamges&&$equipment->iamges[0] ? (new ActionService)->getImageUrl($equipment->iamges[0] , $equipment->id) :
                     'no image',
                'created_at' => Date::parse($equipment->created_at)->format('Y-m-d'),
                'status' => $equipment->status,
                'name' => $equipment->name,
                'count' => $equipment->count,
                'price' => $equipment->price,
                'actions' => (new ActionService)->getEquipmentActions($equipment, 'equipment')
            ];
        }
        $response = self::initResponse($equipments);
        $response['data'] = $data;
        return $response;
    }

    /**
     * Users datatable data
     *
     */
    public static function userDatatable($users): array
    {
        $data = [];
        foreach ($users as $user) {
            $data[] = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'created_at' => Date::parse($user->created_at)->format('Y-m-d'),
                'status' => $user->status,
                'actions' => (new ActionService)->getItemActions($user, 'user')
            ];
        }
        $response = self::initResponse($users);
        $response['data'] = $data;
        return $response;
    }


    /**
     * Users datatable data
     *
     */
    public static function shiftsDatatable($shifts): array
    {
        $data = [];
        foreach ($shifts as $shift) {
            $data[] = [
                'id' => $shift->id,
                'start_date' => $shift->start_date,
                'end_date' => $shift->end_date,
                'shift_number' => $shift->shift_number,
                'shift_count' => $shift->shift_count,
                'actions' => (new ActionService)->getItemActions($shift, 'shift')
            ];
        }
        $response = self::initResponse($shifts);
        $response['data'] = $data;
        return $response;
    }


    /**
     * Post datatable data
     *
     */
    public static function adminDatatable($admins): array
    {
        $data = [];
        foreach ($admins as $admin) {
            $imageUrl = $admin->avatar ? storageImage($admin->avatar) : asset('assets/media/svg/icons/General/User.svg');
            $data[] = [
                'id' => $admin->id,
                'image' => (new ActionService)->getImageUrl($imageUrl, $admin->id),
                'name' => $admin->name . ' (' . ($admin->email) . ')',
                'email' => $admin->email,
                'role' => $admin->roles()->first()->name ?? '',
                'created_at' => Date::parse($admin->created_at)->format('Y-m-d'),
                'status' => $admin->status,
                'actions' => (new ActionService)->getItemActions($admin, 'admin')
            ];
        }
        $response = self::initResponse($admins);
        $response['data'] = $data;
        return $response;
    }


    public static function contactsDatatable($contacts): array
    {
        $data = [];
        foreach ($contacts as $contact) {
            $data[] = [
                'id' => $contact->id,
                'name' => $contact->name,
                'email' => $contact->email,
                'phone' => $contact->phone_number,
                'message' => Str::limit($contact->message, 50, $end = '....'),
                'created_at' => Date::parse($contact->created_at)->format('Y-m-d'),
                'actions' => (new ActionService)->getItemActions($contact, 'contact')
            ];
        }
        $response = self::initResponse($contacts);
        $response['data'] = $data;
        return $response;
    }

    /**
     */
    public static function initResponse($resource): array
    {
        return [
            "meta" => [
                "page" => $resource->currentPage(),
                "pages" => $resource->lastPage(),
                "perpage" => $resource->perPage(),
                "total" => $resource->total(),
                "sort" => request()->get('sort')['sort'] ?? 0,
                "field" => request()->get('sort')['field'] ?? ''
            ]
        ];
    }


    public static function getGoogleMapUrl($resource): string
    {
        return "https://www.google.com/maps/search/?api=1&query=" . $resource[0] . "," . $resource[1];
    }
}
