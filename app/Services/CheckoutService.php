<?php

namespace App\Services;


use App\Http\Traits\CartTrait;
use App\Http\Traits\OrderTrait;
use App\Models\Admin;
use App\Models\Cart;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Log;

class CheckoutService
{
    use CartTrait, OrderTrait;

    /**
     * @param Cart $cart
     * @param int $paymentMethod
     * @param int|null $userId
     * @return OrderDetail
     * @throws \Exception
     */
    public static function proceedToCheckout(Cart $cart,  int $paymentMethod = OrderDetail::STATUS_PAYMENT_URWAY ,int $userId= null): OrderDetail
    {
        $data = [
            'user_id' => $userId,
            'payment_method' => $paymentMethod
        ];

        $orderDetail = self::createOrderDetails($data);
        $items = $cart->items()->get()->groupBy('admin_id');

        foreach ($items as $adminId => $vendorItems) {

            // Create an order for each vendor
            $order = self::createOrder($adminId, $userId, $orderDetail->id);

            // Create a new cart for mode
            $status = 1; // Assuming 1 represents "active" status
            $orderCart = self::createCart($order->id, $status);
            $orderCart->save();

            // Move items from the original cart to the order cart
            $vendorItems->each(function ($item) use ($orderCart) {
                $orderCart->items()->save($item);
                $orderCart->update(['admin_id' => $item->admin_id]);
            });

            // Recalculate total price for the cart and order
            $orderCart->calculateTotalPrice();
            $order->calculatePrice();
        }

        $orderDetail->calculatePrices();
        // Return the order details with associated orders
        return $orderDetail->load(['orders' , 'carts']);
    }
}
