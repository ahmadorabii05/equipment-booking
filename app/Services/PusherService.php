<?php

namespace App\Services;


use GuzzleHttp\Exception\GuzzleException;
use Pusher\ApiErrorException;
use Pusher\Pusher;
use Pusher\PusherException;

class PusherService
{

    public $subscriber;


    public function __construct($subscriber)
    {
        $this->subscriber = $subscriber;
    }


    /**
     * @throws PusherException
     */
    private function getPusherInstance(): Pusher
    {
        $options = config('pusher.service.options');
        return new Pusher(
            config('pusher.service.app-key'),
            config('pusher.service.app-secret'),
            config('pusher.service.app-id'),
            $options
        );
    }

    private function getChannelByService(): array
    {
        return ['event' => 'UpdateUserLocationEvent', 'channel' => 'update-location-' . $this->subscriber->id];
    }


    /**
     * @throws PusherException
     * @throws ApiErrorException
     * @throws GuzzleException
     */
    public function pushData(): void
    {
        $pusher = $this->getPusherInstance();
        $channel = $this->getChannelByService();
        $payload = [
            'id' => $this->subscriber->id,
        ];
        $pusher->trigger($channel['channel'], $channel['event'], $payload);
    }
}
