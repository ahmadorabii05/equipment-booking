<?php

namespace App\Services;

use App\Models\Order;
use App\Models\OrderDetail;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;
use URWay\Client;
use URWay\Response;

class PaymentService
{

    private const CUSTOMER_IP = '10.10.10.10';
    private const CURRENCY = 'SAR';
    private const COUNTRY = 'SA';

    private const REJECTED = 500;
    private const NOT_FOUND = 400;

    private static function getRedirectUrl(): string
    {
        return route('user.checkout.completeToCheckout');
    }


    /**
     * @param OrderDetail $order
     * @return false|mixed|Response
     * @throws GuzzleException
     */
    public static function proceedPayment(OrderDetail $order)
    {
        try {
            $price = number_format((float)$order->total_price, 2, '.', '');
            $client = new Client();
            $client->setTrackId($order->code)
                ->setCustomerEmail($order->user->email)
                ->setAmount($price)
                ->setCustomerIp(self::CUSTOMER_IP)
                ->setCurrency(self::CURRENCY)
                ->setCountry(self::COUNTRY)
                ->setRedirectUrl(self::getRedirectUrl());

            return $client->pay();

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public static function getTransactionPayment($data)
    {
        $responseCode = $data['ResponseCode'];
        if ($data['Result'] === 'Successful' && $responseCode === '000') {
            $trackid = $data['TrackId'];
            $amount = $data['amount'];

            $client = new Client();
            $client->setTrackId($trackid)->setCurrency(self::CURRENCY)->setAmount($amount);
            return $client->find($data['TranId']);
        }
        if (str_starts_with($responseCode, '5')) {
            return self::REJECTED;
        }
        return self::NOT_FOUND;
    }

    public static function sdk(Order $order)
    {
        $price = round((float)$order->total_price, 2);

        $terminalId = config('urway.auth.terminal_id');
        $password = config('urway.auth.password');
        $merchant_key = config('urway.auth.merchant_key');
        $idorder = $order->code;
        //Generate Hash
        $txn_details = $idorder . '|' . $terminalId . '|' . $password . '|' . $merchant_key . '|' . $price . '|' . self::CURRENCY;
        $hash = hash('sha256', $txn_details);


        $fields = array(
            'trackid' => $idorder,
            'terminalId' => $terminalId,
            'customerEmail' => $order->client->email,
            'action' => "1",  // action is always 1
            'merchantIp' => self::CUSTOMER_IP,
            'password' => config('urway.auth.password'),
            'currency' => self::CURRENCY,
            'country' => "SA",
            'amount' => $price,
            "udf1" => "Test1",
            "udf2" => "https://urway.sa/urshop/scripts/response.php",//Response page URL
            "udf3" => "",
            "udf4" => "",
            "udf5" => "Test5",
            'requestHash' => $hash  //generated Hash
        );
        $data = json_encode($fields);
        $ch = curl_init('https://payments-dev.urway-tech.com/URWAYPGService/transaction/jsonProcess/JSONrequest'); // Will be provided by URWAY
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        //execute post
        $server_output = curl_exec($ch);
        //close connection
        curl_close($ch);
        $result = json_decode($server_output);
        Log::error('orders.payment_method2', [$result, $price, $hash]);

        if (!empty($result->payid) && !empty($result->targetUrl)) {
            return $result->targetUrl . '?paymentid=' . $result->payid;
//                header('Location: '. $url, true, 307);//Redirect to Payment Page
        }

        return false;
    }

}
