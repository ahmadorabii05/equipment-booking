<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Auth\LoginPartnerRequest;
use App\Models\Admin;
use App\Repositories\AdminRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends ApiController
{
    protected AdminRepository $adminRepository;

    public function __construct( AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    public function login(LoginPartnerRequest $request): JsonResponse
    {

        $credentials = $request->only('email', 'password');
        if (auth('partner')->attempt($credentials)) {
            $user = Admin::query()->role('partner')->whereId(auth('partner')->id())->first();
            if ($user->status === Admin::STATUS_INACTIVE) {
                return $this->respondError(__('api.please_verify_your_account'));
            }


            if (!$user->token) {
                $token = $user->createToken('API');
                $user->token = $token->plainTextToken;
                $user->save();
            }
            return $this->respondSuccess([
                'token' => $user->token,
            ]);
        }

        return $this->respondError(__('api.username_or_password_invalid'));
    }


    public function logout(Request $request): JsonResponse
    {
        $admin = $this->getUser($request);

        if ($admin) {
            auth('api')->user()->currentAccessToken()->delete();
            $admin->token = null;
            $admin->save();
            return $this->respondSuccess(__('api.done'));
        }

        return $this->respondSuccess(__('api.logout'));
    }


}
