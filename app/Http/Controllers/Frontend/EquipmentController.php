<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Equipment;
use App\Models\Shift;
use App\Repositories\EquipmentRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    private $equipmentRepository;

    public function __construct(EquipmentRepository $equipmentRepository)
    {
        $this->equipmentRepository = $equipmentRepository;
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('frontend.pages.equipment.create');
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $equipments = $this->equipmentRepository->getEquipments($request)->paginate(8);
        return view('frontend.pages.equipment.index', compact('equipments',));
    }

    /**
     * @param Equipment $equipment
     * @param Request $request
     * @return Application|Factory|View
     */
    public function show(Equipment $equipment, Request $request)
    {
        $request->request->add(['admin_id' => $equipment->admin_id ?? null]);
        $recommendedEquipments = $this->equipmentRepository->getEquipments($request)->where('category_id', $equipment->category_id)->take(4)->get();
        return view('frontend.pages.equipment.show', compact('equipment', 'recommendedEquipments'));
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getShiftTimes(Request $request): JsonResponse
    {
        $shiftIds = $request->get('id');
        $shifts = Shift::whereIn('id', $shiftIds)->active()->orderBy('start_date')->get();

        if ($shifts->isEmpty()) {
            return response()->json(['status' => 'error', 'message' => 'Shift not found'], 404);
        }

        $data = $shifts->flatMap(function ($shift) {
            $formattedDate = Carbon::parse($shift->start_date)->format('m-d');
            return Shift::whereRaw('DATE_FORMAT(start_date, "%m-%d") = ?', [$formattedDate])
                ->active()
                ->get();
        })->map(function ($item) {
            return [
                'id'           => $item->id,
                'start_period' => Carbon::parse($item->start_date)->format('H:i'),
                'date'         => Carbon::parse($item->start_date)->format('m-d H:i'),
                'end_period'   => Carbon::parse($item->end_date)->format('H:i'),
                'full_date'    => Carbon::parse($item->start_date)->format('m-d H:i') . ' => ' . Carbon::parse($item->end_date)->format('H:i')
            ];
        });
        $times = collect($data);
        $html = view('frontend.components.u-select-period', compact('times'))->render();
        return response()->json(['html' => $html, 'data' => $data]);
    }
}
