<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\CityRepository;
use App\Repositories\EquipmentRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    public function index(Request $request , EquipmentRepository  $equipmentRepository)
    {
        $equipments = $equipmentRepository
            ->getEquipments($request)
            ->take(4)->get();
        return view('frontend.pages.home' ,compact('equipments'));
    }

    public function changeLang(): RedirectResponse
    {

        if ($local = request('lang')) {
            app()->setLocale(request('lang'));
            App::setLocale($local);
            Config::set('translatable.locale', $local);
            Session::put('lang', $local);
        }
        return redirect()->back();
    }

    public function citiesAutoComplete(Request $request , CityRepository $regionRepository): JsonResponse
    {
        $search = $request->get('search');
        $models = $regionRepository->citiesAutoComplete($search);

        return response()->json([
            'results' => $models
        ]);
    }
}
