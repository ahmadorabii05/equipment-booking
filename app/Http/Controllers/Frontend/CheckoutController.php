<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Equipment;
use App\Models\OrderDetail;
use App\Repositories\CartRepository;
use App\Services\CheckoutService;
use App\Services\PaymentService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CheckoutController extends Controller
{
    private $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->middleware('auth');
        $this->cartRepository = $cartRepository;
    }

    public function index()
    {

    }

    public function proceedToCheckout(Request $request): RedirectResponse
    {
        $user = auth('user')->user();
        $validator = Validator::make($request->all(), [
            // Add your validation rules here
        ]);

        if ($validator->fails()) {
            return $this->responseHandler('error', __('frontend.something_went_wrong'), 'user.cart.index');
        }

        $cart = $this->cartRepository->getUserCart($user->id);

        if ($cart->items->isEmpty()) {
            return $this->responseHandler('error', __('frontend.something_went_wrong'), 'user.cart.index');
        }

        $paymentMethod = OrderDetail::STATUS_PAYMENT_URWAY;
        $url = null;

        DB::beginTransaction();

        try {
            $order = CheckoutService::proceedToCheckout($cart, $paymentMethod, $user->id);

            if ($order->payment_method === OrderDetail::STATUS_PAYMENT_URWAY) {
                $response = PaymentService::proceedPayment($order);
                if ($response->getPaymentUrl()) {
                    $url = $response->getPaymentUrl();
                } else {
                    $cart->update(['order_id' => null, 'status' => 0]);
                    $order->delete();
                    return $this->responseHandler('error', __('frontend.error_invalid_payment'), 'user.cart.index');
                }
            } else {
                $order->update(['is_done' => true]);
            }

            $cart->update(['status' => 1]);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return $this->responseHandler('error', __('frontend.something_went_wrong'), 'user.cart.index');
        } catch (GuzzleException $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return $this->responseHandler('error', __('frontend.something_went_wrong'), 'user.cart.index');
        }

        $route = $url;
        return $this->responseHandler('success', __('frontend.your_order_has_been_created_successfully'), $route, true);
    }


    public function completeCheckoutAction(Request $request, PaymentService $paymentService): RedirectResponse
    {
        $data = $request->all();
        $trackId = $request->get('TrackId');
        $order = OrderDetail::with('carts')->where('code', $trackId)->first();

        if (!$order) {
            Log::error("Order not found for TrackId: $trackId");
            return redirect()->route('user.cart.index')->with('error', __('frontend.order_not_found'));
        }

        $response = $paymentService->getTransactionPayment($data);

        if ($response === 500) {
            $this->handlePaymentError($order, __('frontend.the_payment_rejected_from_bank'));
        } elseif ($response === 400) {
            $this->handlePaymentError($order, __('frontend.payment_is_not_completed'));
        } else {
            DB::transaction(static function () use ($order) {
                if ($order) {
                    $order->update(['is_paid' => true, 'is_done' => true]);
                    $equipmentUpdates = [];
                    foreach ($order->carts as $cart) {
                        foreach ($cart->items as $item) {
                            $equipmentId = $item->equipment_id;
                            $equipmentUpdates[$equipmentId] = ($equipmentUpdates[$equipmentId] ?? 0) + $item->qty;
                        }
                    }

                    foreach ($equipmentUpdates as $equipmentId => $decrementQty) {
                        Equipment::where('id', $equipmentId)->decrement('count', $decrementQty);
                    }
                }
            });
        }

        $request->session()->flash('success', __('frontend.your_order_has_been_created_successfully'));
        return redirect()->route('user.invoice.show', $order->id);
    }

    private function handlePaymentError(?OrderDetail $order, string $errorMessage): void
    {
        Log::error("Payment error for Order ID: {$order} - $errorMessage");
        if ($order) {
            $order->delete();
        }
        session()->flash('error', $errorMessage);
        redirect()->route('user.cart.index');
    }

    private function responseHandler($status, $message, $url, $paymentGateway = false): RedirectResponse
    {
        request()->session()->flash($status, $message);
        if ($paymentGateway) {
            return redirect($url);
        }
        return redirect()->route($url);
    }
}
