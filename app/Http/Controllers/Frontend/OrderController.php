<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\OrderDetail;

class OrderController extends Controller
{
    public function showInvoice($orderId)
    {
        $order = OrderDetail::with('user')->findOrFail($orderId);
        return view('frontend.pages.cart.invoice', compact('order'));
    }
}
