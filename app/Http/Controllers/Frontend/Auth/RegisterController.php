<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UserRegisterRequest;
use App\Models\Admin;
use App\Models\User;
use App\Repositories\AdminRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
    public $userRepository;
    public $adminRepository;

    public function __construct(UserRepository $userRepository, AdminRepository $adminRepository)
    {
        $this->userRepository = $userRepository;
        $this->adminRepository = $adminRepository;
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'type' => ['required', Rule::in(['contractor', 'supplier'])],
        ]);

        if ($validator->fails()) {
            $request->session()->flash('error', __('frontend.pressed_fails'));
            return redirect()->route('user.home');
        }

        if (auth('user')->check()) {
            return redirect()->route('user.home');
        }

        switch ($request->get('type')) {
            case Admin::TYPE_SUPPLIER:
                return view('frontend.auth.register-supplier');
            case User::TYPE_CONTACTOR:
                return view('frontend.auth.register');
        }
    }

    public function registerPost(UserRegisterRequest $request): RedirectResponse
    {
        switch ($request->get('type')) {
            case Admin::TYPE_SUPPLIER:
                return $this->registerSupplierPost($request);
            case User::TYPE_CONTACTOR:
                return $this->registerContractorPost($request);
            default:
                $request->session()->flash('error', __('frontend.invalid_registration_type'));
                return redirect()->back();
        }
    }
    public function registerContractorPost(UserRegisterRequest $request): RedirectResponse
    {
        DB::beginTransaction();

        try {
            // Add the user via the repository
            $user = $this->userRepository->add($request);

            // Commit the transaction if successful
            DB::commit();

            // Log the user in
            Auth::guard('user')->login($user);


            return redirect()->route('user.equipments.index');
        } catch (\Exception $e) {
            DB::rollback();

            Log::error('User registration failed', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
                'user_data' => $request->all()
            ]);

            $request->session()->flash('error', __('frontend.registration_failed'));

            return redirect()->route('user.register');
        }
    }
    public function registerSupplierPost(UserRegisterRequest $request): RedirectResponse
    {
        $role = Role::query()->where('name', 'supplier')->first();
        $request->request->add(['role' => $role->id ?? null]);

        DB::beginTransaction();

        try {
            // Add the user via the repository
            $user = $this->adminRepository->add($request);

            // Commit the transaction if successful
            DB::commit();

            // Log the user in
            Auth::guard('web')->login($user);


            return redirect()->route('admin.dashboard');
        } catch (\Exception $e) {
            DB::rollback();

            Log::error('User registration failed', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
                'user_data' => $request->all()
            ]);

            $request->session()->flash('error', __('frontend.registration_failed'));

            return redirect()->route('user.register');
        }
    }

}
