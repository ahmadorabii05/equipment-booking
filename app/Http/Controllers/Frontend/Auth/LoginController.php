<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Admin;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public $userRepository;
    private const GUARD_WEB = 'web';
    private const GUARD_USER = 'user';

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    public function login()
    {
        if (auth('user')->check()) {
            return redirect()->route('user.login');
        }
        return view('frontend.auth.login');
    }

    public function loginPost(LoginRequest $request): RedirectResponse
    {
        $credentials = $request->only('email', 'password');
        switch ($request->get('type')) {
            case Admin::TYPE_SUPPLIER:
                return $this->loginUser($credentials, $request, self::GUARD_WEB);
            case User::TYPE_CONTACTOR:
                return $this->loginUser($credentials, $request, self::GUARD_USER);
            default:
                return $this->loginUser($credentials, $request);
        }
    }

    /**
     * Attempt to log in the user with the given credentials.
     *
     * @param array $credentials
     * @param Request $request
     * @param string $guard
     * @return RedirectResponse
     */
    public function loginUser(array $credentials, Request $request, string $guard = 'web'): RedirectResponse
    {
        if (Auth::guard($guard)->attempt($credentials)) {
            $user = Auth::guard($guard)->user();

            if (!$user->isActive()) {
                $request->session()->flash('warning', __('frontend.waiting_activation'));
                Auth::guard($guard)->logout();

                return redirect()->route('user.login');
            }

            if ($guard === 'web' && $user instanceof Admin && $user->isActive()) {
                return redirect()->route('admin.dashboard');
            }

            if ($user instanceof User) {
                return redirect()->route('user.equipments.index');
            }

            Auth::guard($guard)->logout();
        }

        $request->session()->flash('error', __('frontend.wrong_credentials'));

        return redirect()->route('user.login');
    }

    public function logout(): RedirectResponse
    {
        auth('user')->logout();
        return redirect()->route('user.login');
    }
}
