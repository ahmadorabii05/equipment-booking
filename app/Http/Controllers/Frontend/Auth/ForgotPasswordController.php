<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{

    public function forgotPassword()
    {
        return view('frontend.auth.forgot-password');
    }

    public function forgotPasswordPost(ForgotPasswordRequest $request): RedirectResponse
    {
        $user = User::query()->where('email', $request->get('email'))->first();
        if (!$user) {
            $request->session()->flash('error', __('frontend.wrong_account_not_found'));
            return redirect()->back();
        }

        try {
            $user = $user->generatePasswordToken();
            if ($user instanceof User) {
                $user->save();
            } else {
                $request->session()->flash('error', __('frontend.invalid_email'));
                return redirect()->route('user.login');
            }
            $request->session()->flash('success', __('frontend.we_send_a_code'));
            return redirect()->route('user.home');

        } catch (\Exception $e) {
            $request->session()->flash('success', __('frontend.invalid_email'));
            return redirect()->route('user.home');
        }
    }

    public function checkCode(Request $request): RedirectResponse
    {
        $user = User::query()->where('reset_token', $request->get('reset_token'))->first();
        if ($user) {
            $checkCode = $user->checkPasswordCode($request->get('reset_token'));
            if ($checkCode) {
                $request->session()->put('user', $user);
                return redirect()->route('user.change.password.page');
            } else {
                $request->session()->flash('error', __('frontend.the_code_is_invalid'));
                return redirect()->route('user.home');
            }
        }
        $request->session()->flash('error', __('frontend.invalid'));
        return redirect()->route('user.home');

    }

    public function changePasswordPage(Request $request)
    {
        $user = $request->session()->get('user');
        if ($user)
            return view('frontend.auth.change-password', compact('user'));
        else
            return redirect()->route('user.home');
    }


    public function changePasswordPost(Request $request): RedirectResponse
    {
        $request->validate([
            'password' => 'required|confirmed'
        ]);
        $user = User::findOrfail($request->get('user'));
        if ($user) {
            $user->changePassword($request->get('password'));
            $request->session()->flash('success', __('frontend.change_password_was_checked'));
        } else {
            $request->session()->flash('error', __('frontend.error_try_change_agine'));
            return redirect()->route('user.home');
        }
        return redirect()->route('user.login');
    }

}
