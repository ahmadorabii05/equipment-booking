<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Equipment;
use App\Repositories\CartRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    private $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function index()
    {
        $userId = auth('user')->id();
        $cart = $this->cartRepository->getUserCart($userId);
        return view('frontend.pages.cart.index', compact('cart'));
    }


    /**
     * @throws \Exception
     */
    public function addToCart(Request $request): RedirectResponse
    {

        $validatedData = Validator::make($request->all(), [
            'full_date' => 'required',
            'quantity' => 'required',
        ]);

        if ($validatedData->fails()) {
            $request->session()->flash('error', $validatedData->errors()->first());
            return redirect()->back()->withErrors(['Equipment not available']);
        }

        $user = auth('user')->user();
        if (!$user) {
            $request->session()->flash('error', 'You are not authenticated');
            return redirect()->back();
        }

        $cart = $this->cartRepository->getUserCart($user->id);

        $equipmentId = $request->get('equipment_id');
        $quantity = (int)$request->get('quantity');
        $shifts = $request->get('full_date');

        $shiftsCount = $shifts ? count($shifts) : 0;
        $equipment = Equipment::query()->active()->findOrFail($equipmentId);

        if (!$equipment) {
            return redirect()->back()->withErrors(['Equipment not available now']);
        }

        $cartItem = $cart->items()->where('equipment_id', $equipment->id)->first();

        try {
            if (!$cartItem) {
                $this->cartRepository->createCartItem($equipment, $quantity, $cart->id, $shiftsCount);
            } else {
//                $this->cartRepository->updateCartItem($equipment, $quantity, $cartItem);
            }

            $user->shifts()->attach($shifts, ['equipment_id' => $equipment->id]);

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $request->session()->flash('error', $e->getMessage());
            return redirect()->route('user.equipments.index');
        }


        $cart = $this->cartRepository->getUserCart($user->id);
        $cart->calculateTotalPrice();

        $request->session()->flash('success', __('frontend.addCart_success'));
        return redirect()->route('user.equipments.index');
    }

    /**
     * @throws \Exception
     */
    public function deleteItem(Equipment $equipment): RedirectResponse
    {

        $user = auth('user')->user();
        if (!$user) {
            return redirect()->back()->withErrors(['You are not allowed to delete']);
        }
        $cart = $this->cartRepository->getUserCart($user->id);
        $equipmentItem = $cart->items()->where('equipment_id', $equipment->id)->first();

        if (!$equipmentItem) {
            return redirect()->back()->withErrors(['Equipment not available']);
        }

        $shiftIds = $user->shifts()->wherePivot('equipment_id', $equipment->id)->pluck('shifts.id')->toArray();
        $user->shifts()->detach($shiftIds);
        $equipmentItem->delete();
        $cart = $this->cartRepository->getUserCart($user->id);
        $cart->calculateTotalPrice();
        return redirect()->route('user.equipments.index');
    }
}
