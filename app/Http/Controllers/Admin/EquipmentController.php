<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EquipmentRequest;
use App\Models\Equipment;
use App\Models\Product;
use App\Repositories\EquipmentRepository;
use App\Services\DatatableService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class EquipmentController extends Controller
{
    public string $resource = 'equipments';
    private EquipmentRepository $equipmentRepository;
    public function __construct(EquipmentRepository $equipmentRepository)
    {
        appendGeneralPermissions($this);
        $this->equipmentRepository = $equipmentRepository;
        view()->share('item', 'equipment');
        view()->share('class', Equipment::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('admin.crud.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.crud.edit-new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EquipmentRequest $request
     * @return RedirectResponse
     */
    public function store(EquipmentRequest $request): RedirectResponse
    {
        $this->equipmentRepository->add($request);

        $request->session()->flash('success', 'Equipment created successfully');

        if ($request->has('add-new')) {
            return redirect()->route('admin.equipment.create');
        }

        return redirect()->route('admin.equipment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Equipment $equipment
     * @return Factory|View
     */
    public function show(Equipment $equipment)
    {
        return view('admin.equipment.show', compact('equipment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Equipment $equipment
     * @return Application|Factory|View
     */
    public function edit(Equipment $equipment)
    {
        return view('admin.crud.edit-new', ['equipment' => $equipment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EquipmentRequest $request
     * @param Equipment $equipment
     * @return RedirectResponse
     */
    public function update(EquipmentRequest $request, Equipment $equipment): RedirectResponse
    {
        $this->equipmentRepository->update($request, $equipment);

        $request->session()->flash('success', 'Equipment updated successfully');

        if ($request->has('add-new')) {
            return redirect()->back();
        }

        return redirect()->route('admin.equipment.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Equipment $equipment
     * @return RedirectResponse
     */
    public function destroy(Request $request, Equipment $equipment): RedirectResponse
    {
        $this->equipmentRepository->delete($equipment);
        $request->session()->flash('success', 'Equipment deleted successfully');
        return redirect()->route('admin.equipment.index');
    }

    /**
     * Retries the resources from storage.
     *
     * @param Request $request
     * @param EquipmentRepository $equipmentRepository
     * @return JsonResponse
     */
    public function getEquipments(Request $request, EquipmentRepository $equipmentRepository): JsonResponse
    {
        $equipments = $equipmentRepository->getEquipmentDataTable($request);
        return response()->json(DatatableService::equipmentDatatable($equipments));
    }

    public function removeImage(Request $request): string
    {
        $item = Equipment::query()->find($request->get('id'));
        $images = [];
        foreach ($item->images as $image) {
            if ($image == $request->get('image')) {
                Storage::disk('public')->delete($image);
            } else {
                $images[] = $image;
            }
        }
        $item->images = $images;
        $item->save();

        return 'image removed successfully';
    }
}
