<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ShiftRequest;
use App\Http\Traits\ActionsTrait;
use App\Models\Shift;
use App\Repositories\ShiftRepository;
use App\Services\DatatableService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ShiftController extends Controller
{
    private $shiftRepository;
    public $resource = 'shift';
    use ActionsTrait;

    public function __construct(ShiftRepository $shiftRepository)
    {
     //   appendGeneralPermissions($this);
        $this->shiftRepository = $shiftRepository;
        view()->share('item', $this->resource);
        view()->share('class', Shift::class);
    }

    /**
     * Display a listing of the resource.
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('admin.crud.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.crud.edit-new');
    }

    public function store(ShiftRequest $request): RedirectResponse
    {
        $this->shiftRepository->add($request);

        $request->session()->flash('success', 'shift created successfully');

        if ($request->has('add-new')) {
            return redirect()->route('admin.shifts.create');
        }

        return redirect()->route('admin.shifts.index');
    }

    public function getShifts(Request $request, ShiftRepository $userRepository): JsonResponse
    {
        $users = $userRepository->getShiftsDataTable($request);
        return response()->json(DatatableService::shiftsDatatable($users));
    }
}
