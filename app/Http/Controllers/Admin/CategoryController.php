<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use App\Services\DatatableService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CategoryController extends Controller
{
    public string $resource = 'category';
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        appendGeneralPermissions($this);
        $this->categoryRepository = $categoryRepository;
        view()->share('item', $this->resource);
        view()->share('class', Category::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('admin.crud.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.crud.edit-new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryRequest $request
     * @return RedirectResponse
     */
    public function store(CategoryRequest $request): RedirectResponse
    {

        $this->categoryRepository->add($request);

        $request->session()->flash('success', 'category created successfully');

        if ($request->has('site-categories')) {
            return back();
        }

        if ($request->has('add-new')) {
            return redirect()->route('admin.categories.create');
        }

        return redirect()->route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return Category
     */
    public function show(Category $category): Category
    {
        return $category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return Application|Factory|View
     */
    public function edit(Category $category)
    {
        return view('admin.crud.edit-new', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryRequest $request
     * @param Category $category
     * @return RedirectResponse
     */
    public function update(CategoryRequest $request, Category $category): RedirectResponse
    {
        $this->categoryRepository->update($request, $category);

        $request->session()->flash('success', 'category updated successfully');

        if ($request->has('site-categories')) {
            return back();
        }

        if ($request->has('add-new')) {
            return redirect()->back();
        }

        return redirect()->route('admin.categories.index');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Category $category
     * @return RedirectResponse
     */
    public function destroy(Request $request, Category $category): RedirectResponse
    {
        $this->categoryRepository->delete($category);
        $request->session()->flash('success', 'category deleted successfully');
        return redirect()->route('admin.categories.index');
    }

    public function getCategories(Request $request): JsonResponse
    {
        $advertisers = $this->categoryRepository->getCategoriesDataTable($request);
        return response()->json(DatatableService::categoriesDatatable($advertisers));
    }
}
