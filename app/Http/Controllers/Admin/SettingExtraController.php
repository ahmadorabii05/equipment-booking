<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;

class SettingExtraController extends Controller
{
    public $resource = 'setting';

    public function __construct()
    {
        appendGeneralPermissions($this);
        view()->share('item', $this->resource);
        view()->share('class', Setting::class);
        view()->share('custom', true);
    }


    public function about()
    {
        $setting = Setting::where('key', 'app.about-us')->first();
        return view('admin.crud.edit-new', compact('setting'));
    }

    public function contact()
    {
        $setting = Setting::where('key', 'app.contact-us')->first();
        return view('admin.crud.edit-new', compact('setting'));
    }

    public function terms()
    {
        $setting = Setting::where('key', 'app.terms-conditions')->first();
        return view('admin.crud.edit-new', compact('setting'));
    }



}
