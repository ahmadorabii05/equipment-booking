<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Repositories\OrderRepository;
use App\Services\DatatableService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class OrderController extends Controller
{
    public $resource = 'order';

    public function __construct()
    {
        appendGeneralPermissions($this);
        view()->share('item', $this->resource);
        view()->share('class', Order::class);
    }

    /**
     * Display a listing of the resource
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('admin.crud.index');
    }

    /**
     * Display the specified resource.
     * @param $id
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id): \Illuminate\Contracts\View\View
    {
        if (!checkIfAdmin() && checkIfAdmin('supplier')) {
            $order = Order::where('order_details_id', $id)
                ->where('admin_id', auth('web')->id())
                ->first();
        } else {
            $order = OrderDetail::findOrFail($id);
            $order->load(['orders', 'carts']);
        }
        return view('admin.order.show', compact('order'));
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param  $id
     * @return string[]
     */
    public function setOrderStatus(Request $request, $id): array
    {
        $order = OrderDetail::findOrFail($id);
        if (checkIfAdmin()) {
            $order->status = $request->get('status');
            foreach ($order->orders as $item) {
                tap($item)->update([
                    'status' => $order->status
                ]);
            }
            $order->save();
        }

        if (checkIfAdmin('supplier')) {
            $order = $order->orders()->where('admin_id', auth('web')->id())->first();
            $currentStatus = $order->status;
            $newStatus = (int)$request->get('status');
            $result = changeOrderStatus($currentStatus, $newStatus, $order);
            if (!$result) {
                return ["status" => "error" , 'message' =>"You can't change order status"];
            }
        }
        return ["status" => "success" , 'message' => "Edit Status Order Successfully"];
    }



    public function getOrders(Request $request, OrderRepository $orderRepository): JsonResponse
    {

        if (checkIfAdmin('supplier')) {
            $vendorId = auth('web')->id();
            $orders = $orderRepository->getOrdersDataTable($request, $vendorId);
            return response()->json(DatatableService::supplierOrdersDatatable($orders, $vendorId));
        }
        $orders = $orderRepository->getOrdersDataTable($request);
        return response()->json(DatatableService::ordersDatatable($orders));
    }

}
