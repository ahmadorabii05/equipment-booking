<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminRequest;
use App\Http\Traits\ActionsTrait;
use App\Models\Admin;
use App\Repositories\AdminRepository;
use App\Services\DatatableService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AdminController extends Controller
{

    public $resource = 'admin';
    protected AdminRepository $adminRepository;
    use ActionsTrait;

    public function __construct(AdminRepository $adminRepository)
    {
        appendGeneralPermissions($this);
        $this->adminRepository = $adminRepository;
        view()->share('item', $this->resource);
        view()->share('class', Admin::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('admin.crud.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.crud.edit-new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminRequest $request
     * @return RedirectResponse
     */
    public function store(AdminRequest $request): RedirectResponse
    {

        $this->adminRepository->add($request);

        $request->session()->flash('success', 'advertiser created successfully');

        if ($request->has('add-new')) {
            return redirect()->route('admin.admins.create');
        }

        return redirect()->route('admin.admins.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Admin $admin
     * @return Factory|\Illuminate\Contracts\View\View
     */
    public function show(Admin $admin)
    {
        return view('admin.admin.show', compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Admin $admin
     * @return Application|Factory|View
     */
    public function edit(Admin $admin)
    {
        return view('admin.crud.edit-new', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminRequest $request
     * @param Admin $admin
     * @return RedirectResponse
     */
    public function update(AdminRequest $request, Admin $admin): RedirectResponse
    {
        $this->adminRepository->update($request, $admin);

        $request->session()->flash('success', 'advertiser updated successfully');

        if ($request->has('add-new')) {
            return redirect()->back();
        }

        return redirect()->route('admin.admins.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function destroy(Request $request, $id): RedirectResponse
    {
        $advertiser = Admin::find($id);
        $this->adminRepository->delete($advertiser);
        $request->session()->flash('success', 'admin deleted successfully');
        return redirect()->route('admin.admins.index');
    }

    public function supplierAutoComplete(Request $request): JsonResponse
    {
        $search = $request->get('search');
        $models = $this->adminRepository->supplierAutoComplete($search);

        return response()->json([
            'results' => $models
        ]);
    }
    /**
     * Retries the resources from storage.
     *
     * @param Request $request
     * @param AdminRepository $adminRepository
     * @return JsonResponse
     */
    public function getAdmins(Request $request, AdminRepository $adminRepository): JsonResponse
    {
        $advertisers = $adminRepository->getAdminsDataTable($request);
        return response()->json(DatatableService::adminDatatable($advertisers));
    }
    public function setStatus(Request $request, $id): string
    {
        $className = modelName($request->get('type') ?? 'admin');
        $item = $className::find($id);
        if ($item){
            $item->status = (int)$request->get('status');
            $item->save();
        }
        return "Edit Status Successfully";
    }


}
