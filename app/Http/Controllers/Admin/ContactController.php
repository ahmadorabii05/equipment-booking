<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Http\Traits\ActionsTrait;
use App\Models\Contact;
use App\Repositories\ContactRepository;
use App\Services\DatatableService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ContactController extends Controller
{
    private $contactRepository;
    public $resource = 'contact';
    use ActionsTrait;

    public function __construct(ContactRepository $contactRepository)
    {
        appendGeneralPermissions($this);
        $this->contactRepository = $contactRepository;
        view()->share('item', $this->resource);
        view()->share('class', Contact::class);
    }

    /**
     * Display a listing of the resource.
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('admin.crud.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Contact $contact
     * @return Factory|\Illuminate\Contracts\View\View
     */
    public function show(Contact $contact)
    {
        return view('admin.crud.show', compact('contact'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Contact $contact
     * @return RedirectResponse
     */
    public function destroy(Request $request, Contact $contact): RedirectResponse
    {
        $this->contactRepository->delete($contact);
        $request->session()->flash('success', 'contact deleted successfully');
        return redirect()->route('admin.contacts.index');
    }


    public function getContacts(Request $request, ContactRepository $contactRepository): JsonResponse
    {
        $contacts = $contactRepository->getContactsDataTable($request);

        return response()->json(DatatableService::contactsDatatable($contacts));

    }


}
