<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\CityRepository;
use App\Services\DatatableService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\City;
use Illuminate\Support\Facades\DB;


class CityController extends Controller
{
    public string $resource = 'city';

    public function __construct()
    {
        appendGeneralPermissions($this);
        view()->share('item', $this->resource);
        view()->share('class', City::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('admin.crud.index');
    }

    /**
     * Display the specified resource.
     *
     * @param City $city
     * @return Factory|View
     */
    public function show(City $city)
    {
        return view('admin.crud.show', compact('city'));
    }

    /**
     * Retries the resources from storage.
     *
     * @param Request $request
     * @param CityRepository $regionRepository
     * @return JsonResponse
     */
    public function getCities(Request $request, CityRepository $regionRepository): JsonResponse
    {
        $regions = $regionRepository->getCitiesDataTable($request);
        return response()->json(DatatableService::cityDatatable($regions));
    }

    public function citiesAutoComplete(Request $request , CityRepository $regionRepository)
    {
        $search = $request->get('search');
        $models = $regionRepository->citiesAutoComplete($search);

        return response()->json([
            'results' => $models
        ]);
    }
}
