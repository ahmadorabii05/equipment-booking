<?php

namespace App\Http\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

trait MediaTrait
{
    public function uploadMedia(Request $request,  $model , $key = 'images')
    {
        if ($request->hasFile($key)){
            $imagesNames = array();
            foreach ($request->file($key) as $image) {
                $imageName = Storage::disk('public')->put('models', $image);
                ImageOptimizer::optimize('storage/'.$imageName);
                $imagesNames[] = $imageName;
            }
            $model->{$key} = $imagesNames;
        }
        return $model;
    }

    public function updateMedia(Request $request,  $model , $key = 'images'){

        if ($request->hasFile($key)){
            // if there is an old image delete it
            if ($model->{$key} != null){
                foreach ($model->{$key} as $image){
                    Storage::disk('public')->delete($image);
                }
            }

            // store the new image
            $imageNames = array();
            foreach ($request->file($key) as $image) {
                $imageName = Storage::disk('public')->put('models', $image);
                ImageOptimizer::optimize('storage/'.$imageName);
                $imageNames[] = $imageName;
            }
            $model->{$key} = $imageNames;
        }

        return $model;
    }
}
