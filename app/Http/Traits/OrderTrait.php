<?php

namespace App\Http\Traits;

use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Log;

trait OrderTrait
{
    public static function createOrderDetails($array): OrderDetail
    {
        return  OrderDetail::create([
            'user_id' => $array['user_id'],
            'payment_method' => $array['payment_method'],
            'code' => generateCode(),
        ]);
    }


    /**
     * @param int $adminId
     * @param int|null $userId
     * @param int|null $orderDetailsId
     * @return Order
     */
    private static function createOrder(int $adminId,
                                        int $userId = null,
                                        int $orderDetailsId = null): Order
    {
        $array = [
            'admin_id' => $adminId,
            'order_details_id' => $orderDetailsId,
            'user_id' => $userId,
        ];
        return self::createOrderModel($array);
    }
    public static function createOrderModel($array): Order
    {
        return Order::create([
            'user_id' => $array['user_id'],
            'order_details_id' => $array['order_details_id'],
            'admin_id' => $array['admin_id'],
        ]);
    }
}
