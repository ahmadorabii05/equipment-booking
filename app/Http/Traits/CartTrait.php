<?php

namespace App\Http\Traits;

use App\Models\Cart;
use App\Models\CartItem;
use Exception;


trait CartTrait
{
    private function deliveryStatus($product): int
    {
        if ($this->checkIfProductHasFreeShipping($product)) {
            return CartItem::STATUS_DELIVERY_FREE;
        }
        return CartItem::STATUS_DELIVERY_CHARGED;
    }


    private function checkIfProductHasFreeShipping($product): bool
    {
        return $product->status_shipping_fees === CartItem::STATUS_DELIVERY_FREE
            && $product->unit->status_shipping_fees === CartItem::STATUS_DELIVERY_FREE;
    }

    private function deleteUnavailableItems($ids, $cart): void
    {
        $cart->items->each(function ($item) use ($ids) {
            if ($item->product->status === 0 || !in_array($item->product->id, $ids, true) || !$item->product->in_stock) {
                $item->delete();
            }
        });
    }

    /**
     * @throws Exception
     */
    public static function createCart($orderId = null, $status = 0): Cart
    {
        return new Cart([
            'user_id' => auth('user')->id(),
            'order_id' => $orderId,
            'status' => $status ?? 0,
        ]);
    }
}
