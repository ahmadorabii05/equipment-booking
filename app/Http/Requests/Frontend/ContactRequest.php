<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;


class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST':
                return [
                    'name' => 'required|string|max:255',
                    'message' => 'required',
                    'email' => 'required|email',
                    'g-recaptcha-response' => 'required|captcha',
                ];
            case 'DELETE':
                return [];
            default:
                break;
        }
        return [];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'The name is required',
            'email.required' => 'The email is required',
            'email.email' => 'The email must be valid',
            'message.required' => 'The message is required',
        ];
    }

}
