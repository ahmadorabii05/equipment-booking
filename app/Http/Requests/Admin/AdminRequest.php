<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'email' => 'required|email|unique:admins,email',
                    'name' => 'required|string',
                    'role' => 'required|gt:0',
                    'password' => 'required|confirmed',
                ];
            case 'PATCH':
            case 'PUT':
                $user = $this->route()->admin;
                return [
                    'email' => 'email|unique:admins,email,' . $user->id,
                    'name' => 'required|string',
                    'role' => 'required|gt:0',
                ];
            case 'DELETE':
                return [];
            default:
                break;
        }
        return [];
    }

    public function messages(): array
    {
        return [

        ]; // TODO: Change the autogenerated stub
    }
}
