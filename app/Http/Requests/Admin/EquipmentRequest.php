<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EquipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch ($this->method()) {
            case 'POST':
            case 'PATCH':
            case 'PUT':
                return [
                    'name' => 'required|string',
                    'price' => 'required|gt:0',
                    'count' => 'required|gt:0',
                    'admin_id' => 'required',
                ];
            case 'DELETE':
                return [];
            default:
                break;
        }
        return [];
    }

    public function messages(): array
    {
        return [

        ]; // TODO: Change the autogenerated stub
    }
}
