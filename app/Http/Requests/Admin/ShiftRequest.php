<?php

namespace App\Http\Requests\Admin;

use App\Models\Shift;
use Illuminate\Foundation\Http\FormRequest;

class ShiftRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $shift = Shift::query()->orderByDesc('end_date')->first();
        switch ($this->method()) {
            case 'PATCH':
            case 'PUT':
            case 'POST':
                $rules =  [
                    'count_of_shifts' => 'required',
                ];
                if ($shift){
                    $rules['start_date'] = 'required|date|after:' . $shift->end_date ;
                    $rules['end_date']   = 'required|date|after:' . $shift->end_date ;
                }
                return $rules;
            case 'DELETE':
                return [];
            default:
                break;
        }
        return [];
    }

}
