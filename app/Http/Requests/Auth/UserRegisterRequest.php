<?php

namespace App\Http\Requests\Auth;


use Illuminate\Foundation\Http\FormRequest;


class UserRegisterRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch ($this->get('type')) {
            case 'supplier':
                return [

                ];
            case 'contractor':
                return [

                ];
        }
    }
}
