<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use function Symfony\Component\Translation\t;

class Shift extends Model
{
    use HasFactory;

    protected $fillable =['start_date', 'end_date','status', 'shift_number', 'shift_count'];
    protected $appends =['name' , 'date'];

    public function getNameAttribute()
    {
        return $this->start_date . ' => ' . $this->end_date ;
    }

    public function getDateAttribute()
    {
        return Carbon::parse($this->start_date)->format('Y-m-d') ;
    }

    public function equipment(): BelongsToMany
    {
        return $this->belongsToMany(Equipment::class, 'equipment_shift_user', 'shift_id', 'equipment_id')
            ->withPivot('user_id')
            ->withTimestamps();
    }

    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', 1);
    }

}
