<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'total_price',
        'total_without_vat_price',
        'total_vat',
        'user_id',
        'order_id',
        'status',
        'admin_id'
    ];

    protected $with = ['items'];

    protected $casts = [
        'total_price' => 'double',
        'total_vat' => 'double',
        'total_without_vat_price' => 'double'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function owner(): BelongsTo
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(CartItem::class, 'cart_id');
    }

    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', 1);
    }

    public function scopeNotActive(Builder $query): Builder
    {
        return $query->where('status', 0);
    }

    public function calculateTotalPrice(): void
    {
        $this->total_without_vat_price = $this->calculateSuTotalPrice();
        $this->total_price = $this->totalPrice();
        $this->total_vat = $this->calculateVat();
        $this->save();
    }


    public function calculateSuTotalPrice(): float
    {
        $price = 0;
        foreach ($this->items as $item) {
            $price += $item->price;
        }
        $this->total_without_vat_price = $price;
        return round($price, 2);
    }

    public function calculateVat(): float
    {
        $price = $this->calculateSutotalPrice();
        $number = round(($price * 0.15), 2);
        $this->total_vat = $number;
        return $number;
    }

    public function totalPrice(): float
    {
        $price = $this->calculateSutotalPrice() + $this->calculateVat();
        return round($price, 2);
    }

    /**
     * @param $value
     * @return float
     */
    public function getTotalVatAttribute($value): float
    {
        return round($value, 2);
    }

    /**
     * @param $value
     * @return float
     */
    public function getTotalPricesAttribute($value): float
    {
        return round($value, 2);
    }

    /**
     * @param $value
     * @return float
     */
    public function getTotalPriceAttribute($value): float
    {
        return round($value, 2);
    }
}
