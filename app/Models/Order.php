<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_details_id',
        'total_price',
        'code',
        'user_id',
        'admin_id',
        'is_paid',
        'is_done',
        'payment_method',
        'status',
    ];

    protected $appends = ['status_name', 'payment_name'];

    protected $with = ['items'];
    protected $casts = [
        'total_price' => 'double',
    ];


    public const STATUS_PENDING = 1;
    public const STATUS_ONGOING = 2;
    public const STATUS_CANCELLED = 3;
    public const STATUS_DELIVERED = 4;
    public const STATUS_RETURNED = 5;
    public const STATUS_REFUNDED = 6;
    public const STATUS_PAYMENT_CASH = 1;
    public const STATUS_PAYMENT_URWAY = 5;

    public const ALLOWED_TRANSITIONS = [
        self::STATUS_PENDING => [
            self::STATUS_ONGOING,
            self::STATUS_CANCELLED,
            self::STATUS_DELIVERED,
            self::STATUS_RETURNED,
            self::STATUS_REFUNDED
        ],
        self::STATUS_ONGOING => [
            self::STATUS_CANCELLED,
            self::STATUS_DELIVERED,
            self::STATUS_RETURNED,
            self::STATUS_REFUNDED
        ],
        self::STATUS_CANCELLED => [
            self::STATUS_REFUNDED,
            self::STATUS_RETURNED
        ],
        self::STATUS_RETURNED => [self::STATUS_REFUNDED],
        self::STATUS_REFUNDED => [],
        self::STATUS_DELIVERED => []
    ];

    /** ORDER RELATIONS **/
    public function client(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function admin(): BelongsTo
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function cart(): HasOne
    {
        return $this->hasOne(Cart::class, 'order_id');
    }

    public function items(): HasManyThrough
    {
        return $this->hasManyThrough(CartItem::class, Cart::class);
    }

    public function orderDetails(): BelongsTo
    {
        return $this->belongsTo(OrderDetail::class, 'order_details_id');
    }

    /** ORDER APPENDS **/
    public function getClientNameAttribute()
    {
        return $this->client->name;
    }

    public function getDateAttribute()
    {
        return $this->created_at->format('Y-m-d');
    }

    public function getTotalPriceAttribute($value): float
    {
        return round($value, 2);
    }


    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case self::STATUS_ONGOING:
                return __('order.ongoing');
            case self::STATUS_CANCELLED:
                return __('order.cancelled');
            case self::STATUS_DELIVERED:
                return __('order.delivered');
            default:
                return __('order.pending');
        }
    }

    /** ORDER METHODS **/
    public function calculatePrice(): void
    {
        $cart = $this->cart;
        $this->total_price = round($cart->total_price + $cart->total_vat, 4);
        $this->save();
    }

    public function scopeOwned(Builder $query): Builder
    {
        return $query;
    }

    public function scopeDone(Builder $query): Builder
    {
        return $query->where('is_done', true);
    }


    public function getPaymentNameAttribute()
    {
        return __('front.visa');
    }

}
