<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class City extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use SpatialTrait;


    protected $spatialFields = [
        'location',
    ];

    public $translatedAttributes = ['name'];
    protected $fillable = ['region_id', 'center', 'city_id' ,'longitude' ,'latitude' , 'status'];

     protected $hidden = ['translations', 'created_at','updated_at' ,'region_id' , 'status' , 'created_at' ,'updated_at' ];

    protected $casts = [
        'center' => 'array',
    ];
     public function users(): HasMany
     {
      return $this->hasMany(User::class);
     }
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', 1);
    }
}
