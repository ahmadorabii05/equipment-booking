<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    public const STATUS_ACTIVE = 1;
    public const STATUS_INACTIVE = 0;
    public $translatedAttributes = ['name' ,'description'];

    protected $fillable = [
        'sort_order',
        'slug',
        'status',
        'image',
        'parent_category'
    ];

    protected $hidden = [
        'translations',
        'created_at',
        'updated_at',
        'parent_category',
        'status'
    ];


    public function supports(): HasMany
    {
        return $this->hasMany(Admin::class);
    }

    public function parentCategory(): HasMany
    {
        return $this->hasMany(__CLASS__, 'parent_category');
    }

    public function subcategories(): HasMany
    {
        return $this->parentCategory()->active();
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(__CLASS__, 'parent_category');
    }

    public function equipment(): HasMany
    {
        return $this->hasMany(Equipment::class, 'category_id');
    }


    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', 1);
    }


    public function getTitleAttribute()
    {
        return $this->name;
    }

    public function getParent()
    {
        if (!$this->getAttribute('parent_category')) {
            return null;
        }
        return $this->getAttribute('parent_category');
    }
    /**
     * Scope a query to only include active users.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeParent(Builder $query): Builder
    {
        return $query->whereNull('parent_category');
    }

    public function scopeSub(Builder $query): Builder
    {
        return $query->whereNotNull('parent_category');
    }

    public function isParent(): bool
    {
        if ($this->getAttribute('parent_category')) {
            return false;
        }
        return true;
    }

}
