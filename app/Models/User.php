<?php

namespace App\Models;

use App\Mail\ForgotMail;
use App\Mail\VerifyMail;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public const TYPE_CONTACTOR = 'contractor';
    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'company_name',
        'tax_number',
        'commercial_registration',
        'email',
        'password',
        'phone_number',
        'code',
        'token',
        'email_verified_at',
        'mobile_verified_at',
        'reset_token',
        'reset_verified',
        'status',
        'city_id',
    ];

    public $visibleAttributes = [
        'name' => 'text',
        'email' => 'text',
        'phone_number' => 'text',
        'status' => 'status',
    ];

    public $mediaAttributes = [
        'avatar' => 'image',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'mobile_verified_at' => 'datetime',
    ];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email_verified_at',
        'mobile_verified_at',
        'reset_token',
        'reset_verified',
        'status',
        'code',
        'app_notification_status',
        'created_at',
        'updated_at',
    ];


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }



    public function isActive(): bool
    {
        return $this->status;
    }


    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function equipment(): BelongsToMany
    {
        return $this->belongsToMany(Equipment::class, 'equipment_shift_user', 'user_id', 'equipment_id')
            ->withPivot('shift_id')
            ->withTimestamps();
    }

    public function shifts(): BelongsToMany
    {
        return $this->belongsToMany(Shift::class, 'equipment_shift_user', 'user_id', 'shift_id')
            ->withPivot('equipment_id')
            ->withTimestamps();
    }

}
