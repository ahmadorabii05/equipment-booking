<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Equipment extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    protected $table = 'equipments';
    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;
    public $translatedAttributes = ['name' ,'description'];
    protected $fillable = [
        'admin_id',
        'category_id',
        'production_year',
        'count',
        'price',
        'reference_name',
        'discount',
        'isPrivate',
        'withDriver',
        'description',
        'private_project',
        'images',
    ];

    protected $casts = [
        'images' => 'array',
        'price' => 'double',
        'discount' => 'double',
    ];


    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE)
            ->orWhere('count', '!=', 0);
    }

    public function owner(): BelongsTo
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function shifts(): BelongsToMany
    {
        return $this->belongsToMany(Shift::class, 'equipment_shift', 'equipment_id', 'shift_id');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function item(): BelongsTo
    {
        return $this->belongsTo(CartItem::class, 'equipment_id');
    }
}
