<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class OrderDetail extends Model
{
    use HasFactory;

    public const STATUS_PAYMENT_CASH = 1;
    public const STATUS_PAYMENT_SADAD = 4;
    public const STATUS_PAYMENT_URWAY = 5;

    protected $fillable = [
        'user_id',
        'code',
        'status',
        'user_id',
        'payment_method',
        'total_price',
        'total_without_vat_price',
        'total_vat',
        'is_paid',
        'is_done',
        'status',
    ];

    protected $casts = [
        'total_price' => 'double',
        'total_without_vat_price' => 'double',
        'total_vat' => 'double',
    ];

    public function scopeOwned(Builder $query): Builder
    {
        return $query;
    }

    public function orders(): hasMany
    {
        return $this->hasMany(Order::class, 'order_details_id');
    }

    public function calculatePrices(): void
    {
        $carts = $this->carts();
        $this->total_without_vat_price = $carts->sum('carts.total_without_vat_price');
        $this->total_vat = $carts->sum('carts.total_vat');
        $this->total_price = $carts->sum('carts.total_price');
        $this->save();
    }

    public function carts(): HasManyThrough
    {
        return $this->hasManyThrough(Cart::class, Order::class , 'order_details_id', 'order_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case Order::STATUS_ONGOING:
                return __('order.ongoing');
            case Order::STATUS_CANCELLED:
                return __('order.cancelled');
            case Order::STATUS_DELIVERED:
                return __('order.delivered');
            default:
                return __('order.pending');
        }
    }
    public function getPaymentNameAttribute()
    {
        switch ($this->payment_method) {
            case self::STATUS_PAYMENT_URWAY:
                return 'Visa';
            case self::STATUS_PAYMENT_CASH:
                return 'Cash';
            default:
                return __('front.visa');
        }
    }

    public function scopeDone(Builder $query): Builder
    {
        return $query->where('is_done', 1);
    }

    public function getDateAttribute()
    {
        return $this->created_at->format('Y-m-d');
    }
}
