<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;
    public $translatedAttributes = [];

    protected $fillable = ['name','email','message' , 'phone_number'];
    public $visibleAttributes = [
        'name' => 'text',
        'email' => 'text',
        'message' => 'text',
        'phone_number' => 'text',
    ];
}
