<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CartItem extends Model
{
    use HasFactory;

    public const STATUS_DELIVERY_FREE = 1;
    public const STATUS_DELIVERY_CHARGED = 0;

    protected $fillable = [
        'equipment_id',
        'cart_id',
        'admin_id',
        'price',
        'qty',
        'book_date',
        'shift_count'
    ];
    protected $hidden = ['id', 'updated_at', 'cart_id', 'admin_id'];
    protected $casts = ['price' => 'double', 'book_date' => 'datetime'];

    public function getPriceAttribute($value): float
    {
        return round($value, 2);
    }

    public function equipment(): BelongsTo
    {
        return $this->belongsTo(Equipment::class);
    }

    public function cart(): BelongsTo
    {
        return $this->belongsTo(Cart::class);
    }
}
