<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'phone_number',
        'company_name',
        'code',
        'tax_number',
        'commercial_registration',
        'image',
        'city_id',
        'admin_id',
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
