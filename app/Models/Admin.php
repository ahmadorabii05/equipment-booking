<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Authenticatable
{
    use HasFactory;
    use Notifiable;
    use HasRoles;
    use HasApiTokens;

    public $translatedAttributes = [];

    public const TYPE_SUPPLIER = 'supplier';
    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'name',
        'email',
        'password',
        'token',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'token',
    ];

    public function details(): HasOne
    {
        return $this->hasOne(AdminDetails::class, 'admin_id');
    }

    public function equipments(): HasMany
    {
        return $this->hasMany(Equipment::class);
    }

    public function city(): HasOne
    {
        return $this->hasOne(City::class, 'city_id');
    }

    public function scopeIsActive(Builder $query): bool
    {
        return $query->active()->role('supplier')->first()->status === self::STATUS_ACTIVE;
    }

    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

}
