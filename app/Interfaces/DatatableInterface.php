<?php

namespace App\Interfaces;


use App\Models\Admin;
use App\Models\Post;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

interface DatatableInterface
{
    public static function initResponse($resource );
}
