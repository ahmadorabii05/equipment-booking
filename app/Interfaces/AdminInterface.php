<?php

namespace App\Interfaces;


use App\Models\Admin;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

interface AdminInterface
{
    public function add(Request $request);
    public function update(Request $request, Admin $admin);
    public function delete(Admin $admin);
    public function getAdmins(Request $request, $withAdvertisements = false): Builder;
    public function getAdminsDataTable(Request $request): LengthAwarePaginator;
}
