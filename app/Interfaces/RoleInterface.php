<?php

namespace App\Interfaces;


use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

interface RoleInterface
{
    public function add(Request $request);
    public function update(Request $request, Role $role);
    public function delete(Role $role);
    public function getRoles();
}
