<?php

namespace App\Repositories;

use App\Http\Traits\MediaTrait;
use App\Models\Admin;
use App\Models\Equipment;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EquipmentRepository
{
    use MediaTrait;
    public function add(Request $request): Equipment
    {
        $equipment = new Equipment(populateModelData($request, Equipment::class));
        $equipment = $this->uploadMedia($request, $equipment);
        $equipment->owner()->associate($request->get('admin_id'));
        $equipment->category()->associate($request->get('category'));
        $equipment->save();
        $equipment->shifts()->attach($request->get('shifts'));
        return $equipment;
    }
    public function update(Request $request, Equipment $equipment): void
    {
        $equipment->update(populateModelData($request, Equipment::class));
        $equipment = $this->updateMedia($request, $equipment);
        $equipment->owner()->associate($request->get('admin_id'));
        $equipment->category()->associate($request->get('category'));
        $equipment->save();
        $equipment->shifts()->sync($request->get('shifts'));
    }
    public function delete(Equipment $equipment): void
    {
        $equipment->delete();
    }
    public function getEquipmentDataTable(Request $request): LengthAwarePaginator
    {
        $equipments = Equipment::query();

        if ($request->has('query')) {
            if (isset($request->get('query')['status']) != null) {
                $equipments->where('status', $request->get('query')['status']);
            }
            if (isset($request->get('query')['search']) != null) {
                $tokens = convertToSeparatedTokens($request->get('query')['search']);
                $equipments->whereRaw("MATCH(description , name) AGAINST(? IN BOOLEAN MODE)", $tokens);
            }
        }
        $userId = Auth::id();
        $user = Admin::query()->where('id', $userId)->role('supplier')->first();
        if ($user) {
            $equipments->where('admin_id', $user->id);
        }
        if ($request->has('sort')) {
            $field = $request->get('sort.field');
            if (!in_array($field, app(Equipment::class)->getFillable(), true)) {
                $field = 'id';
            }
            $equipments = $equipments->orderBy($field, $request->get('sort')['sort'] ?? 'asc')
                ->paginate($request->get('pagination')['perpage'], ['*'], 'page', $request->get('pagination')['page']);
        } else {
            $equipments = $equipments->orderBy('id', 'desc')
                ->paginate($request->get('pagination')['perpage'], ['*'], 'page', $request->get('pagination')['page']);
        }

        return $equipments;
    }
    public function getEquipments(Request $request)
    {
        $equipments = Equipment::active()->orderByDesc('id');

        if ($request->get('admin_id')) {
            $equipments->where('admin_id', $request->get('admin_id'));
        }

        if ($request->get('search')) {
            $tokens = convertToSeparatedTokens($request->get('search'));
            $equipments->whereRaw("MATCH(description , name) AGAINST(? IN BOOLEAN MODE)", $tokens);
        }

        return $equipments;
    }

}
