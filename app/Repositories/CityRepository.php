<?php

namespace App\Repositories;

use App\Models\City;
use Illuminate\Http\Request;

class CityRepository
{

    public function getCitiesDataTable(Request $request)
    {

        $cities = City::query()->with('users')->withCount('users')
            ->withTranslation();

        if (isset($request->get('query')['region'])) {
            $cities->where('region_id', $request->get('query')['region']);
        }

        if (isset($request->get('query')['search'])) {
            $tokens = convertToSeparatedTokens($request->get('query')['search']);
            $cities->whereHas('translations', function ($query) use ($tokens) {
                $query->whereRaw("MATCH(name) AGAINST(? IN BOOLEAN MODE)", $tokens);
            });
        }
        if ($request->has('sort')) {
            $field = $request->get('sort')['field'];
            if (!in_array($field, app(City::class)->getFillable(), true)) {
                $field = 'id';
            }
            $cities = $cities->orderBy($field, $request->get('sort')['sort'] ?? 'asc')
                ->paginate($request->get('pagination')['perpage'], ['*'], 'page', $request->get('pagination')['page']);
        } else {
            $cities = $cities->orderBy('id', 'desc')
                ->paginate($request->get('pagination')['perpage'], ['*'], 'page', $request->get('pagination')['page']);
        }
        return $cities;
    }

    public function citiesAutoComplete($search)
    {
        $tokens = convertToSeparatedTokens($search);

        return City::query()
            ->whereHas('translations', function ($query) use ($tokens) {
                $query->whereRaw("MATCH(name) AGAINST(? IN BOOLEAN MODE)", $tokens);
            })
            ->take(5)
            ->get()->map(function ($result) {
                return array(
                    'id' => $result->id,
                    'text' => $result->name,
                );
            });

    }

}
