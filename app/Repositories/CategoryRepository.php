<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;

class CategoryRepository
{

    public function add(Request $request): Category
    {
        $user = new Category(populateModelData($request, Category ::class));
        $user->image = uploadImage('image', 'user');
        $user->save();
        return $user;
    }

    public function update(Request $request, Category $user)
    {

        $user->update($request->except(['password']));

        $user->save();
    }

    public function delete(Category $category): void
    {
        if ($category->image !== null) {
            $category->image = Storage::disk('public')->delete($category->image);
        }

        $category->delete();
    }

    public function getCategorys(Request $request): Builder
    {
        $users = Category ::query();

        if ($request->has('status') && $request->get('status') != null) {
            $users = $users->where('status', $request->get('status'));
        }
        if ($search = $request->get('search')) {
            $tokens = convertToSeparatedTokens($search);
            $users->whereRaw("MATCH(name) AGAINST(? IN BOOLEAN MODE)", $tokens);
        }

        return $users->orderBy('created_at');
    }

    public function categoryAutoComplete($search)
    {
        $tokens = convertToSeparatedTokens($search);
        return Category ::query()
            ->where('status' , Category::STATUS_ACTIVE)
            ->whereRaw("MATCH(first_name, last_name, email, phone_number) AGAINST(? IN BOOLEAN MODE)", $tokens)
            ->take(5)
            ->get()->map(function ($result) {
                return array(
                    'id' => $result->id,
                    'text' => $result->name . ' (' . $result->email . ')',
                );
            });
    }

    public function getCategoriesDataTable(Request $request): LengthAwarePaginator
    {
        $users = Category ::query();
        if ($request->has('query')) {
            if (isset($request->get('query')['status']) && $request->get('query')['status'] !== null) {
                $users->where('status', $request->get('query')['status']);
            }

            if ($request->get('query.search') !== null) {
                $tokens = convertToSeparatedTokens($request->get('query.search'));
                $users->whereRaw("MATCH(name) AGAINST(? IN BOOLEAN MODE)", $tokens);
            }
        }
        if ($request->has('sort')) {
            $field = $request->get('sort.field');
            if (!in_array($field, app(Category ::class)->getFillable(), true)) {
                $field = 'id';
            }
            $users = $users->orderBy($field, $request->get('sort.sort') ?? 'asc')
                ->paginate($request->get('pagination.perpage'), ['*'], 'page', $request->get('pagination.page'));
        } else {
            $users = $users->orderBy('id', 'desc')
                ->paginate($request->get('pagination.perpage'), ['*'], 'page', $request->get('pagination.page'));
        }

        return $users;
    }

}
