<?php

namespace App\Repositories;

use App\Interfaces\ContactInterface;
use App\Models\Contact;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class ContactRepository
{

    public function delete(Contact $contact)
    {
        $contact->delete();
    }


    public function getContactsDataTable(Request $request): LengthAwarePaginator
    {
        $contacts = Contact::query();


        if ($request->has('sort')) {
            $field = $request->get('sort')['field'];
            if (!in_array($field, app(Contact::class)->getFillable())) $field = 'id';
            $contacts = $contacts->orderBy($field, $request->get('sort')['sort'] ?? 'asc')
                ->paginate($request->get('pagination')['perpage'], ['*'], 'page', $request->get('pagination')['page']);
        } else
            $contacts = $contacts->orderBy('id', 'desc')
                ->paginate($request->get('pagination')['perpage'], ['*'], 'page', $request->get('pagination')['page']);

        return $contacts;
    }

}
