<?php

namespace App\Repositories;

use App\Events\UpdateUserLocationEvent;
use App\Models\City;
use App\Models\User;
use App\Services\GoogleMapService;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UserRepository
{

    public function add(Request $request): User
    {
        $user = new User(populateModelData($request, User::class));
        $user->image = uploadImage('image', 'user');
        $user->save();
        return $user;
    }

    public function update(Request $request, User $user)
    {

        $user->update($request->except(['password']));

        $user->save();
    }

    public function delete(User $user): void
    {
        if ($user->avatar !== null)
            $user->avatar = Storage::disk('public')->delete($user->avatar);

        $user->delete();
    }

    public function getUsers(Request $request, $user = null): Builder
    {
        $users = User::query();

        if ($request->has('status') && $request->get('status') != null) {
            $users = $users->where('status', $request->get('status'));
        }
        if ($search = $request->get('search')) {
            $tokens = convertToSeparatedTokens($search);
            $users->whereRaw("MATCH(name) AGAINST(? IN BOOLEAN MODE)", $tokens);
        }

        return $users->orderBy('created_at');
    }

    public function usersAutoComplete($search)
    {
        $tokens = convertToSeparatedTokens($search);
        return User::query()
            ->where('status' , User::STATUS_ACTIVE)
            ->whereRaw("MATCH(first_name, last_name, email, phone_number) AGAINST(? IN BOOLEAN MODE)", $tokens)
            ->take(5)
            ->get()->map(function ($result) {
                return array(
                    'id' => $result->id,
                    'text' => $result->name . ' (' . $result->email . ')',
                );
            });
    }

    public function getUsersDataTable(Request $request): LengthAwarePaginator
    {
        $users = User::query();
        if ($request->has('query')) {
            if (isset($request->get('query')['status']) && $request->get('query')['status'] !== null) {
                $users->where('status', $request->get('query')['status']);
            }

            if ($request->get('query.search') !== null) {
                $tokens = convertToSeparatedTokens($request->get('query.search'));
                $users->whereRaw("MATCH(first_name, last_name, email, phone_number) AGAINST(? IN BOOLEAN MODE)", $tokens);
            }
        }
        if ($request->has('sort')) {
            $field = $request->get('sort.field');
            if (!in_array($field, app(User::class)->getFillable(), true)) {
                $field = 'id';
            }
            $users = $users->orderBy($field, $request->get('sort.sort') ?? 'asc')
                ->paginate($request->get('pagination.perpage'), ['*'], 'page', $request->get('pagination.page'));
        } else {
            $users = $users->orderBy('id', 'desc')
                ->paginate($request->get('pagination.perpage'), ['*'], 'page', $request->get('pagination.page'));
        }

        return $users;
    }

}
