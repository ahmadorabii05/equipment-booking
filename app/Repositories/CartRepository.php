<?php

namespace App\Repositories;

use App\Http\Traits\CartTrait;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Equipment;
use Carbon\Carbon;

class CartRepository
{
    use CartTrait;

    public function createCart($orderId = null, $status = 0, $supplierId = null): Cart
    {
        return new Cart([
            'user_id' => auth('user')->id(),
            'order_id' => $orderId,
            'status' => $status ?? 0,
            'admin_id' => $supplierId
        ]);
    }

    /**
     * @throws \Exception
     */
    public function getUserCart($userId): Cart
    {
        return Cart::query()->notActive()->with('items')->whereUserId($userId)->firstOrCreate([], [
                'user_id' => $userId,
                'active' => false,
            ]);
    }

    public function createCartItem(Equipment $equipment, $quantity, $cartId, $shiftsCount): CartItem
    {
        return CartItem::create([
            'equipment_id' => $equipment->id,
            'qty' => $quantity,
            'cart_id' => $cartId,
            'price' => $equipment->price * $quantity * $shiftsCount,
            'admin_id' => $equipment->admin_id,
            'shift_count' => $shiftsCount,
            'book_date' => Carbon::now()->format('Y-m-d H:i'),
        ]);
    }

    public function updateCartItem($equipment, $quantity, $cartItem): void
    {
        $cartItem->update([
            'qty' => $quantity,
            'price' => $equipment->price * $quantity,
        ]);
    }
}
