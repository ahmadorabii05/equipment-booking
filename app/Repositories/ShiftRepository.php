<?php

namespace App\Repositories;

use App\Models\Shift;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class ShiftRepository
{

    public function add(Request $request): void
    {
        $shifts = generateDates(
            $request->get('start_date') ,
            $request->get('end_date') ,
            $request->get('count_of_shifts')
        );

        foreach ($shifts as $shift) {
            Shift::create([
               'start_date' => $shift['shift_start'],
               'end_date' => $shift['shift_end'],
               'shift_number' => $shift['shift_number'],
               'shift_count' => $request->get('count_of_shifts'),
            ]);
        }
    }
    public function getShiftsDataTable(Request $request): LengthAwarePaginator
    {
        $users = Shift ::query();
        if ($request->has('sort')) {
            $field = $request->get('sort.field');
            if (!in_array($field, app(Shift ::class)->getFillable(), true)) {
                $field = 'id';
            }
            $users = $users->orderBy($field, $request->get('sort.sort') ?? 'asc')
                ->paginate($request->get('pagination.perpage'), ['*'], 'page', $request->get('pagination.page'));
        } else {
            $users = $users->orderBy('id', 'desc')
                ->paginate($request->get('pagination.perpage'), ['*'], 'page', $request->get('pagination.page'));
        }

        return $users;
    }

    public function getShiftsToShow()
    {
        $startDate = Carbon::now();
        return Shift ::query()->where('start_date' ,'>=' ,$startDate)->active();
    }

}
