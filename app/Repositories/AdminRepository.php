<?php

namespace App\Repositories;

use App\Interfaces\AdminInterface;
use App\Models\Admin;
use App\Models\AdminDetails;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class AdminRepository implements AdminInterface
{

    public function add(Request $request)
    {
        $admin = new Admin(populateModelData($request, Admin::class));
        if ($request->has('password')) {
            $admin->password = Hash::make($request->get('password'));
        }
        $admin->save();
        $admin->roles()->sync($request->get('role'));

        if ($admin->id !== 1) {
            $adminDetails = new AdminDetails(populateModelData($request, AdminDetails::class));
            $adminDetails->admin_id = $admin->id;
            $adminDetails->city_id = $request->get('city_id');
            $adminDetails->image = uploadImage('image', 'admin');
            $adminDetails->save();
        }
        return $admin;
    }

    public function supplierAutoComplete($search)
    {
        $tokens = convertToSeparatedTokens($search);

        return Admin::query()->with('details')
            ->where('status', Admin::STATUS_ACTIVE)
            ->role('supplier')
            ->whereRaw("MATCH(name,email) AGAINST(? IN BOOLEAN MODE)", $tokens)
            ->orWhereHas('details', function ($query) use ($tokens) {
                $query->whereRaw("MATCH(code,phone_number,tax_number,commercial_registration) AGAINST(? IN BOOLEAN MODE)", $tokens);
            })->take(5)
            ->get()->map(function ($result) {
                return array(
                    'id' => $result->id,
                    'text' => $result->name . ' (' . $result->email . ')',
                );
            });
    }

    public function update(Request $request, Admin $admin)
    {
        foreach ($admin->roles()->get() as $role) {
            $admin->removeRole($role);
        }

        $admin->roles()->sync($request->get('role'));

        $admin->update(populateModelData($request, Admin::class));
        if ($request->has('password') && $request->get('password') !== null) {
            $admin->password = Hash::make($request->get('password'));
        }
        $admin->save();

        $data = populateModelData($request, AdminDetails::class);
        if ($admin->id !== 1) {
            $details = AdminDetails::where('admin_id', $admin->id)->first();
            $data['image'] = uploadImage('image', 'admin');
            tap($details)->update($data);
        }
    }

    public function delete(Admin $admin)
    {
        if ($admin->details->image !== null) {
            Storage::disk('public')->delete($admin->details->image);
        }

        $admin->delete();
    }

    public function getAdmins(Request $request, $withAdvertisements = false): Builder
    {

        $admins = Admin::query();

        if ($role = $request->get('role')) {
            $admins = $admins->role($role);
        }

        if ($status = $request->get('status')) {
            $admins = $admins->where('status', $status);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $tokens = convertToSeparatedTokens($search);
            $admins->whereRaw("MATCH(name,email) AGAINST(? IN BOOLEAN MODE)", $tokens)
                ->orWhereHas('details', function ($query) use ($tokens) {
                    $query->whereRaw("MATCH(code,phone_number,tax_number,commercial_registration) AGAINST(? IN BOOLEAN MODE)", $tokens);
                });
        }

        return $admins->orderBy('id');
    }

    public function getAdminsDataTable(Request $request): LengthAwarePaginator
    {
        $admins = Admin::query();

        if ($request->has('query')) {
            if (isset($request->get('query')['status']) != null) {
                $admins->where('status', $request->get('query')['status']);
            }

            if (isset($request->get('query')['role']) != null) {
                $admins = $admins->role($request->get('query')['role']);
            }

            if (isset($request->get('query')['search']) != null) {
                $tokens = convertToSeparatedTokens($request->get('query')['search']);
                $admins->whereRaw("MATCH(name,email) AGAINST(? IN BOOLEAN MODE)", $tokens)
                    ->orWhereHas('details', function ($query) use ($tokens) {
                        $query->whereRaw("MATCH(code,phone_number,tax_number,commercial_registration) AGAINST(? IN BOOLEAN MODE)", $tokens);
                    });
            }
        }

        if ($request->has('sort') && $request->get('sort')['field'] != 'role') {
            $admins = $admins->orderBy($request->get('sort')['field'], $request->get('sort')['sort'] ?? 'asc')
                ->paginate($request->get('pagination')['perpage'], ['*'], 'page', $request->get('pagination')['page']);
        } else {
            $admins = $admins->orderBy('id', 'desc')
                ->paginate($request->get('pagination')['perpage'], ['*'], 'page', $request->get('pagination')['page']);
        }

        return $admins;
    }


}
