<?php


use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Setting;


function setting($key, $type)
{
    if ($type === 'rich_text_box') {
        $type = 'richTextBox';
    }

    $setting = Setting::where('key', $key)->with($type)->first();

    if ($setting !== null && isset($setting->{$type}->value)) {
        return $setting->{$type}->value;
    }

    return '';
}

function getGoogleMapKey(): string
{
    return config('app.google-map');
}


function changeOrderStatus(int $currentStatus, int $newStatus, $order): bool
{

    if (in_array($newStatus, Order::ALLOWED_TRANSITIONS[$currentStatus], true)) {
        $order->update(['status' => $newStatus]);
        return true;
    }
    return false;
}

function generateCode(): string
{
    creation:
    $randomCode = generateRandomString(8);
    if (OrderDetail::where('code', $randomCode)->exists()) {
        goto creation;
    }
    return $randomCode;
}
