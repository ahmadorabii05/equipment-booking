<?php

use App\Models\Category;
use App\Models\City;
use App\Models\Order;
use App\Repositories\ShiftRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;


function localImage($file, $default = '')
{
    if (!empty($file)) {
        return Str::of(Storage::disk('local')->url($file))->replace('storage', 'uploads');
    }

    return $default;
}

function checkIfAdmin($role = 'Admin')
{
    return auth('web')->user()->hasRole($role);
}

function storageImage($file, $default = '')
{
    if (Str::contains($file, 'picsum.photos')) {
        return $file;
    }

    if (!empty($file)) {
        return str_replace('\\', '/', Storage::disk('public')->url($file));
    }
    return $default;
}


function getStatusOrderVariables(): array
{
    $pending = newStd(['name' => __('order.pending'), 'value' => Order::STATUS_PENDING]);
    $delivered = newStd(['name' => __('order.delivered'), 'value' => Order::STATUS_DELIVERED]);
    $ongoing = newStd(['name' => __('order.ongoing'), 'value' => Order::STATUS_ONGOING]);
    $cancelled = newStd(['name' => __('order.cancelled'), 'value' => Order::STATUS_CANCELLED]);
    $refund = newStd(['name' => __('order.refunded'), 'value' => Order::STATUS_REFUNDED]);
    $return = newStd(['name' => __('order.returned'), 'value' => Order::STATUS_RETURNED]);

    return [$pending, $delivered, $ongoing, $cancelled, $refund, $return];
}

function newStd($array = []): stdClass
{
    $std = new \stdClass();
    foreach ($array as $key => $value) {
        $std->$key = $value;
    }
    return $std;
}

function getRoles()
{
    return Role::query()->where('name', '!=', 'partner')->get();
}

function getCites()
{
    return City::query()->get();
}

function getStatusVariables(): array
{
    $active = newStd(['name' => __('admin.active'), 'value' => 1]);
    $inactive = newStd(['name' => __('admin.inactive'), 'value' => 0]);
    return [$active, $inactive];
}

function getSubCategories(): Collection
{
    $categories = Category::query()->withTranslation()->get();
    if ($categories) {
        return $categories;
    }
    return collect();
}
function getShowShifts(): Collection
{
    $shifts = (new ShiftRepository)->getShiftsToShow()->get()->map(function ($shift) {
        return ['name' => Carbon::parse($shift->start_date)->format('Y-m-d'), 'id' => $shift->id];
    })->groupBy('name');
    if ($shifts->count()) {
        return $shifts;
    }
    return collect();
}
function geShifts(): Collection
{
    $shifts = (new ShiftRepository)->getShiftsToShow()->get()->map(function ($shift) {
        return ['name' => Carbon::parse($shift->start_date)->format('Y-m-d H:i'), 'id' => $shift->id];
    });
    if ($shifts->count()) {
        return $shifts;
    }
    return collect();
}

function geAdminShifts(): Collection
{
    $shifts = (new ShiftRepository)->getShiftsToShow()->get();
    if ($shifts->count()) {
        return $shifts;
    }
    return collect();
}

function getCategories(): Collection
{
    $categories = Category::query()->withTranslation()->get();
    if ($categories) {
        return $categories;
    }
    return collect();
}

function generateDates($startDate, $endDate, $shiftsPerDay = 3): array
{
    $start = Carbon::parse($startDate);
    $end = Carbon::parse($endDate);
    $allShifts = [];

    // Define time slots for each shift (example: 8-hour shifts)
    $shiftDuration = 24 / $shiftsPerDay;

    while ($start->lte($end)) {
        for ($i = 0; $i < $shiftsPerDay; $i++) {
            $shiftStart = $start->copy()->addHours($i * $shiftDuration);
            $shiftEnd = $shiftStart->copy()->addHours($shiftDuration)->subMinute();

            $allShifts[] = [
                'date' => $start->toDateString(),
                'shift_number' => $i + 1,
                'shift_start' => $shiftStart->format('Y-m-d H:i'),
                'shift_end' => $shiftEnd->format('Y-m-d H:i')
            ];
        }
        $start->addDay();
    }

    return $allShifts;

}



