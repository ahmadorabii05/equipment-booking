<?php

namespace App\View\Components;


use Illuminate\View\Component;
use Illuminate\View\View;

class ErrorMessage extends Component
{

    /**
     * @var boolean
     */
    public $message ;

    public function __construct($message = '')
    {

        $this->message = $message;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('frontend.components.error-message');
    }
}
