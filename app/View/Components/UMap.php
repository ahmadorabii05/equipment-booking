<?php

namespace App\View\Components;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class UMap extends Component
{
    public $hiddenInputs = false;
    public $longitude;
    public $latitude;

    /**
     * Create a new component instance.
     *
     * @param mixed|null $latitude
     * @param mixed|null $longitude
     * @param bool $hiddenInputs
     */
    public function __construct($latitude = null, $longitude = null, $hiddenInputs = false)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->hiddenInputs = $hiddenInputs;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return Application|Factory|View
     */
    public function render()
    {
        return view('admin.city.u-map');
    }
}
