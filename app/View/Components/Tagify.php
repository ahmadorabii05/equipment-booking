<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Tagify extends BaseComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = '' , $required = false,$oldValue=null)
    {
        parent::__construct($name, $required, $oldValue);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('admin.components.tagify');
    }
}
