<?php

namespace App\View\Components;


use Illuminate\View\View;

class Number extends BaseComponent
{

    /**
     * @var integer
     */
    public $min;

    /**
     * @var integer
     */
    public $max;

    /**
     * @var boolean
     */
    public $decimal;


    /**
     * @var boolean
     */
    public $percentage;


    /**
     * @var boolean
     */
    public $readonly;


    /**
     * @var boolean
     */
    public $label;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param bool $required
     * @param string $locale
     * @param null $oldValue
     * @param int $min
     * @param int $max
     * @param bool $decimal
     * @param bool $percentage
     * @param bool $readonly
     * @param bool $label
     */
    public function __construct($name = '', $required = false, $label = true, $locale = '', $oldValue = null, $min = 0, $max = 999999, $decimal = false, $percentage = false, $readonly = false)
    {
        parent::__construct($name, $required, $locale, $oldValue);
        $this->min = $min;
        $this->max = $max;
        $this->decimal = $decimal;
        $this->readonly = $readonly;
        $this->percentage = $percentage;
        $this->label = $label;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('admin.components.number');
    }
}
