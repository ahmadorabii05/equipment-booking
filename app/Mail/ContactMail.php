<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Log;

class ContactMail extends Mailable
{
    use Queueable;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): ContactMail
    {
        Log::error('hey');
            return $this
            ->subject($this->data['subject'])
            ->with(['data' => $this->data['data']])
            ->markdown('emails.contact');
    }
}
