<?php

namespace App\Observers;

use App\Models\AdminDetails;

class AdminDetailsObserver
{
    /**
     * Handle the AdminDetails "created" event.
     *
     * @param  \App\Models\AdminDetails  $adminDetails
     * @return void
     */
    public function created(AdminDetails $adminDetails)
    {
        //
    }

    /**
     * Handle the AdminDetails "creating" event.
     *
     * @param  \App\Models\AdminDetails  $adminDetails
     * @return void
     */
    public function creating(AdminDetails $adminDetails)
    {
        creation:
        $adminCount = AdminDetails::whereYear('created_at','=', date('Y'))->count();
        $sequenceNumber = 100000 + ++$adminCount;
        $randomCode = generateRandomString(2, true);
        $adminDetailCode = $sequenceNumber."MI-".date('y').$randomCode;

        if(AdminDetails::where('owner_reference', $adminDetailCode)->exists())
            goto creation;
        $adminDetails->owner_reference = $adminDetailCode;
    }

    /**
     * Handle the AdminDetails "updated" event.
     *
     * @param  \App\Models\AdminDetails  $adminDetails
     * @return void
     */
    public function updated(AdminDetails $adminDetails)
    {
        //
    }

    /**
     * Handle the AdminDetails "deleted" event.
     *
     * @param  \App\Models\AdminDetails  $adminDetails
     * @return void
     */
    public function deleted(AdminDetails $adminDetails)
    {
        //
    }

    /**
     * Handle the AdminDetails "restored" event.
     *
     * @param  \App\Models\AdminDetails  $adminDetails
     * @return void
     */
    public function restored(AdminDetails $adminDetails)
    {
        //
    }

    /**
     * Handle the AdminDetails "force deleted" event.
     *
     * @param  \App\Models\AdminDetails  $adminDetails
     * @return void
     */
    public function forceDeleted(AdminDetails $adminDetails)
    {
        //
    }
}
