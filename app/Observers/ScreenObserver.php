<?php

namespace App\Observers;

use App\Models\Screen;

class ScreenObserver
{
    /**
     * Handle the Screen "created" event.
     *
     * @param \App\Models\Screen $screen
     * @return void
     */
    public function created(Screen $screen)
    {
        $screen->owner()->update([
            'screens_count' => $screen->owner()->count() + 1,
        ]);
    }

    /**
     * Handle the Screen "creating" event.
     *
     * @param \App\Models\Screen $screen
     * @return void
     */
    public function creating(Screen $screen)
    {
        creation:
        $screenCount = Screen::whereYear('created_at', '=', date('Y'))->count();
        $sequenceNumber = 100000 + ++$screenCount;
        $randomCode = generateRandomString(2, true);
        $screenDetailCode = $sequenceNumber . "MI-" . date('y') . $randomCode;

        if (Screen::where('screen_reference', $screenDetailCode)->exists())
            goto creation;
        $screen->screen_reference = $screenDetailCode;
    }


    public function updating(Screen $screen)
    {
        if ($screen->isDirty('admin_id')) {
            $screen->owner()->update([
                'screens_count' => $screen->owner()->count() + 1,
            ]);
        } else {
            $screen->owner()->update([
                'screens_count' => $screen->owner()->count()
            ]);
        }
    }


}
