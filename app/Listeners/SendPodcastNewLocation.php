<?php

namespace App\Listeners;

use App\Events\UpdateUserLocationEvent;
use App\Services\PusherService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;
use Pusher\PusherException;

class SendPodcastNewLocation
{
    private const SERVICE_NOTIFY_USER = 'notify-user';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UpdateUserLocationEvent $event
     * @return void
     */
    public function handle(UpdateUserLocationEvent $event): void
    {
        $this->sendDataByPusher($event->receiver);
    }

    private function sendDataByPusher($user): void
    {
        $service = new PusherService($user);
        try {
            $service->pushData();
        } catch (GuzzleException|PusherException $e) {
            Log::error('error pushing notification', (array)$e->getMessage());
        }
    }
}
