import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel([
            'resources/css/app.css',
            'resources/css/default.css',
            'resources/css/default.js',
            'resources/js/app.js',
        ]),
    ],
});
