<?php

namespace Database\Factories;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EquipmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name:en' => $this->faker->name(),
            'name:ar' => $this->faker->name(),
            'description:ar' => $this->faker->sentence(),
            'production_year' => $this->faker->sentence(),
            'reference_name' => $this->faker->sentence(),
            'description:en' => $this->faker->sentence(),
            'price' => random_int(10,1000),
            'count' => random_int(1,10),
            'admin_id' => Admin::role('supplier')->inRandomOrder()->first()->id,
        ];
    }

}
