<?php

namespace Database\Seeders;

use App\Models\Equipment;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(SettingSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(UserSeeder::class);
        Equipment::factory()->count(10)->create();
    }
}
