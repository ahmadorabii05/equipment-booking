<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\AdminDetails;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {

        $admin = Admin::query()->where('email', 'admin@6meem.com')->first();
        $supplier = Admin::query()->where('email', 'supplier@6meem.com')->first();
        if (!$admin) {
            Admin::query()->create([
                'name' => 'Admin',
                'email' => 'admin@6meem.com',
                'status' => 1,
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ]);
        }
        if (!$supplier) {
            Admin::query()->create([
                'name' => 'supplier',
                'email' => 'supplier@6meem.com',
                'status' => 1,
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ]);
        }

       AdminDetails::create([
            'phone_number' => generateRandomString(10),
            'tax_number' => generateRandomString(10),
            'commercial_registration' => generateRandomString(10),
            'code' => generateRandomString(10),
            'admin_id' => $supplier->id ?? null,
        ]);
        $admin = Admin::query()->first();
        $supplier = Admin::query()
            ->where('email', 'supplier@6meem.com')
            ->first();

        $Admin = Role::findOrCreate('Admin');
        $Supplier = Role::findOrCreate('supplier');
        $admin->syncRoles($Admin->id);
        $supplier->syncRoles($Supplier->id);

        foreach (config('permission.permissions') as $permission) {
            Permission::findOrCreate($permission);
            $Admin->givePermissionTo($permission);
        }

        foreach (config('permission.supplier_permissions') as $permission) {
            Permission::findOrCreate($permission);
            $Supplier->givePermissionTo($permission);
        }

    }
}
