<?php

namespace Database\Seeders;

use App\Models\City;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Seeder;


class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = json_decode(file_get_contents(storage_path() . "/jsonfile/cities/cities.json"), true);

        foreach ($array as $key => $value) {
            $lat = $value['center'][0];
            $lng = $value['center'][1];
            $city = City::query()->create([
                'city_id' => $value['city_id'],
                'name:ar' => $value['name_ar'],
                'name:en' => $value['name_en'],
                'longitude' => $lng,
                'latitude' => $lat,
                'region_id' => $value['region_id'] ?? null,
            ]);
        }
    }
}
