<?php

use App\Models\Branch;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', static function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable()->unique();

            $table->boolean('is_paid')->default(false);
            $table->boolean('is_done')->default(false);

            $table->tinyInteger('status')->default(Order::STATUS_PENDING);
            $table->tinyInteger('payment_method')->default(OrderDetail::STATUS_PAYMENT_CASH);

            $table->foreignId('user_id')->constrained()->onDelete('cascade');

            $table->decimal('total_without_vat_price', 10, 2)->nullable();
            $table->decimal('total_vat', 10, 2)->nullable();
            $table->decimal('total_price', 10, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
