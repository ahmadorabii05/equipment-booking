<?php

use App\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', static function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->integer('sort_order')->default(1);
            $table->string('image')->nullable();
            $table->unsignedBigInteger('parent_category')->nullable();
            $table->boolean('status')->default(Category::STATUS_ACTIVE);
            $table->timestamps();
        });
        Schema::table('categories', static function (Blueprint $table) {
            $table->foreign('parent_category')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
