<?php

use App\Models\Admin;
use App\Models\Category;
use App\Models\Equipment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipments', static function (Blueprint $table) {
            $table->id();
            $table->integer('count')->default(1);
            $table->string('production_year');
            $table->string('reference_name');
            $table->longText('images')->nullable();

            $table->double('price' ,10,2);
            $table->double('discount' , 10,2)->default(0);

            $table->tinyInteger('status')->default(Equipment::STATUS_ACTIVE);
            $table->boolean('isPrivate')->default(false);
            $table->boolean('withDriver')->default(false);
            $table->foreignIdFor(Admin::class)->nullable()->constrained()->cascadeOnDelete();
            $table->foreignIdFor(Category::class)->nullable()->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE equipments ADD FULLTEXT idx_full_name (reference_name)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment');
    }
}
