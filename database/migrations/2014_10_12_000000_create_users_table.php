<?php

use App\Models\City;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', static function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('company_name')->nullable();
            $table->string('tax_number')->nullable();
            $table->string('commercial_registration')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('image')->nullable();
            $table->foreignIdFor(City::class)->nullable()->constrained()->onDelete('set null');
            $table->boolean("status")->default(User::STATUS_ACTIVE);

            $table->text('code')->nullable();
            $table->string('token')->nullable();

            $table->timestamp('email_verified_at')->nullable();

            $table->string('reset_token')->nullable();
            $table->enum('reset_verified',['yes','no'])->default('no');
            $table->enum('app_notification_status',['yes','no'])->default('yes');

            $table->rememberToken();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE users ADD FULLTEXT idx_full_name (name, email,commercial_registration,tax_number,company_name,phone_number)');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
