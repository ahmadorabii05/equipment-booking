<?php

use App\Models\Admin;
use App\Models\Equipment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('equipment_translations', static function (Blueprint $table) {
            $table->id(); // Laravel 5.8+ use bigIncrements() instead of increments()
            $table->string('locale')->index();

            // Foreign key to the main model
            $table->foreignIdFor(Equipment::class)->constrained('equipments')->cascadeOnDelete();
            $table->unique(['equipment_id', 'locale']);

            // Actual fields we want to translate
            $table->string("name");
            $table->longText("description");
            $table->timestamps();
        });
        DB::statement('ALTER TABLE equipment_translations ADD FULLTEXT idx_full_name (description , name)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('equipment_translations');
    }
}
