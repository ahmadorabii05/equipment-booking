/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*******************************!*\
  !*** ./resources/js/users.js ***!
  \*******************************/
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return generator._invoke = function (innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; }(innerFn, self, context), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; this._invoke = function (method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); }; } function maybeInvokeDelegate(delegate, context) { var method = delegate.iterator[context.method]; if (undefined === method) { if (context.delegate = null, "throw" === context.method) { if (delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method)) return ContinueSentinel; context.method = "throw", context.arg = new TypeError("The iterator does not provide a 'throw' method"); } return ContinueSentinel; } var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) { if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; } return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, define(Gp, "constructor", GeneratorFunctionPrototype), define(GeneratorFunctionPrototype, "constructor", GeneratorFunction), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (object) { var keys = []; for (var key in object) { keys.push(key); } return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) { "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); } }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, "catch": function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

$('document').ready( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
  var currentURL, regex, match;
  return _regeneratorRuntime().wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          changeStatus();
          currentURL = window.location.href;
          regex = /\/users\/(\d+)/;
          match = currentURL.match(regex);

          if (!window.location.href.includes('create') && !window.location.href.includes('edit') && !(match && match[1])) {
            dataTables();
          }

        case 5:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
})));

function dataTables() {
  var searchParams = new URLSearchParams(window.location.search);
  var table = $('#kt_datatable_users');
  var locale = table.attr('data-locale') === '' ? "en" : table.attr('data-locale');
  var apiUrl = table.attr('data-url');
  var apiStatus = table.attr('data-status');
  var columnsName = {
    'ar': {
      'id': 'ID',
      'first_name': 'الاسم الاول',
      'last_name': 'الاسم الاخير',
      'email': 'ايميل',
      'city': 'المدينة',
      'phone_number': 'رقم الهاتف',
      'created_at': 'تاريخ الانشاء',
      'status': 'الحالة',
      'Actions': 'الاجراءات'
    },
    'en': {
      'id': 'ID',
      'name': 'Name',
      'city': 'City',
      'email': 'Email',
      'phone_number': 'Phone Number',
      'created_at': 'Creation Date',
      'status': 'Status',
      'Actions': 'Actions'
    }
  };
  var datatable = table.KTDatatable({
    // datasource definition
    data: {
      type: 'remote',
      source: {
        read: {
          url: apiUrl,
          // sample custom headers
          headers: {
            'x-my-custom-header': 'some value',
            'x-test-header': 'the value',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          map: function map(raw) {
            // sample data mapping
            var dataSet = raw;

            if (typeof raw.data !== 'undefined') {
              dataSet = raw.data;
            }

            return dataSet;
          },
          params: {
            query: {
              status: searchParams.has('status') ? searchParams.get('status') : null,
              search: searchParams.has('search') ? searchParams.get('search') : null,
              phone_number: searchParams.has('phone_number') ? searchParams.get('phone_number') : null
            }
          }
        }
      },
      pageSize: 10,
      serverPaging: true,
      serverFiltering: true,
      serverSorting: true
    },
    // layout definition
    layout: {
      scroll: false,
      footer: false
    },
    // column sorting
    sortable: true,
    pagination: true,
    // columns definition
    columns: [{
      field: 'id',
      title: columnsName[locale]['id'],
      width: 90,
      textAlign: 'center',
      template: function template(row) {
        return row.image;
      }
    }, {
      field: 'name',
      title: columnsName[locale]['name']
    }, {
      field: 'email',
      title: columnsName[locale]['email']
    }, {
      field: 'city',
      width: 75,
      title: columnsName[locale]['city'],
      template: function template(row) {
        return "<a href=\"".concat(row.city_link, "\" >").concat(row.city, "</a>");
      }
    }, {
      field: 'created_at',
      title: columnsName[locale]['created_at'],
      type: 'date',
      format: 'MM/BB/YYYY'
    }, {
      field: 'status',
      title: columnsName[locale]['status'],
      template: function template(row) {
        var status = {
          0: {
            'title': 'Inactive',
            'class': ' label-light-danger'
          },
          1: {
            'title': 'Active',
            'class': ' label-light-success'
          }
        };

        if (apiStatus !== 'undefined' && apiStatus !== undefined) {
          return status[apiStatus]['title:' + locale];
        }

        var options = ' ';

        for (var key in status) {
          options += '<option value="' + key + '" ' + (row.status == key ? 'selected' : "") + '>' + status[key]['title'] + '</option>\n';
        }

        return ' <span class="text-dark-75 font-weight-bolder d-block font-size-lg">\n' + '         <span class="label label-lg label-inline ' + status[row.status]["class"] + ' mr-2">\n' + '               <select class="btn btn-dropdown dropdown-toggle"\n' + '                    id="statusItem" name="statusItem" data-id="' + row.id + '"\n' + '                    style="width: 100% !important; opacity: 1 !important;">\n' + options + '               </select>\n' + '         </span>\n' + '  </span>';
      }
    }, {
      field: 'Actions',
      title: columnsName[locale]['Actions'],
      sortable: false,
      width: 125,
      overflow: 'visible',
      autoHide: false,
      template: function template(row) {
        return row.actions;
      }
    }]
  });
  var url = new URL(window.location.href);
  $('#kt_datatable_search_status').on('change', function () {
    datatable.search($(this).val().toLowerCase(), 'status');
    url.searchParams.set('status', $(this).val().toLowerCase());
    window.history.replaceState(null, null, url);
  });
  $('#kt_datatable_search_search').on('keyup', function () {
    datatable.search($(this).val().toLowerCase(), 'search');
    url.searchParams.set('search', $(this).val().toLowerCase());
    window.history.replaceState(null, null, url);
  });
  table.on('datatable-on-layout-updated', function () {
    changeStatus(table);
  });
}

function changeStatus(table) {
  $('.change-status').on('click', function () {
    var id = this.getAttribute('data-id');
    var action = this.getAttribute('data-action');
    confirmChangeStatus(id, action, 0, table);
  });
  $('select[name="statusItem"]').on('change', function () {
    var id = this.getAttribute('data-id');
    confirmChangeStatus(id, 'change', this.value, table);
  });
  $('.deleteRow').on('click', function (e) {
    clickLinkConfirm(this, "Are you sure you want to delete this row?");
    e.preventDefault();
  });
  $('.fancybox').fancybox({});
}

function confirmChangeStatus(id) {
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'change';
  var status = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  var table = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

  if (action === 'active') {
    status = 1; //Order::STATUS_ONGOING
  } else if (action === 'inactive') {
    status = 0; //Order::STATUS_CANCELLED
  }

  Swal.fire({
    title: "Confirm!",
    text: "Are you sure you want to " + action + " this order?",
    icon: "warning",
    buttonsStyling: false,
    confirmButtonText: "<i class='la la-thumbs-o-up'></i> Yes " + action + " it!",
    showCancelButton: true,
    cancelButtonText: "<i class='la la-thumbs-down'></i> No, thanks",
    customClass: {
      confirmButton: "btn btn-danger",
      cancelButton: "btn btn-default"
    }
  }).then(function (result) {
    if (result.value) {
      setOrderStatus(id, status, table);
    } else if (result.dismiss === "cancel") {
      Swal.fire("Cancelled", "Your order is safe :)", "error");
    }
  });
}

function setOrderStatus(id, status, table) {
  $.ajax({
    url: '/admin/datatables/setStatus/' + id,
    data: {
      status: status,
      type: 'user'
    },
    success: function success(result) {
      if (table != null) {
        table.reload();
      } else {
        setInterval(function () {
          window.location.reload();
        }, 2000);
      }

      Swal.fire({
        text: result,
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "<i class='la la-thumbs-o-up'></i> OK!",
        showCancelButton: false,
        customClass: {
          confirmButton: "btn btn-danger"
        }
      });
    }
  });
}

function clickLinkConfirm(element, message) {
  Swal.fire({
    title: "Confirm!",
    text: message,
    icon: "warning",
    buttonsStyling: false,
    confirmButtonText: "<i class='la la-thumbs-o-up'></i> Yes !",
    showCancelButton: true,
    cancelButtonText: "<i class='la la-thumbs-down'></i> No",
    customClass: {
      confirmButton: "btn btn-danger",
      cancelButton: "btn btn-default"
    }
  }).then(function (result) {
    if (result.value) {
      $(element).find('form').submit();
    } else if (result.dismiss === "cancel") {
      Swal.fire("The Cancelled Done!", "The item in safe", "error");
    }
  });
}

$(document).ready(function () {
  var $registerForm = $('#__form');

  var _method = $('input[name="_method"]').val();

  var passwordRequired = _method !== 'PUT';

  if ($registerForm.length) {
    $registerForm.validate({
      rules: {
        name: {
          required: true
        },
        email: {
          required: true
        },
        phone_number: {
          required: true
        },
        password: {
          required: passwordRequired
        },
        password_confirmation: {
          required: passwordRequired // equalTo: "#password"

        },
        city_id: {
          required: true,
          validUrl: false,
          url: false
        }
      },
      messages: {
        name: {
          required: "The First Name is required"
        },
        email: {
          required: "The Email Address is required"
        },
        phone_number: {
          required: "The Phone Number is required"
        },
        password: {
          required: "The Password is required"
        }
      }
    });
  }
});
/******/ })()
;