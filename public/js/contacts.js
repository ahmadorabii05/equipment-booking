/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**********************************!*\
  !*** ./resources/js/contacts.js ***!
  \**********************************/
$('document').ready(function () {
  dataTables();
});

function dataTables() {
  var table = $('#kt_datatable_contacts');
  var locale = table.attr('data-locale') === '' ? "en" : table.attr('data-locale');
  var apiUrl = table.attr('data-url');
  var columnsName = {
    'ar': {
      'id': 'ID',
      'name': 'الاسم الاول',
      'email': 'ايميل',
      'message': 'الرسالة',
      'phone_number': 'رقم الهاتف',
      'created_at': 'تاريخ الانشاء',
      'Actions': 'الاجراءات'
    },
    'en': {
      'id': 'ID',
      'name': 'Name',
      'email': 'Email',
      'message': 'Message',
      'phone_number': 'Phone',
      'created_at': 'Creation Date',
      'Actions': 'Actions'
    }
  };
  var datatable = table.KTDatatable({
    // datasource definition
    data: {
      type: 'remote',
      source: {
        read: {
          url: apiUrl,
          // sample custom headers
          headers: {
            'x-my-custom-header': 'some value',
            'x-test-header': 'the value',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          map: function map(raw) {
            // sample data mapping
            var dataSet = raw;

            if (typeof raw.data !== 'undefined') {
              dataSet = raw.data;
            }

            return dataSet;
          }
        }
      },
      pageSize: 10,
      serverPaging: true,
      serverFiltering: true,
      serverSorting: true
    },
    // layout definition
    layout: {
      scroll: false,
      footer: false
    },
    // column sorting
    sortable: true,
    pagination: true,
    // columns definition
    columns: [{
      field: 'id',
      title: columnsName[locale]['id'],
      width: 90,
      textAlign: 'center'
    }, {
      field: 'name',
      title: columnsName[locale]['name']
    }, {
      field: 'phone',
      title: columnsName[locale]['phone_number']
    }, {
      field: 'email',
      title: columnsName[locale]['email']
    }, {
      field: 'message',
      title: columnsName[locale]['message']
    }, {
      field: 'created_at',
      title: columnsName[locale]['created_at'],
      type: 'date',
      format: 'MM/BB/YYYY'
    }, {
      field: 'Actions',
      title: columnsName[locale]['Actions'],
      sortable: false,
      width: 125,
      overflow: 'visible',
      autoHide: false,
      template: function template(row) {
        return row.actions;
      }
    }]
  });
}
/******/ })()
;