"use strict";

// Class definition

var KTWizard2 = function () {
    // Base elements
    let _wizardEl;
    let _formEl;
    let _wizard;
    let _validations = [];
    const locale = document.getElementById('locale').getAttribute('content');
    const messages = {
        'ar': {
            confirmMessage: 'معذرة ، يبدو أنه تم اكتشاف بعض الأخطاء ، يرجى المحاولة مرة أخرى.',
            confirmMessageButtonText: 'تم',
            notEmpty: {
                category_id: 'اختر على ألأقل فئة واحدة',
                number_of_hours: 'ادخل عدد الساعات في الأسبوع',
                start_date: 'ادخل تاريخ ابتداء الحملة آلإعلانية',
                attachment: 'يجب رفع الملف',
                advertiser_id: 'اختر المعلنين المفضلين',
                number_of_min: 'ادخل عدد دقائق الاعلان',
                user_budget: 'ادخل عدد دقائق الاعلان',
            }
        },
        'en': {
            confirmMessage: 'Sorry, looks like there are some errors detected, please try again.',
            confirmMessageButtonText: 'Ok, got it!',
            notEmpty: {
                category_id: 'Select at least one category',
                number_of_hours: 'Enter a number of hours',
                start_date: 'Check advertised Starting',
                attachment: 'The company name (advertiser) required',
                advertiser_id: 'This filed is required',
                number_of_min: 'This filed is required',
                user_budget: 'This filed is required',
            },
        }
    }

    // Private functions
    const initWizard = function () {
        // Initialize form wizard
        _wizard = new KTWizard(_wizardEl, {
            startStep: 1, // initial active step number
            clickableSteps: false // to make steps clickable this set value true and add data-wizard-clickable="true" in HTML for class="wizard" element
        });

        // Validation before going to next page
        _wizard.on('beforeNext', function (wizard) {
            _validations[wizard.getStep() - 1].validate().then(function (status) {
                if (status == 'Valid') {
                    _wizard.goNext();
                    KTUtil.scrollTop();
                } else {
                    Swal.fire({
                        text: messages[locale].confirmMessage,
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: messages[locale].confirmMessageButtonText,
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });
                }
            });

            _wizard.stop();  // Don't go to the next step
        });

        // Change event
        _wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    };

    const initValidation = function () {
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        // Step 1
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    // user_budget: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: messages[locale].notEmpty.number_of_hours
                    //         }
                    //     }
                    // },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    // category_id: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: messages[locale].notEmpty['category_id']
                    //         }
                    //     }
                    // },
                    // advertiser_id: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: messages[locale].notEmpty['category_id']
                    //         }
                    //     }
                    // },
                    // number_of_hours: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: messages[locale].notEmpty.number_of_hours
                    //         }
                    //     }
                    // },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));

        // Step 2
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    // attachment: {
                    //     validators: {
                    //         maxSize: 8048,
                    //         message: messages[locale].notEmpty.attachment,
                    //     },
                    //     notEmpty: {
                    //         message: messages[locale].notEmpty['attachment']
                    //     },
                    // },
                    // start_date: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: messages[locale].notEmpty['start_date']
                    //         },
                    //
                    //     }
                    // },
                    // end_date: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: messages[locale].notEmpty['end_date']
                    //         },
                    //     }
                    // },
                    // number_of_min: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: messages[locale].notEmpty.number_of_min
                    //         },
                    //     }
                    // },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));
    };

    return {
        // public functions
        init: function () {
            _wizardEl = KTUtil.getById('kt_wizard');
            _formEl = KTUtil.getById('kt_form');
            initWizard();
            initValidation();
        }
    };
}();

jQuery(document).ready(function () {
    KTWizard2.init();
});
