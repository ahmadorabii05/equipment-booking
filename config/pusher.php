<?php

return [

    'service' => [
        'app-id' => env('PUSHER_APP_ID'),
        'app-key' => env('PUSHER_APP_KEY'),
        'app-secret' => env('PUSHER_APP_SECRET'),
        'options'=>[
            'cluster' => 'ap2',
            'encrypted' => true
        ]
    ]

];
