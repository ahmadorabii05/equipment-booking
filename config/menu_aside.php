<?php
;
return [

    'items' => [
        [
            'title' => config('app.title'),
            'root' => true,
            'icon' => 'assets/media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => 'admin.dashboard',
            'new-tab' => false,
        ],


        [
            'section' => 'Custom',
        ],
        [
            'title' => 'admin.users_management',
            'icon' => 'assets/media/svg/icons/Communication/Group.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'admin.contractor',
                    'permission' => 'list users',
                    'bullet' => 'dot',
                    'submenu' => [
                        [
                            'title' => 'admin.all_contractor',
                            'page' => 'admin.users.index',
                            'permission' => 'list users',
                        ],
                        [
                            'title' => 'admin.new_contractor',
                            'page' => 'admin.users.create',
                            'permission' => 'add users',
                        ]
                    ]
                ],
                [
                    'title' => 'admin.admins',
                    'permission' => 'list admins',
                    'bullet' => 'dot',
                    'submenu' => [
                        [
                            'title' => 'admin.all_admins',
                            'page' => 'admin.admins.index',
                            'permission' => 'list admins',
                        ],
                        [
                            'title' => 'admin.new_admin',
                            'page' => 'admin.admins.create',
                            'permission' => 'add admins',
                        ]
                    ]
                ],
                [
                    'title' => 'admin.roles',
                    'bullet' => 'dot',
                    'permission' => 'list roles',
                    'submenu' => [
                        [
                            'title' => 'admin.all_roles',
                            'page' => 'admin.roles.index',
                            'permission' => 'list roles',
                        ],
                        [
                            'title' => 'admin.new_role',
                            'page' => 'admin.roles.create',
                            'permission' => 'add roles',
                        ]
                    ]
                ]
            ]
        ],

        [
            'section' => 'Custom',
        ],
        [
            'title' => 'admin.settings',
            'icon' => 'assets/media/svg/icons/Tools/Tools.svg',
            'bullet' => 'line',
            'permission' => 'list settings',

            'root' => true,
            'submenu' => [
                [
                    'title' => 'admin.all_settings',
                    'page' => 'admin.settings.index',
                    'permission' => 'list settings',
                ],
                [
                    'title' => 'admin.new_settings',
                    'page' => 'admin.settings.create',
                    'permission' => 'add settings',
                ]
            ],
        ],

        [
            'section' => 'Custom',
        ],
        [
            'title' => 'admin.categories',
            'icon' => 'assets/media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'permission' => 'list categories',

            'root' => true,
            'submenu' => [
                [
                    'title' => 'admin.all_categories',
                    'page' => 'admin.categories.index',
                    'permission' => 'list categories',
                ],
                [
                    'title' => 'admin.new_categories',
                    'page' => 'admin.categories.create',
                    'permission' => 'add categories',
                ]
            ],
        ],

        [
            'section' => 'Custom',
        ],
        [
            'title' => 'admin.shifts',
            'icon' => 'assets/media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
//            'permission' => 'list shifts',
            'page' => 'admin.shifts.index',
            'root' => true,
        ],

        [
            'section' => 'Custom',
        ],
        [
            'title' => 'admin.cities',
            'icon' => 'assets/media/svg/icons/Map/Marker2.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'admin.all_cities',
                    'page' => 'admin.cities.index',
                    'permission' => 'list cities',
                ],
            ],
        ],

        [
            'section' => 'Custom',
        ],
        [
            'title' => 'admin.equipments',
            'icon' => 'assets/media/svg/icons/Map/Marker2.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'admin.all_equipments',
                    'page' => 'admin.equipment.index',
                    'permission' => 'list equipments',
                ], [
                    'title' => 'admin.new_equipment',
                    'page' => 'admin.equipment.create',
                    'permission' => 'add equipments',
                ]
            ],
        ],
        // Orders
        [
            'section' => 'Custom',
        ],
        [

            'title' => 'admin.orders',
            'icon' => 'assets/media/svg/icons/General/Clipboard.svg',
            'bullet' => 'line',
            'root' => true,
            'permission' => 'list orders',
            'submenu' => [
                [
                    'bullet' => 'dot',
                    'title' => 'admin.all_orders',
                    'page' => 'admin.orders.index',
                    'permission' => 'list orders'
                ],
            ]
        ],
        [
            'section' => 'Custom',
        ],
        [
            'title' => 'admin.contact',
            'icon' => 'assets/media/svg/icons/Text/Align-center.svg',
            'bullet' => 'line',
            'permission' => 'list contacts',
            'page' => 'admin.contacts.index',
        ],

    ],


];
