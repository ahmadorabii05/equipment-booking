<?php

return [
    'please_verify_your_account' => 'Please verify your account',
    'username_or_password_invalid' => 'Email or password is incorrect.',
    'your_profile_updated' => 'Your profile has been updated successfully',
    'your_request_is_registered' => 'Your request is registered.',
];
