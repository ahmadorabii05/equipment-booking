<?php

return [
    'name' => 'Name',
    'email' => 'Email',
    'status' => 'Status',
    'company_name' => 'Company name',
    'commercial_registration' => 'Commercial registration',
    'tax_number' => 'Tax number',
    'city_id' => 'City',
    'phone_number' => 'Phone Number',
    'image' => 'Image',
    'company_id' => 'Company',
];
