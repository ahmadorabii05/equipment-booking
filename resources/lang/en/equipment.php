<?php

return [
    'name' => 'Name',
    'description' => 'Description',
    'admin_id' => 'Supplier',
    'count' => 'Count',
    'price' => 'Price',
    'discount' => 'Discount',
    'status' => 'Status',
    'images' => 'Images',
    'production_year' => 'Production Year',
    'reference_name' => 'Reference Name',
    'category' => 'Category',
    'isPrivate' => 'Is private project',
    'withDriver' => 'With drivers',
    'shifts' => 'Shifts',
];
