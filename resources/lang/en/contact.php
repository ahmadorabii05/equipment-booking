<?php

return [
    'contact_deleted_successfully' => 'Contact deleted successfully',
    'name' => 'Name',
    'message' => 'Message',
    'phone_number' => 'Phone Number',
    'email' => 'Email',
];
