<?php

return [
    'category_id' => 'Category',
    'user_id' => 'User',
    'start_date' => 'Start Date',
    'status' => 'Status',
    'weeks_count' => 'Weeks Count',
    'screen' => 'Screen Type',
];
