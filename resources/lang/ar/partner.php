<?php
    return [
        'name' => 'Name',
        'password' => 'Password',
        'phone_number' => 'Phone Number',
        'email' => 'Email',
        'whatsapp' => 'Whatsapp',
        'status' => 'Status',
        'avatar' => 'Avatar',
    ];
?>
