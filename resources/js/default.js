import Swal from 'sweetalert2';



window.Swal = Swal;


export const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-left',
    showConfirmButton: false,
    timer: 3000,
    background: '#0A66ED',
    timerProgressBar: true,
});

export const statusMessage = (status, message, key = null) => {
    let html = '<spans style="color: #FFFFFF; font-size: 18px;">' + message + '</spans>'
    if (key) {
        html = '<spans style="color: #FFFFFF; font-size: 18px;">' + message[key] + '</spans>'
    }

    Toast.fire({icon: status, html}).then(r =>console.log(r))
}


export const ajaxRequest = (url, data={}, method = 'GET') => {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: method,
            url: url,
            data: data,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        });
    })
}



