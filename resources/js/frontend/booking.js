import {ajaxRequest, statusMessage} from "../default";

$(document).ready(function () {
    $('#reservation_start').on('change', function () {
        const shiftId = $(this).val();
        if (shiftId.length > 0) {
            const url = '/get-shift-times';
            ajaxRequest(url, {id: shiftId}).then((response) => {
                if (response.error) {
                    statusMessage(response.status, response.message);
                } else {
                    if (response.data.length === 0) {
                        statusMessage('error', 'Error there is no shift any more');
                        return
                    }
                    var $firstShiftStart = $('#full_date');
                    var $shiftDetails = $('.time-picker');
                  console.log($firstShiftStart ,$shiftDetails)

                    // Clear existing options
                    $firstShiftStart.empty();
                    $firstShiftStart.append('<option value="" disabled>Select time</option>');

                    // Add new options based on the response data
                    $.each(response.data, function(index, item) {
                        $firstShiftStart.append(`<option value="${item?.id}">${item?.full_date}</option>`);
                    });

                    // Refresh Select2
                    $('.select2').select2({ width: '100%'});
                    // Show the shift details section
                    $shiftDetails.removeClass('d-none');
                }
            }).catch((error) => {
                $('.time-picker').addClass('d-none');
                statusMessage(error.status, error.message);
            })
        }
    })
})
