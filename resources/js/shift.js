import {MarkerClusterer} from "@googlemaps/markerclusterer";
import axios from "axios";

$('document').ready(function () {
    if (!window.location.href.includes('create') && !window.location.href.includes('edit')) {
        if ($('#kt_datatable').length) {
            dataTables();
        }
    }
});
function dataTables() {
    let searchParams = new URLSearchParams(window.location.search);
    let table = $('#kt_datatable');
    let locale = table.attr('data-locale') === '' ? "en" : table.attr('data-locale');
    let apiUrl = table.attr('data-url');
    let apiStatus = table.attr('data-status');
    let columnsName = {
        'ar': {
            'id': 'ID',
            'name': 'الاسم',
            'email': 'أيميل',
            'role': 'النوع',
            'created_at': 'تاريخ الإنشاء',
            'status': 'الحالة',
            'Actions': 'الاجتراءات',
        },
        'en': {
            'id': 'ID',
            'name': 'Start date',
            'email': 'End date',
            'role': 'Role',
            'created_at': 'Creation Date',
            'status': 'Status',
            'Actions': 'Actions',
        },
    };

    let datatable = table.KTDatatable({
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    url: apiUrl,
                    // sample custom headers
                    headers: {
                        'x-my-custom-header': 'some value', 'x-test-header': 'the value',
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    map: function (raw) {
                        // sample data mapping
                        let dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    },
                    params: {
                        query: {
                            status: searchParams.has('status') ? searchParams.get('status') : null,
                            search: searchParams.has('search') ? searchParams.get('search') : null,
                        }
                    },
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,

        },
        // layout definition
        layout: {
            scroll: false,
            footer: false,
        },

        // column sorting
        sortable: true,

        pagination: true,

        // columns definition
        columns: [{
            field: 'id',
            title: columnsName[locale]['id'],
        }, {
            field: 'start_date',
            title: columnsName[locale]['name'],
            type: 'date',
            format: 'MM/BB/YYYY',
        }, {
            field: 'end_date',
            title: columnsName[locale]['email'],
            type: 'date',
            format: 'MM/BB/YYYY',
        }, {
            field: 'Actions',
            title: columnsName[locale]['Actions'],
            sortable: false,
            width: 125,
            overflow: 'visible',
            autoHide: false,
            template: function (row) {
                return row.actions
            },
        }
        ],
    });
    const url = new URL(window.location.href);

    $('#kt_datatable_search_status').on('change', function () {
        datatable.search($(this).val().toLowerCase(), 'status');
        url.searchParams.set('status', $(this).val().toLowerCase());
        window.history.replaceState(null, null, url);
    });


    $('#kt_datatable_search_search').on('keyup', function () {
        datatable.search($(this).val().toLowerCase(), 'search');
        url.searchParams.set('search', $(this).val().toLowerCase());
        window.history.replaceState(null, null, url);
    });


}






