"use strict";

import {statusMessage} from "./default";

$(window).on('load', function (e) {
    const waringMsg = $('#waring_message').val();
    const errorMsg = $('#error_message').val();
    const succeedMsg = $('#succeed_message').val();

    if (errorMsg) {
        statusMessage('error', errorMsg)
    }

    if (waringMsg) {
        statusMessage('warning', waringMsg)
    }

    if (succeedMsg) {
        statusMessage('success', succeedMsg)
    }
})

