import {MarkerClusterer} from "@googlemaps/markerclusterer";
import axios from "axios";

$('document').ready(function () {
    if (!window.location.href.includes('create') && !window.location.href.includes('edit')) {
        if ($('#kt_datatable').length) {
            dataTables();
        }
    }
});
let map;
let markers;
let table;

async function initMap({latitude, longitude, fullMap}) {
    const {Map, InfoWindow} = await google.maps.importLibrary("maps");
    const {AdvancedMarkerElement, PinElement} = await google.maps.importLibrary(
        "marker",
    );
    const lat = latitude !== undefined ? parseFloat(latitude) : 23.8859
    const lng = longitude !== undefined ? parseFloat(longitude) : 45.0792
    const htmlElement = fullMap ? 'full-map-element' : 'map'
    const zoom = fullMap ? 6 : 8
    map = new google.maps.Map(document.getElementById(htmlElement), {
        zoom: zoom,
        center: {lat: lat, lng: lng},
        mapId: "DEMO_MAP_ID",
    });
    const infoWindow = new google.maps.InfoWindow({
        content: "",
        disableAutoPan: true,
    });
    // Create an array of alphabetical characters used to label the markers.
    const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    // Add some markers to the map.
    markers = locations.map((position, i) => {
        const label = position.name !== undefined ? position.name.charAt(0) : labels[i % labels.length];
        const pinGlyph = new google.maps.marker.PinElement({
            glyph: label,
            glyphColor: "white",
        });
        const marker = new google.maps.marker.AdvancedMarkerElement({
            position,
            content: pinGlyph.element,
            title: position.name
        });

        // markers can only be keyboard focusable when they have click listeners
        // open info window when marker is clicked
        marker.addListener("click", () => {
            infoWindow.setContent(position.lat + ", " + position.lng);
            infoWindow.open(map, marker);
        });
        google.maps.event.addListener(marker, 'click', function () {
            window.location.href = '/admin/users/' + position.id;
        });
        return marker;
    });

    // Add a marker clusterer to manage the markers.
    new MarkerClusterer({markers, map});
}

document.querySelector('.full-map').addEventListener('click', async function (e) {
    const response = await axios.get('/admin/locations-users-on-map').then(function (response) {
        locations = [...getUsersLocations(response.data.content)];
    });
    initMap({fullMap: true}).then(() => table.reload())
})

let locations = [];
$('#full-map-modal-lg').on('hidden.bs.modal', function (e) {
    table.reload()
});
const modal = $(`
    <div class="modal fade">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group row">
              <div class="col-lg-12 col-xl-12 ">
                <div class="x_title">
                  <div class="clearfix"></div>
                </div>
                <div id="map" class="gmaps d-flex justify-content-center" style="height: 600px; width: 100%;"></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
`).appendTo("body");

function showMapForData({cityName, latitude, longitude, usersLocation}) {
    modal.find("modal-title").text(cityName);
    modal.modal("show");
    initMap({latitude, longitude, usersLocation, fullMap: false}).then(() => locations = []);
}

function dataTables() {
    let searchParams = new URLSearchParams(window.location.search);
    table = $('#kt_datatable');
    let locale = table.attr('data-locale') === '' ? "en" : table.attr('data-locale');
    let apiUrl = table.attr('data-url');
    let apiStatus = table.attr('data-status');
    let columnsName = {
        'ar': {
            'title': 'العنوان',
            'status': 'الحالة',
            'actions': 'الحالة',
            'location': 'Location On Map',
            'users_count': 'Users in this city',
        },
        'en': {
            'title': 'Name',
            'id': 'ID',
            'status': 'Status',
            'users_count': 'Count of users',
            'actions': 'Status',
            'location': 'Location On Map',
        },
    };

    const tableDataMap = {};

    let datatable = table.KTDatatable({
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    url: apiUrl,
                    // sample custom headers
                    headers: {
                        'x-my-custom-header': 'some value', 'x-test-header': 'the value',
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    map: function (raw) {
                        // sample data mapping
                        let dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                            localStorage.setItem('cities-info', JSON.stringify(raw.data));
                        }
                        return dataSet;
                    },
                    params: {
                        query: {
                            status: searchParams.has('status') ? searchParams.get('status') : null,
                        }
                    },
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,

        },
        // layout definition
        layout: {
            scroll: false,
            footer: false,
        },

        // column sorting
        sortable: true,

        pagination: true,

        // columns definition
        columns: [{
            field: 'id',
            title: columnsName[locale]['id'],
            width: 50,
        }, {

            field: 'name',
            width: 125,
            title: columnsName[locale]['title'],
            template: function (row) {
                return `<a href="${row.center}" target="_blank">${row.name}</a>`;
            }

        }, {
            field: 'users_count',
            title: columnsName[locale]['users_count'],
            width: 130,
        }, {
            field: 'center',
            title: columnsName[locale]['location'],
            sortable: false,
            width: 130,
            overflow: 'visible',
            autoHide: false,
            template: function (row, data, index) {
                tableDataMap[row.id] = row;
                return `<button class="inti-map btn btn-primary" data-id="${row.id}">Map info</button>`;
            },
        }],
    });

    table.on("datatable-on-init", function () {
        $(this).find(".inti-map").on("click", function () {
            const rowDataId = $(this).data("id");
            const rowData = tableDataMap[rowDataId];
            const usersLLocations = getUsersLocations(rowData.users)
            showMapForData({
                cityName: rowData.name,
                latitude: rowData.lat,
                longitude: rowData.lng,
                usersLocation: usersLLocations,
            });

        });
    });

    const url = new URL(window.location.href);
    $('#kt_datatable_search_search').on('keyup', function () {
        datatable.search($(this).val().toLowerCase(), 'search');
        url.searchParams.set('search', $(this).val().toLowerCase());
        window.history.replaceState(null, null, url);
    });
    $('#region').on('change', function () {
        datatable.search($(this).val().toLowerCase(), 'region');
        url.searchParams.set('region', $(this).val().toLowerCase());
        window.history.replaceState(null, null, url);
    });

}

// let locations = [];
function getUsersLocations(users) {
    if (hasData(users)) {
        users.forEach(user => {
            locations.push({
                id: user.id,
                name: user.first_name,
                lat: parseFloat(user.latitude),
                lng: parseFloat(user.longitude)
            });
        });
    }
    return locations;
}

function hasData(obj) {
    return Object.keys(obj).length > 0;
}





