<div class="form-group row col-md-12">
    <label class="col-xl-2 col-lg-2 col-form-label text-right">{{ __('admin.location_on_map') }}</label>
    <div class="col-lg-9 col-xl-9">
        <div class="x_title">
            <div class="clearfix"></div>
        </div>
        <div id="map" class="gmaps" style="height: 500px; width: 82%;" ></div>
        <div id="infowindow-content">
            <span id="place-name"  class="title"></span><br>
            <span id="place-address"></span>
        </div>
    </div>
</div>

