<div class="form-group row">
    <label
        class="col-xl-2 col-lg-2 col-form-label text-right ">{{ __('admin.location_on_map') }}</label>
    <div class="col-lg-9 col-xl-9 ">
        <div class="x_title">
            <div class="clearfix"></div>
        </div>
        <div id="map" class="gmaps d-flex justify-content-center" style="height: 500px; width: 90%; "></div>
        <div id="infowindow-content">
            <span id="place-name" class="title"></span><br>

            <span id="place-address"></span>
        </div>
    </div>
</div>
<div class="form-group row ">
    <div class="col-lg-9 col-xl-9">
        <input type="number" step="any"
               class="form-control"
               placeholder="latitude"
               name="latitude"
               id="latitude"
               hidden
               value="{{ $latitude }}"
        />
    </div>
</div>
<div class="form-group row ">
    <div class="col-lg-9 col-xl-9">
        <input type="number" step="any" class="form-control"
               placeholder="longitude"
               name="longitude"
               id="longitude"
               hidden
               value="{{ $longitude }}"
        />
    </div>
</div>


