<div class="form-group row mb-5">
    <label class="col-xl-2 col-lg-2 col-form-label text-right @if($required) required @endif" >
        {{ isset($item) ? __($item.'.'.$name) : $name }}
    </label>
    <div class="col-lg-9 col-xl-9">
        <div class="checkbox-inline">
            @foreach($choices as $choice)
                <label class="checkbox checkbox-square checkbox-light-success">
                    <input name="{{ $name }}[]" {{ $oldValue ? (in_array($choice->value , $oldValue->{$name}) ? 'checked' : '') :'' }} type="checkbox" value="{{ $choice->value }}" @if($required) required @endif/> {{ $choice->name }}
                    <span></span>
                </label>
            @endforeach
            @error($name)
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>
</div>
