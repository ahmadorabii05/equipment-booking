<x-radio-screen :withLabel="true" :name="'screen'" :choices="$screens"
:oldValue="$entity ?? null"></x-radio-screen>

@foreach($screens as $name)
<div class="attachment_{{$name->id}} d-none">
    <div class="form-group row">
    <label class="col-xl-2 col-lg-2 col-form-label text-right  ">
    {{$name->name .' ( '.$name->width .' X ' .$name->height .' )'}}
    </label>
        <div class="col-lg-4 col-md-11 col-sm-12">
            <div class="">
            <input type="file" class="dropify" name="attachment_{{$name->id}}" value=""
                    data-default-file="">
            </div>
        </div>
    </div>
</div>
@endforeach