<x-text :name="'name'" :locale="$locale" :oldValue="$entity ?? null" :required="true"></x-text>
@if($locale == 'en')
        <x-select displayName="name" :name="'category'" :locale="$locale" :required="true" :options="getCategories()"
                  :oldValue="$entity->category ?? null"></x-select>
        <x-number :name="'sort_order'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-number>
        <x-radio :name="'status'" :withLabel="true" :choices="getStatusVariables()" :oldValue="$entity ?? null"></x-radio>
        <x-image-dropify :name="'images'" :multiple="false" :oldValue="$entity ?? null"></x-image-dropify>
    @endif
<x-slot name="richTextBoxScript"></x-slot>

{{--End Form Inputs--}}
