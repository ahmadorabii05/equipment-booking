<x-master title="{{__('admin.show')}} {{ __('admin.'.$item) }}">

    @section('style')
        <style>
            @media print {
                #header, #navbar, #navbar, #toolbar, #footer, .title, .btn {
                    display: none !important;
                }
            }
        </style>
    @endsection
    <x-breadcrumb :item="$item"></x-breadcrumb>
    <x-slot name="richTextBoxScript"></x-slot>
    @role('Admin')

    <!--begin::Container-->
    <div class="container">
        <!--begin::Content-->
        <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
            <!--begin::Entry-->
            <div class="d-flex flex-column-fluid">
                <!--begin::Container-->
                <div class="container">
                    <!-- begin::Card-->
                    <div class="card card-custom overflow-hidden">
                        <div class="card-body p-0">
                            <!-- begin: Invoice-->
                            @php
                                $shippingOrder = ${$item}->orders->whereNotNull('shipping_details_id')->first()
                            @endphp
                            <!-- begin: Invoice header-->
                            <div class="row justify-content-center bgi-size-cover bgi-no-repeat py-8 px-8 py-md-27 px-md-0"
                                    style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});">
                                <div class="col-md-9">
                                    <div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                                        <h1 class="display-4 text-white font-weight-boldest mb-10">INVOICE</h1>
                                        <div class="d-flex flex-column align-items-md-end px-0">
                                        </div>
                                    </div>
                                    <div class="border-bottom w-100 opacity-20"></div>
                                    <div class="d-flex justify-content-between text-white pt-6">
                                        <div class="d-flex flex-column flex-root">
                                            <span class="font-weight-bolde mb-2r">DATE</span>
                                            <span class="opacity-70">{{${$item}->created_at}}</span>
                                        </div>
                                        <div class="d-flex flex-column flex-root">
                                            <span class="font-weight-bolder mb-2">INVOICE NO.</span>
                                            <span class="opacity-70">{{${$item}->code ?? ''}}</span>
                                        </div>
                                        <div class="d-flex flex-column flex-root">
                                            <span class="font-weight-bolder mb-2">INVOICE TO.</span>
                                            <span class="opacity-70">{{${$item}->user->name ?? ''}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end: Invoice header-->
                            <!-- begin: Invoice body-->
                            <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
                                <div class="col-md-9">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th class="pl-0 font-weight-bold text-muted text-uppercase">
                                                    Suppliers
                                                </th>
                                                <th class="text-right font-weight-bold text-muted text-uppercase">
                                                    Status
                                                </th>
                                                <th class="text-right font-weight-bold text-muted text-uppercase">
                                                    Amount
                                                </th>
                                                <th class="text-right font-weight-bold text-muted text-uppercase">
                                                    Vat 15%
                                                </th>
                                                <th class="text-right pr-0 font-weight-bold text-muted text-uppercase">
                                                    Total Amount
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php $orderDetails = ${$item} @endphp
                                            @foreach( $orderDetails->orders as $order)
                                                <tr class="font-weight-boldest font-size-lg">
                                                    <td class="pl-0 pt-7">{{$order->admin->details->company_name ?? ''}}</td>
                                                    <td class="text-right pt-7">{{$order->statusName ?? ''}}</td>
                                                    <td class="text-right pt-7">{{$order->cart->total_price . ' SAR' ?? ''}}</td>
                                                    <td class="text-right pt-7">{{$order->cart->total_vat . ' SAR' ?? ''}}</td>
                                                    <td class="text-danger pr-0 pt-7 text-right">{{$order->total_price . ' SAR' ?? ''}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- end: Invoice body-->
                            <!-- begin: Invoice footer-->
                            <div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
                                <div class="col-md-9">
                                    <div class="d-flex justify-content-between flex-column flex-md-row font-size-lg">
                                        <div class="d-flex flex-column mb-10 mb-md-0">
                                            <div class="font-weight-bolder font-size-lg mb-3">BANK TRANSFER</div>
                                            <div class="d-flex justify-content-between mb-3">
                                                <span class="mr-15 font-weight-bold">Payment Status</span>
                                                <span class="text-right">{{ $orderDetails->is_paid ? 'Is paid' : 'Is not paid' }}</span>
                                            </div>
                                            <div class="d-flex justify-content-between mb-3">
                                                <span class="mr-15 font-weight-bold">Payment Method</span>
                                                <span class="text-right">{{ $orderDetails->payment_name ?? '' }}</span>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <span class="mr-15 font-weight-bold">Code:</span>
                                                <span class="text-right">{{ $orderDetails->code ?? '' }}</span>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-column text-md-right">
                                            <span class="font-size-lg font-weight-bolder mb-1">TOTAL AMOUNT</span>
                                            <span
                                                    class="font-size-h2 font-weight-boldest text-danger mb-1">{{$orderDetails->total_price . ' SAR'}}</span>
                                            <span>The vat and shipping amount Included</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end: Invoice footer-->
                            <!-- begin: Invoice action-->
                            <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
                                <div class="col-md-9">
                                    <div class="d-flex justify-content-between">
                                        <button type="button" class="btn btn-light-primary font-weight-bold"
                                                onclick="window.print();">Download Invoice
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- end: Invoice action-->
                            <!-- end: Invoice-->
                        </div>
                    </div>
                    <!-- end::Card-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Entry-->
        </div>
        <!--end::Content-->
    </div>
    <!--end::Container-->
    @endrole
    @role('supplier')
    @if(${$item})
        <!--begin::Container-->
        <div class="container">
            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container">
                        <!-- begin::Card-->
                        <div class="card card-custom overflow-hidden">
                            <div class="card-body p-0">
                                <!-- begin: Invoice-->
                                <!-- begin: Invoice header-->
                                <div
                                        class="row justify-content-center bgi-size-cover bgi-no-repeat py-8 px-8 py-md-27 px-md-0"
                                        style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});">
                                    <div class="col-md-9">
                                        <div
                                                class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                                            <h1 class="display-4 text-white font-weight-boldest mb-10">INVOICE</h1>
                                        </div>
                                        <div class="border-bottom w-100 opacity-20"></div>
                                        <div class="d-flex justify-content-between text-white pt-6">
                                            <div class="d-flex flex-column flex-root">
                                                <span class="font-weight-bolde mb-2r">DATE</span>
                                                <span class="opacity-70">{{${$item}->created_at}}</span>
                                            </div>
                                            <div class="d-flex flex-column flex-root">
                                                <span class="font-weight-bolder mb-2">INVOICE NO.</span>
                                                <span class="opacity-70">{{${$item}->code ?? ''}}</span>
                                            </div>
                                            <div class="d-flex flex-column flex-root">
                                                <span class="font-weight-bolder mb-2">INVOICE TO.</span>
                                                <span class="opacity-70">{{${$item}->client->name ?? ''}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end: Invoice header-->
                                <!-- begin: Invoice body-->
                                <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
                                    <div class="col-md-9">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th class="pl-0 font-weight-bold text-muted text-uppercase">
                                                        Name
                                                    </th>
                                                    <th class=" font-weight-bold text-muted text-uppercase">
                                                        Qty
                                                    </th>
                                                    <th class="text-right font-weight-bold text-muted text-uppercase">
                                                        Number of days
                                                    </th>
                                                    <th class="text-right font-weight-bold text-muted text-uppercase">
                                                       Date
                                                    </th>
                                                    <th class="text-right pr-0 font-weight-bold text-muted text-uppercase">
                                                        Price
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($order->items as $product)
                                                    <tr class="font-weight-boldest font-size-lg">
                                                        <td class="pl-0 pt-7">{{$product->name}}</td>
                                                        <td class="pl-0 pt-7">{{$product->qty }}</td>
                                                        <td class="pl-0 pt-7 text-right">{{$product->days_count ?? ''}}</td>
                                                        <td class="pl-0 pt-7 text-right">{{$product->book_date ?? ''}}</td>
                                                        <td class="text-danger  pt-7 text-right">{{$product->price . ' SAR'}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- end: Invoice body-->
                                <!-- begin: Invoice footer-->
                                <div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
                                    <div class="col-md-9">
                                        <div
                                                class="d-flex justify-content-between flex-column flex-md-row font-size-lg">
                                            <div class="d-flex flex-column mb-10 mb-md-0">
                                                <div class="font-weight-bolder font-size-lg mb-3">BANK TRANSFER</div>
                                                <div class="d-flex justify-content-between mb-3">
                                                    <span class="mr-15 font-weight-bold">Payment Status</span>
                                                    <span class="text-right">{{ ${$item}->orderDetails->is_paid ? 'Is paid' : 'Is not paid' }}</span>
                                                </div>
                                                <div class="d-flex justify-content-between mb-3">
                                                    <span class="mr-15 font-weight-bold">Vat 15%</span>
                                                    <span class="text-right">{{ ${$item}->cart->total_vat ?? '' . 'SAR' }}</span>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <span class="mr-15 font-weight-bold">Code:</span>
                                                    <span class="text-right">{{ ${$item}->orderDetails->code ?? '' }}</span>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column text-md-right">
                                                <span class="font-size-lg font-weight-bolder mb-1">TOTAL AMOUNT</span>
                                                <span
                                                        class="font-size-h2 font-weight-boldest text-danger mb-1">{{ $order->total_price }}</span>
                                                <span>The vat and shipping amount Included</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end: Invoice footer-->
                                <!-- begin: Invoice action-->
                                <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
                                    <div class="col-md-9">
                                        <div class="d-flex justify-content-between">
                                            <button type="button" class="btn btn-light-primary font-weight-bold"
                                                    onclick="window.print();">Download Invoice
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- end: Invoice action-->
                                <!-- end: Invoice-->
                            </div>
                        </div>
                        <!-- end::Card-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Container-->
    @endif

    @endrole
</x-master>
