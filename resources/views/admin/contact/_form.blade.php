@if($locale == 'en')
<x-text :name="'title'" :locale="'en'" :oldValue="$entity ?? null"></x-text>
@role('Admin')
<x-select-ajax-data :byClass="false" :displayName="'name'" :name="'owner_id'" url="{{route('admin.adminsAutoComplete')}}" :required="true" :multiple="false" :oldValue="isset($entity) && $entity->owner ? $entity->owner->admin : null"></x-select-ajax-data>
@endrole()
<x-radio :name="'status'" :choices="getStatusVariables()" :oldValue="$entity ?? null"></x-radio>
<x-radio :name="'availability'" :choices="getStatusVariables()" :oldValue="$entity ?? null"></x-radio>
@endif
