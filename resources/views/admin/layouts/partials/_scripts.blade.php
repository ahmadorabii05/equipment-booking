<script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
<script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/tinymce/tinymce.bundle.js') }}"></script>
<!--end::Global Theme Bundle-->
<script src="{{ asset('assets/js/pages/crud/file-upload/image-input.js') }}"></script>
<script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/dropify/dist/js/dropify.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

