@if($locale == 'en')
    <x-number :name="'count_of_shifts'" :oldValue="null" :required="true"></x-number>

    <x-date :name="'start_date'" :locale="''" :oldValue="request()->get('start_date') ?? null"
            :required="true"></x-date>

    <x-date :name="'end_date'" :locale="''" :oldValue="request()->get('start_date') ?? null"
            :required="true"></x-date>
@endif
