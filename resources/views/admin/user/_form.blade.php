{{--Form Inputs--}}
@if($locale == 'en')
    <x-text :name="'name'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-text>

    <x-email :name="'email'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-email>

    <x-text :name="'phone_number'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-text>

    <x-text :name="'company_name'" :locale="''" :oldValue="$entity ?? null" :required="false"></x-text>

    <x-text :name="'commercial_registration'" :locale="''" :oldValue="$entity ?? null" :required="false"></x-text>

    <x-text :name="'tax_number'" :locale="''" :oldValue="$entity ?? null" :required="false"></x-text>

    <x-password :name="'password'" :locale="''" :required="true"></x-password>

    <x-radio :name="'status'" :withLabel="true" :choices="getStatusVariables()" :oldValue="$entity ?? null"></x-radio>

    <x-select-ajax-data :name="'city_id'" url="{{route('admin.citiesAutoComplete')}}"
                        :displayName="'name'" :oldValue="isset($entity) ? $entity->city : null" >
    </x-select-ajax-data>

    <x-image :name="'image'" :locale="''" :oldValue="$entity ?? null"></x-image>

@endif

{{--End Form Inputs--}}
