<script src="{{ asset('custom/js/jquery.validate.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{getGoogleMapKey()}}&loading=async&language=en&callback=initMap&libraries=geometry"></script>
<script src="{{ asset('js/users.js') }}"></script>

