<x-master title="{{__('admin.show')}} {{ __('admin.'.$item) }}">

    @section('breadcrumb')
        @include('admin.layouts.base._breadcrumb', ['singular' => __('admin.'.$item), 'route' =>
        action("App\Http\Controllers\Admin\\".toTitle($item)."Controller@index"), 'plural' => __('admin.'.plural($item))])
    @endsection

    <!--begin::Container-->
    <div class="container">
        <!--begin::Dashboard-->
        <!--begin::Row-->
        <div class="row">
            <div class="col-xl-12">
                <h1 class="mt-10">{{ __('admin.show') }}
                    <code>{{ ${$item}->name }}</code> {{ __('admin.'.$item) }}
                </h1>
                <div class="card card-custom shadow-lg mt-10">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <!--begin::Header-->
                        <!--end::Header-->
                        <div class="card-body px-0">
                            <div class="tab-content pt-5">
                                <div class="tab-pane active">
                                    <!--begin::Tab Content-->
                                    <div class="col-lg-12">
                                        <!--begin::Advance Table Widget 2-->
                                        <div class="card card-custom card-stretch gutter-b">
                                            <!--begin::Body-->
                                            <div class="card-body pt-3 pb-0">
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div
                                                            class="d-flex align-items-center mb-9 bg-light-secondary rounded p-5">
                                                            <div class="d-flex flex-column flex-grow-1 mr-2">
                                                                <a href="#"
                                                                   class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">
                                                                    Name</a>
                                                                <span
                                                                    class="text-muted font-weight-bold user-name" >{{${$item}->name}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div
                                                            class="d-flex align-items-center mb-9 bg-light-secondary rounded p-5">
                                                            <div class="d-flex flex-column flex-grow-1 mr-2">
                                                                <a href="#"
                                                                   class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">Email</a>
                                                                <span
                                                                    class="text-muted font-weight-bold">{{${$item}->email}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--end::Body-->
                                            </div>
                                            <!--end::Advance Table Widget 2-->
                                            <div class="card-body pt-3 pb-0">
                                                <div class="col-md-12 row">
                                                    <h5>Map</h5>
                                                    <div class="card-body">
                                                        <input hidden name="user-id" type="text" value="{{${$item}->id}}" id="user-id">
                                                        <x-map   :longitude="${$item}->longitude ?? null"
                                                                 :latitude="${$item}->latitude ?? null" :hiddenInputs="false">
                                                        </x-map>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Tab Content-->
                                </div>
                                <div class="card-footer">
                                    <button onclick="window.history.back()" type="submit" class="btn btn-primary mr-2">
                                        <i class="ki ki-long-arrow-back icon-sm"></i>{{__('admin.back')}}
                                    </button>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Card-->
                    </div>
                </div>
                <!--end::Row-->
                <!--begin::Row-->
            </div>
        </div>
    </div>
    <!--end::Container-->
    @section('scripts')
            <script src="https://maps.googleapis.com/maps/api/js?key={{getGoogleMapKey()}}&loading=async&language=en&callback=initMap"></script>
            <script src="{{ asset('js/bootstrap.js') }}"></script>
            <script src="{{ asset('js/users.js') }}"></script>
    @endsection
</x-master>
