@if($locale == 'en')

    <x-text :name="'name'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-text>

    <x-text :name="'company_name'" :locale="''" :oldValue="$entity->details ?? null" :required="true"></x-text>

    <x-text :name="'commercial_registration'" :locale="''" :oldValue="$entity->details ?? null" :required="true"></x-text>

    <x-text :name="'tax_number'" :locale="''" :oldValue="$entity->details ?? null" :required="true"></x-text>

    <x-email :name="'email'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-email>

    <x-password :name="'password'" :locale="''" :required="true"></x-password>

    @can('edit roles')
        <x-select :name="'role'" :locale="''" displayName="name" :multiple="false"
                  :options="getRoles()"
                  :oldValue="isset($entity)  &&  $entity->roles()->first() ?
                   $entity->roles()->first()->id :
                    null"></x-select>
    @endcan

    <x-text :name="'phone_number'" :locale="''" :oldValue="$entity->details ?? null" :required="true"></x-text>

    <x-radio :name="'status'" :choices="getStatusVariables()" :oldValue="$entity ?? null"></x-radio>

    <x-select-ajax-data :name="'city_id'" url="{{route('admin.citiesAutoComplete')}}"
                        :displayName="'name'" :oldValue="isset($entity) ? $entity->city : null" >
    </x-select-ajax-data>

    <x-image :name="'image'" :locale="''" :oldValue="$entity->details ?? null"></x-image>
@endif
