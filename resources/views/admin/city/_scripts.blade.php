<script src="{{ asset('custom/js/jquery.validate.min.js') }}"></script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{getGoogleMapKey()}}&loading=async&language=en&callback=initMap"></script>
<script src="{{ asset('js/city.js') }}"></script>

