<!--begin::Advance Table Widget 3-->
<div class="card card-custom gutter-b shadow-lg mt-10">
    <!--begin::Header-->
    <div class="card-header border-0 py-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">{{ __('admin.all_'.plural($item)) }}</span>
        </h3>
        @can('add '.plural($item))
            <div class="card-toolbar">
                <a href="{{ route('admin.'.plural($item).'.create') }}"
                   class="btn btn-primary font-weight-bolder font-size-sm">
                    <i class="fas fa-plus-circle"></i>{{ __('admin.new_'.$item) }}
                </a>
            </div>
        @endcan
        <div class="card-toolbar">
            <button class="btn btn-primary full-map font-weight-bolder font-size-sm"
                    type="button"
                    data-toggle="modal" data-target="#full-map-modal-lg">
                <i class="fas fa-map"></i>Full map
            </button>
        </div>
    </div>
    <!--end::Header-->
    <!--begin::Body-->
    <div class="card-body pt-0 pb-3">
        <!--begin::Table-->
        <div class="">
            <table class="table table-checkable table-head-custom table-head-bg table-borderless table-vertical-center">
                @include('admin.'.$item.'._gridFilter')
                <div class="datatable datatable-bordered datatable-head-custom"
                     id="kt_datatable" data-locale="en" data-url="{{ route('admin.datatables.getCities') }}">
                </div>
            </table>
        </div>
        <!--end::Table-->
    </div>
    <!--end::Body-->
</div>
<!--end::Advance Table Widget 3-->


<!-- Modal -->
<div class="modal fade" id="full-map-modal-lg" tabindex="-1" aria-labelledby="#full-map-modal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="full-map-modal">Full map</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="loader" class="d-none"></div>
                <div id="full-map-element" class="gmaps d-flex justify-content-center" style="height: 500px; width: 100%; "></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
