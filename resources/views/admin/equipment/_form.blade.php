{{--Form Inputs--}}

    @if($locale == 'en')
        <x-text :name="'name'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-text>
        <x-text :name="'production_year'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-text>
        <x-text :name="'reference_name'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-text>
        <x-rich-text-box :name="'description'" :locale="''" :oldValue="$entity ?? null"></x-rich-text-box>
        <x-select-ajax-data :name="'admin_id'" url="{{route('admin.supplierAutoComplete')}}"
                            :displayName="'name'" :oldValue="isset($entity) ? $entity->owner : null" >
        </x-select-ajax-data>
        <x-select displayName="name" :name="'category'" :locale="$locale" :required="true" :options="getSubCategories()" :oldValue="$entity->category ?? $entity->category ?? null"></x-select>
        <x-select displayName="name" :name="'shifts'" :locale="''" :required="true" :multiple="true" :options="geAdminShifts()" :oldValues="$entity->shifts? $entity->shifts()->pluck('shifts.id')->toArray() : []"></x-select>
        <x-number :name="'count'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-number>
        <x-number :name="'price'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-number>
        <x-number :name="'discount'" :locale="''" :oldValue="$entity ?? null" :required="true"></x-number>
        <x-radio :name="'status'" :withLabel="true" :choices="getStatusVariables()" :oldValue="$entity ?? null"></x-radio>
        <x-radio :name="'isPrivate'" :withLabel="true" :choices="getStatusVariables()" :oldValue="$entity ?? null"></x-radio>
        <x-radio :name="'withDriver'" :withLabel="true" :choices="getStatusVariables()" :oldValue="$entity ?? null"></x-radio>
        <x-image-dropify :name="'images'" :multiple="true" :oldValue="$entity ?? null"></x-image-dropify>
    @endif
<x-slot name="richTextBoxScript"></x-slot>

{{--End Form Inputs--}}
