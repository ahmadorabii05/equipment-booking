<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Email From {{ config('app.title') }}</title>
    <style type="text/css" rel="stylesheet" media="all">
        /* Base ------------------------------ */
        *:not(br):not(tr):not(html) {
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        body {
            width: 100% !important;
            height: 100%;
            margin: 0;
            line-height: 1.4;
            background-color: #F5F7F9;
            color: #839197;
            -webkit-text-size-adjust: none;
        }

        a {
            color: #414EF9;
        }

        /* Layout ------------------------------ */
        .email-wrapper {
            width: 100%;
            margin: 0;
            padding: 0;
            background-color: #F5F7F9;
        }

        .email-content {
            width: 100%;
            margin: 0;
            padding: 0;
        }

        /* Masthead ----------------------- */
        .email-masthead {
            padding: 15px 0;
            text-align: center;
            background-color: #0A66ED;
            color: white;
            font-size: 35px;
            font-weight: bold;
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif
        }


        /* Body ------------------------------ */
        .email-body {
            width: 100%;
            margin: 0;
            padding: 0;
            border-top: 1px solid #E7EAEC;
            border-bottom: 1px solid #E7EAEC;
            background-color: #bcbec0;
        }

        .invoice .email-body {
            background-color: white;
            border-top: 3px solid black
        }

        .email-body_inner {
            width: 570px;
            margin: 0 auto;
            padding: 0;
            color: black !important
        }

        .email-footer {
            width: 570px;
            margin: 0 auto;
            padding: 0;
            text-align: center;
        }

        .email-footer p {
            color: #fff;
            background-color: #2b2f3a;
            padding: 10px 1rem;
            /* border-bottom-right-radius: 25px; */
            border-radius: 0px 0px 10px 2px;
            font-weight: bold
        }

        .email-footer p a {
            color: white;
            text-decoration: none
        }

        .body-action {
            width: 100%;
            margin: 30px auto;
            padding: 0;
            text-align: center;
        }

        .body-sub {
            margin-top: 25px;
            padding-top: 25px;
            border-top: 1px solid #E7EAEC;
        }

        .content-cell {
            padding: 35px 0;
            color: black;

        }

        .content-cell h1 {
            font-size: 25px;
            text-transform: uppercase
        }

        .content-cell p {
            color: #292E31;
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;

        }

        .align-right {
            text-align: right;
        }

        /* Type ------------------------------ */
        h1 {
            margin-top: 0;
            color: #292E31;
            font-size: 19px;
            font-weight: bold;
            text-align: left;
        }

        h2 {
            margin-top: 0;
            color: #292E31;
            font-size: 16px;
            font-weight: bold;
            text-align: left;
        }

        h3 {
            margin-top: 0;
            color: #292E31;
            font-size: 14px;
            font-weight: bold;
            text-align: left;
        }

        p {
            margin-top: 0;
            color: #839197;
            font-size: 16px;
            line-height: 1.5em;
            text-align: left;
        }

        p.sub {
            font-size: 12px;
        }

        p.center {
            text-align: center;
        }

        .d-flex {
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap
        }

        .d-flex .qr-code {
            padding-top: 4rem;
        }

        /*Media Queries ------------------------------ */
        @media only screen and (max-width: 600px) {
            .email-body_inner,
            .email-footer {
                width: 100% !important;
            }

            .email-masthead {
                font-size: 25px !important
            }
        }

        @media only screen and (max-width: 450px) {
            .email-masthead {
                font-size: 20px !important
            }
        }
    </style>
</head>
<body>
<table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
    <tr class="d-flex email-masthead" style="justify-content: space-between">
        <td class="ml-4" style="margin-left:20px">
            <img src="{{asset('assets/assets/images/logo/sixmeem-logo.svg')}}" width="150" alt="">
        </td>
    </tr>
    <tr>
        <td align="center">
            <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
                <!-- Logo -->
                <tr>
                    <td class="email-masthead">
                        New Contact Request
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td class="email-body" width="100%">
                        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                            <!-- Body content -->
                            <tr class="d-flex">
                                <td class="content-cell">
                                    <p>Name : {{$data['data']->name ?? ''}} </p>
                                    <p>Email : {{$data['data']->email ?? ''}}</p>
                                    <h2>Message : {{$data['data']->message ?? ''}} </h2>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr></tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
