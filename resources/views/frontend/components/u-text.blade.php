<div class="form-group row" dir="{{ $locale=='ar' ? 'rtl' : 'ltr' }}" >
    <div class="col-lg-12 col-xl-12">
        <input type="text" class="form-control form-control-lg  @error($name)is-invalid @enderror "
               placeholder=" {{  __('admin.'.$name)}}"
               @if($required) required @endif
               name="{{ $name }}"
               id="{{$name}}"
               value="{{ old($name) }}"
            {{ $readonly ? 'readonly' : '' }}
        />
    </div>
   @error($name)
   <div class="invalid-feedback d-block">{{ $message }}</div>
  @enderror
</div>
