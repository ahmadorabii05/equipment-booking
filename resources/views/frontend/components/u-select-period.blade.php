<select class="form-control select2-class w-100" multiple name="full_date" id="full_date[]">
    <option value="" disabled>Select time</option>
    @foreach($times as $item)
        <option value="{{$item['id'] ?? ''}}">
            {{ $item['full_date'] ?? '' }}
        </option>
    @endforeach
</select>
