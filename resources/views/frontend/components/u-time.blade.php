<div class="form-group row" dir="{{ $locale=='ar' ? 'rtl' : '' }}">
    <div class="col-lg-12 col-xl-12">
        <input class="form-control form-control-lg form-control-solid @error($name) is-invalid @enderror"
               name="{{ $name }}"
               value="{{ old($name) }}"
               type="datetime-local" id="{{ $name }}"/>
    </div>
    @error($name)
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>
