<div class="form-group row" dir="{{ $locale=='ar' ? 'rtl' : '' }}">
    <div class="col-lg-12 col-xl-12">
        <input type="password" class="form-control"
               placeholder="{{ __('frontend.enter') }} {{ __('frontend.'.$name) }}"
               name="{{ $name }}"
        />
    </div>
{{--    @error($name)--}}
{{--    <div class="invalid-feedback">{{ $message }}</div>--}}
{{--    @enderror--}}
</div>

<div class="form-group row" dir="{{ $locale=='ar' ? 'rtl' : '' }}">
    <div class="col-lg-12 col-xl-12">
        <input type="password" class="form-control"
               placeholder="{{ __('frontend.enter')}} {{ __('frontend.confirm')}} {{ __('frontend.'.$name) }}"
               name="{{ $name }}_confirmation"
        />
    </div>
</div>
