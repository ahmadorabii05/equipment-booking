<div class="form-group row" dir="{{ $locale=='ar' ? 'rtl' : 'ltr' }}">
    <div class="col-lg-12 col-xl-12">
        @if(count($options) > 0 || $nullable)
            <select class="form-control form-control-lg form-control-solid select2 "
                    id="{{ $name }}" name="{{ $name }}{{$multiple?'[]':''}}"
                    {{ $multiple ? 'multiple="multiple"' : '' }}
                    {{ $required ? 'required' : '' }}
                    dir="{{ $locale=='ar' ? 'rtl' : '' }}"
                    style="width: 100% !important; opacity: 1 !important;">
                @if($all == true)
                    <option {{ $oldValue ? '' : 'selected' }} value="0"> {{ __('admin.all_'.plural($name)) }}</option>
                @endif
                @if($multiple == false)
                    <option value="0" {{!$oldValue ? 'selected' : ''}}> Select data</option>
                    @foreach($options as $option)
                        @php $option = json_decode(json_encode($option)) @endphp
                        <option {{  $oldValue ? (is_object($oldValue) ? ($option->id == $oldValue->id  ? 'selected' : '') : ($option->id == $oldValue ? 'selected' : '')) : (old($name) == $option->id ? 'selected' : '') }} value="{{ $option->id }}">{{ method_exists($option, 'translate') && $option->translate($locale) ? ($option->translate($locale)->$displayName != '' ? $option->translate($locale)->$displayName : $option->$displayName) : $option->$displayName }}</option>
                    @endforeach
                @else
                    @foreach($options as $option)
                        @php $option = json_decode(json_encode($option)) @endphp
                        <option {{ $oldValues ? ( in_array($option->id, $oldValues) ? 'selected' : '') : '' }} value="{{ $option->id }}">{{ method_exists($option, 'translate') && $option->translate($locale) ? ($option->translate($locale)->$displayName ?? $option->$displayName ) : $option->$displayName }}</option>
                    @endforeach
                @endif
            </select>
        @else
                <a class="text-center text-dark-75 font-weight-bolder text-hover-danger mt-3 ml-3 font-size-lg" style="line-height:2.5">{{__('admin.no_records_found')}}</a>
        @endif
    </div>
    @error($name)
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror


</div>
