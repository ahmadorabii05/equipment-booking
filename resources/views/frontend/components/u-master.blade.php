<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.layouts._meta._meta')
</head>
<body>
@include('frontend.layouts._parts._header')
<div class="slot">
    {{$slot}}
</div>

@if (session()->has('error'))
    <input type="hidden" id="error_message" value="{{session()->get('error')}}">
@endif
@if (session()->has('warring'))
    <input type="hidden" id="waring_message" value="{{session()->get('warning')}}">
@endif
@if (session()->has('success'))
    <input type="hidden" id="succeed_message" value="{{session()->get('success')}}">
@endif

@include('frontend.layouts._parts._footer')
@include('frontend.layouts._scripts._scripts')

@yield('scripts')
</body>
</html>
