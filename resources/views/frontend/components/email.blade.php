<div class="form-group row" dir="{{ $locale=='ar' ? 'rtl' : '' }}">
    <div class="col-lg-12 col-xl-12">
        <input type="email" class="form-control  is-invalid "
               placeholder="{{ __('frontend.enter') }} {{ __('frontend.'.$name)}}"
               name="{{ $name }}"
               value="{{ old($name) }}"
        />
    </div>
{{--    @error($name)--}}
{{--    <div class="invalid-feedback">{{ $message }}</div>--}}
{{--    @enderror--}}
</div>



