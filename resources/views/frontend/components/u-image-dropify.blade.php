<div class="form-group row">
    <div class=" col-sm-12">
        <div class="">
            <input type="file" name="{{ $name }}" value="{{$oldValue ? $oldValue->{$name} : ''}}" class="dropify"
                   @if($required) required
                   @endif data-default-file="{{ $oldValue ? storageImage($oldValue->{$name}) : '' }}">
        </div>
    </div>
</div>
