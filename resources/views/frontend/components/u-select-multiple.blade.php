<select class="form-control selectpicker @error($name) is-invalid @enderror" id="{{$name}}" name="{{$name}}[]"
        @if($multiple) multiple="multiple" @endif data-actions-box="true" aria-label="{{__('frontend.'.$name)}}"
    {{ $required ? 'required' : '' }}
>
    <option disabled @if(!$multiple) selected @endif value="0">
        {{__('frontend.select_'.$name)}}
    </option>
    @foreach($options as $option)
        <option value="{{isset($option->id)? $option->id: $option->value}}">
            {{  $option->$displayName  ?? $option->name }}
        </option>
    @endforeach
</select>
@error($name)
<x-error-message :message="$message"/>
@enderror
