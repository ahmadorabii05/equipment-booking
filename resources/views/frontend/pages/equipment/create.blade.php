<x-u-master>

<div class="container mt-5">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Equipments</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create</li>
        </ol>
    </nav>
    <h2>Basic equipment info</h2>
    <br>
    <form>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="type">Type</label>
                <input type="text" class="form-control" id="type">
            </div>
            <div class="form-group col-md-4">
                <label for="productionDate">Production Date</label>
                <input type="text" class="form-control" id="productionDate">
            </div>
            <div class="form-group col-md-4">
                <label for="model">Model</label>
                <input type="text" class="form-control" id="model">
            </div>
        </div>
        <!-- Add other form fields similar to the above structure -->

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="manufacturer">Manufacturer</label>
                <input type="text" class="form-control" id="manufacturer">
            </div>
            <div class="form-group col-md-4">
                <label for="licensePlate">License Plate</label>
                <input type="text" class="form-control" id="licensePlate">
            </div>
            <div class="form-group col-md-4">
                <label for="registrationNumber">Registration Number</label>
                <input type="text" class="form-control" id="registrationNumber">
            </div>
        </div>
        <!-- Continue adding the rest of the fields in a similar manner -->

        <div class="form-row mt-3">
            <div class="col-md-4">
                <div class="upload-box">
                    <img  src="placeholder-image.png" class="img-fluid" alt="Upload">
                </div>
            </div>
            <div class="col-md-4">
                <div class="upload-box">
                    <img  src="placeholder-image.png" class="img-fluid" alt="Upload">
                </div>
            </div>
            <div class="col-md-4">
                <div class="upload-box">
                    <img  src="placeholder-image.png" class="img-fluid" alt="Upload">
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-center">
            <button type="submit" class="btn btn-warning mt-4">Create Equipment</button>
        </div>
    </form>
</div>
</x-u-master>

