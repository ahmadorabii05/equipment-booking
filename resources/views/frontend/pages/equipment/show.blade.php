<x-u-master>
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('user.equipments.index')}}">Equipment</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$equipment->name}}</li>
            </ol>
        </nav>
        <div class="box">
            <div class="row">
                <div class="col-md-6 main-image">
                    <img src="{{storageImage($equipment->images[0] ?? '')}}"
                         height="520" width="300"
                         onerror="this.src='{{asset('assets/assets/images/no-image.jpg')}}';"
                         alt="{{$equipment->name}}">

                    @if($equipment->images && count($equipment->images) > 2)
                        <div class="image-gallery mt-3">
                            @foreach($equipment->images as $image)
                                @if($loop->index !== 0)
                                    <img src="{{storageImage($image)}}" style="height: 100px; width: 100px"
                                         onerror="this.src='{{asset('assets/assets/images/no-image.jpg')}}';"
                                         alt="Thumbnail {{$loop->index}}">
                                @endif
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="col-md-6">
                    <h3>{{$equipment->name}}</h3>
                    <form method="POST" action="{{route('user.cart.addToCart')}}">
                        @csrf
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="reference_name">Reference Name</label>
                                <input type="text" class="form-control"
                                       id="reference_name"
                                       name="reference_name"
                                       value="{{$equipment->reference_name ?? ''}}"
                                       readonly>
                            </div>
                            <div class="form-group col-6">
                                <label for="type">Type</label>
                                <input type="text"
                                       class="form-control"
                                       readonly
                                       id="type"
                                       value="{{$equipment->category->name ?? __('frontend.no_category')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="production_year">Production Year</label>
                                <input type="text" class="form-control"
                                       id="production_year"
                                       name="production_year"
                                       value="{{$equipment->production_year ?? ''}}"
                                       readonly>
                            </div>
                            <input type="hidden" name="equipment_id" id="equipment_id" value="{{$equipment->id}}"/>
                            <div class="form-group col-6">
                                <label for="quantity">Quantity</label>
                                <input type="number" class="form-control" id="quantity" name="quantity" value="1"
                                       max="{{$equipment->count}}"
                                       min="1">
                            </div>
                        </div>
                        @if(count($equipment->shifts) !== 0)
                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="reservation_start">Reservation date</label>
                                    <select class="form-control select2 w-100" multiple name="reservation_start[]"
                                            id="reservation_start">
                                        <option value="" disabled>Select date</option>
                                        @foreach($equipment->shifts as $index => $item)
                                            <option value="{{$item->id ?? '' }}">
                                                {{ $item->date }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row d-none time-picker">
                                <div class="form-group col-12">
                                    <label for="full_date[]">First Shift Start</label>
                                    <select class="form-control select2 w-100" multiple name="full_date[]"
                                            id="full_date">
                                        <option value="" disabled>Select time</option>
                                    </select>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-12 text-center text-danger">
                                    <h3>{{__('frontend.not_available_shifts')}}</h3>
                                </div>
                            </div>
                        @endif

                        <div class="form-check mb-3">
                            <input type="checkbox" {{ $equipment->isPrivate === 1 ? "checked" :'' }}  readonly
                                   class="form-check-input" id="privateProject" name="private">
                            <label class="form-check-label" for="privateProject">Private Project</label>
                        </div>

                        <div class="form-check mb-3">
                            <input type="checkbox" {{ $equipment->withDriver === 1 ? "checked" :'' }} readonly
                                   class="form-check-input" id="withDriver" name="withDriver">
                            <label class="form-check-label" for="withDriver">With driver</label>
                        </div>

                        <button type="submit" class="btn btn-color w-100">Book</button>
                        <div class="d-flex">
                            <p class="mt-3">Cash On Delivery Fees: 00 SR | Shipping Fees: 00 SR | Address: If inside
                                WEWA</p>
                            <p class="mt-3"> Delivered Within: 3 working days | Delivery Note: Order May get delayed</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="related-units mt-5">
            <h4>Related Units</h4>
            <div class="row">
                @foreach($recommendedEquipments as $equipment)
                    <!-- Equipment Item -->
                    <div class="col-md-3 mb-4 box ">
                        <a href="{{route('user.equipments.show' ,$equipment )}}" class="card">
                            <img src="{{storageImage($equipment->images[0] ?? '')}}"
                                 onerror="this.src='{{asset('assets/assets/images/no-image.jpg')}}';"
                                 class="card-img-top" alt="{{$equipment->name}}">
                            <div class="card-body">
                                <h5 class="card-title">{{$equipment->name}}</h5>
                                <p class="card-text">{{$equipment->count}} Unit</p>
                            </div>
                        </a>
                    </div>
                    <!-- Repeat Equipment Item as needed -->
                @endforeach
            </div>

        </div>
    </div>
    @section('scripts')
        <script type="text/javascript" src="{{asset('js/frontend/booking.js')}}"></script>
    @endsection
</x-u-master>
