<x-u-master :title="__('frontend.home')">
    @section('styles')
        <style>

            .card-img-top {
                height: 50px;
                object-fit: cover;
            }
        </style>
    @endsection

    <!-- Main Content -->
    <div class="container my-5">
        <h1 class="mb-4">Equipments</h1>
        <div class="row">
            @foreach($equipments as $equipment)
                <!-- Equipment Item -->
                <div class="col-md-3 mb-4 box">
                    <a href="{{route('user.equipments.show' ,$equipment )}}" class="card">
                        <img src="{{storageImage($equipment->images[0] ?? '')}}"
                             height="250" width="250"
                             onerror="this.src='{{asset('assets/assets/images/no-image.jpg')}}';"
                             class="card-img-top" alt="{{$equipment->name}}">
                        <div class="card-body">
                            <h5 class="card-title">{{$equipment->name}}</h5>
                            <p class="card-text">{{$equipment->count}} Unit</p>
                        </div>
                    </a>
                </div>
                <!-- Repeat Equipment Item as needed -->
            @endforeach
        </div>
        <div class="row mb-4 d-flex justify-content-center">
            {{$equipments->links()}}
        </div>
    </div>
</x-u-master>
