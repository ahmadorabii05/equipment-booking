<x-u-master :title="__('frontend.home')">
    <div class="fst-div">
        <div class="main-img">
            <img width="100%" src="{{asset('assets/assets/images/backgrounds/truckBackground.png')}}" alt="">
            <div class="main-text">
                <h1 class="font-weight-bold text-bold">Emdadat</h1>
                <h1>Heavy Equipment Rental</h1>
                <a href="" class="btn learn_more w-50">Learn more</a>
            </div>
        </div>
        <div class="black-bar">
            <div class="circl-div d-flex align-items-center">
                <div class="circle">
                    <img src="{{asset('assets/assets/images/logo/membership.svg')}}" alt=""/>
                </div>
                <span class="mx-2 text-white">Membership</span>
            </div>
            <div class="circl-div d-flex align-items-center">
                <div class="circle">
                    <img src="{{asset('assets/assets/images/logo/truck.svg')}}" alt=""/>
                </div>
                <span class="mx-2 text-white">Maintenance</span>
            </div>
            <div class="circl-div d-flex align-items-center">
                <div class="circle">
                    <img src="{{asset('assets/assets/images/logo/branches.svg')}}" alt=""/>
                </div>
                <span class="mx-2 text-white">Branches</span>
            </div>
            <div class="circl-div d-flex align-items-center">
                <div class="circle">
                    <img src="{{asset('assets/assets/images/logo/wallet.svg')}}" alt=""/>
                </div>
                <span class="mx-2 text-white">Membership</span>

            </div>
        </div>
    </div>

    @if(count($equipments))
        <div class="container">
            <div class="units mt-5">
                <div class="d-flex justify-content-between">
                    <h4>Units</h4>
                    <p>
                        <a href="{{route('user.equipments.index')}}">
                            Show All
                        </a>
                    </p>
                </div>

                <div class="row">
                    @foreach($equipments as $equipment)
                        <!-- Equipment Item -->
                        <div class="col-md-3 mb-4">
                            <a href="{{route('user.equipments.show' ,$equipment )}}" class="card">
                                <img src="{{storageImage($equipment->images[0] ?? '')}}"
                                     onerror="this.src='{{asset('assets/assets/images/no-image.jpg')}}';"
                                     class="card-img-top" alt="{{$equipment->name}}">
                                <div class="card-body">
                                    <h5 class="card-title">{{$equipment->name}}</h5>
                                    <p class="card-text">{{$equipment->count}} Unit</p>
                                </div>
                            </a>
                        </div>
                        <!-- Repeat Equipment Item as needed -->
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    <div class="container-fluid my-5 ">
        <img src="{{asset('custom/advisor.png')}}" alt="Bulldozer" class="img-fluid w-100">
    </div>
</x-u-master>
