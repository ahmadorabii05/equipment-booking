<x-u-master>
    <div class="fst-div">
        <div class="main-img">
            <img width="100%" src="{{asset('assets/assets/images/backgrounds/truckBackground.png')}}">
            <div class="main-text">
                <h1 class="font-weight-bold text-bold">One Site Page</h1>
                <h1>Lorem Ipsum is simply dummy text</h1>
                <a href="" class="btn learn_more">Learn more</a>
            </div>
        </div>
        <div class="black-bar"  >
            <div class="circl-div d-flex align-items-center">
                <div class="circle">
                    <img src="{{asset('assets/assets/images/logo/membership.svg')}}" />
                </div>
                <span class="mx-2 text-white">Membership</span>
            </div>
            <div class="circl-div d-flex align-items-center">
                <div class="circle">
                    <img src="{{asset('assets/assets/images/logo/truck.svg')}}" />

                </div>
                <span class="mx-2 text-white">Maintenance</span>

            </div>

            <div class="circl-div d-flex align-items-center">
                <div class="circle">
                    <img src="{{asset('assets/assets/images/logo/branches.svg')}}" />
                </div>
                <span class="mx-2 text-white">Branches</span>

            </div>
            <div class="circl-div d-flex align-items-center">
                <div class="circle">
                    <img src="{{asset('assets/assets/images/logo/wallet.svg')}}" />
                </div>
                <span class="mx-2 text-white">Membership</span>

            </div>
        </div>
    </div>
</x-u-master>





