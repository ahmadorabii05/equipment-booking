<x-u-master :title="__('frontend.home')">
    @section('styles')
        <style>
            .text-dark {
                color: black !important;
            }

            .table-bordered {
                border: 1px solid white;
            }

            .table-bordered th, .table-bordered td {
                border: 1px solid white;
            }
        </style>
    @endsection

    <div class="container">
        <h1 class="my-4 text-dark">Your Cart</h1>
        @if ($cart->items->isEmpty())
            <div class="alert alert-info mx-auto d-flex justify-content-center">
                Your cart is empty.
            </div>
            <a href="{{route('user.equipments.index')}}" class="btn btn-primary d-flex justify-content-center">Return back</a>
        @else
            <div class="table-responsive">
                <table class="table table-bordered text-dark">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Equipment</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Days Count</th>
                        <th scope="col">Book Date</th>
                        <th scope="col">Price</th>
                        <th scope="col">Total</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($cart->items as $index => $item)
                        <tr>
                            <th scope="row">{{ $index + 1 }}</th>
                            <td>{{ $item->equipment->name }}</td>
                            <td>{{ $item->qty }}</td>
                            <td>{{ $cart->user->shifts()->wherePivot('equipment_id', $item->equipment_id)->count() . ' '. __('frontend.shifts') }}</td>
                            <td>{{ $item->book_date }}</td>
                            <td>SAR {{ number_format($item->equipment->price, 2) }}</td>
                            <td>SAR {{ number_format($item->price, 2) }}</td>
                            <td>
                                <a href="{{route('user.cart.deleteItem' , ['equipment'=> $item->equipment])}}"
                                   class="btn btn-danger btn-sm">Remove</a>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="6" class="text-right"><strong>Vat 15%</strong></td>
                        <td>SAR {{ number_format($cart->total_vat, 2) }}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="6" class="text-right"><strong>Total Price</strong></td>
                        <td>SAR {{ number_format($cart->total_price, 2) }}</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <a href="{{route('user.checkout.index')}}" class="btn btn-primary">Proceed to Checkout</a>
        @endif
    </div>

</x-u-master>
