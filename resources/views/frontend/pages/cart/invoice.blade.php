<x-u-master>
    <div class="container mt-5">
        <div class="card">
            <div class="card-header bg-primary text-white">
                <h3>Invoice</h3>
            </div>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h5>From:</h5>
                        <address>

                        </address>
                    </div>
                    <div class="col-sm-6 text-right">
                        <h5>To:</h5>
                        <address>
                            <strong>{{ $order->user->name }}</strong><br>
                            Email: {{ $order->user->email }}<br>
                            City: {{ $order->user->city->name }}<br>
                            Phone: {{ $order->user->phone_number  }}
                        </address>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h5>Invoice #{{ $order->code }}</h5>
                    </div>
                    <div class="col-sm-6 text-right">
                        <h5>Date: {{ $order->created_at->format('Y-m-d') }}</h5>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Item</th>
                            <th class="text-right">Quantity</th>
                            <th class="text-right">Price</th>
                            <th class="text-right">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($order->carts as $index => $cart)
                            @foreach($cart->items as $item)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $item->equipment->name }}</td>
                                <td class="text-right">{{ $item->qty }}</td>
                                <td class="text-right">${{ number_format($item->price, 2) }}</td>
                                <td class="text-right">${{ number_format($item->quantity * $item->price, 2) }}</td>
                            </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4" class="text-right">Subtotal</th>
                            <th class="text-right">${{ number_format($order->total_without_vat_price, 2) }}</th>
                        </tr>
                        <tr>
                            <th colspan="4" class="text-right">Tax</th>
                            <th class="text-right">${{ number_format($order->total_vat, 2) }}</th>
                        </tr>
                        <tr>
                            <th colspan="4" class="text-right">Total</th>
                            <th class="text-right">${{ number_format($order->total_price, 2) }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="card-footer bg-primary text-white text-right">
                Thank you for your business!
            </div>
        </div>
    </div>
</x-u-master>
