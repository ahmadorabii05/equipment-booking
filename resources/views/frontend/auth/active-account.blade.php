<x-u-master :title="__('frontend.active_account')">
    <div class="container">
        <div class="row">
            <div class="cart-login">
                <form class="login-form" action="#">
                    <div class="middle-form">
                        <div class="title-login"><span>{{__('frontend.active_your_account')}}</span></div>
                        <div class="d-flex justify-content-center">
                            <a href="#" id="resend-email" class="text-center"
                               data-user="{{session()->get('user')?  session()->get('user')->email : auth('user')->user()->email }}"
                               data-url="{{route('user.resend.email')}}">
                                {{__('frontend.resend_email')}}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @section('scripts')
        <script type="text/javascript">
            $('#resend-email').on('click', (e) => {
                e.preventDefault();

                $.ajax({url: $('#resend-email').data('url'), method: 'GET', data: {email: $('#resend-email').data('user')},})

                    .then(function (data) {
                         if (data.status) {
                             toastr.success(data.message);
                         } else {
                            toastr.error(data.message);
                        }
                })
                    .catch(function (err) {
                    toastr.error(err.message);
                    })
            })
        </script
    @endsection
</x-u-master>
