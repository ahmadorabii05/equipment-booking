<x-u-master :title="__('frontend.login-user')">

    <div class="container mt-5">
        <h2 class="text-orange">{{__('frontend.login-user')}}</h2>
        <form class="login-form box" method="POST" action="{{route('user.login')}}">
            @csrf
            <div class="container w-50">
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email" class="form-label">{{__('frontend.email')}}</label>
                            <input type="email" name="email" class="form-control"
                                   placeholder="&#xf0e0; {{__('frontend.email')}}" required>
                            @error('email')
                            <x-error-message :message="$message"/>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="password" class="form-label">{{__('frontend.password')}}</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                                   placeholder="&#61475; {{__('frontend.password')}}" required>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="form-group mx-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio"
                                   name="type"
                                   value="contractor"
                                   id="type1" {{request()->get('type') === 'contractor' ? 'checked' :''}}>
                            <label class="form-check-label font-size-h3"
                                   for="type1">
                                Contractor
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio"
                                   name="type"
                                   value="supplier"
                                   id="type2" {{request()->get('type') === 'supplier' ? 'checked' :''}}>
                            <label class="form-check-label font-size-h3" for="type2">
                                Supplier
                            </label>
                        </div>
                        <div class="signup-link ">
                            If you don't have account
                            <a class="text-warning signup-link-a" data-toggle="modal"
                               data-target="#purchaseModal">{{__('frontend.register')}}
                            </a>
                            now
                        </div>
                    </div>
                </div>
                <div class="row my-4">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-orange mt-3">{{__('frontend.login-user')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog"
         aria-labelledby="purchaseModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="purchaseModalLabel">Register as</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('user.register')}}" method="GET" id="register-form">
                        <div class="modal-body">
                            <div class="row">
                                <input type="hidden" id="type" name="type" value="">
                                <div class="col-md-6 mt-2 equipment-card">
                                    <div class="equipment-card">
                                        <div class="card">
                                            <img src="{{asset('custom/construction-machine.png')}}" alt="" height="144">
                                            <div class="card-body">
                                                <div class="d-flex justify-content-center">
                                                    <button class="btn btn-orange supplier-btn">Supplier</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2 equipment-card">
                                    <div class="equipment-card">
                                        <div class="card">
                                            <img src="{{asset('custom/loan.png')}}" alt="" height="144">
                                            <div class="card-body">
                                                <div class="d-flex justify-content-center">
                                                    <button class="btn btn-orange contactor-btn">Contractor</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @section('scripts')
        <script>
            const form = $('#register-form')
            const typeInput = $('#type')
            $(document).ready(function () {
                $('#myModal').modal('show');
            });
            $('.contactor-btn').on('click', function (e) {
                e.preventDefault();
                typeInput.val('contractor');
                form.submit()
            })
            $('.supplier-btn').on('click', function (e) {
                e.preventDefault();
                typeInput.val('supplier');
                form.submit()
            })
        </script>
    @endsection
</x-u-master>
