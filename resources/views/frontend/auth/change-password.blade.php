<x-u-master :title="__('frontend.change_password')">
    <div class="container">
        <div class="row">
            <div class="cart-login">
                <form class="login-form" method="POST" action="{{route('user.change.password')}}">
                    @csrf
                    <div class="middle-form">
                        <div class="title-login"><span>{{__('frontend.change_password')}}</span></div>

                        <div class="form-group">

                            <label class="form-label">{{__('frontend.password')}}</label>
                            <input id="password" type="password" placeholder="&#xf023; {{__('frontend.password')}}"
                                   class="form-control @error('password') is-invalid @enderror"
                                   name="password" required autocomplete="new-password">
                            @error('password')
                            <x-error-message :message="$message"/>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="form-label">{{__('frontend.confirm_password')}}</label>
                            <input id="password-confirm" type="password"
                                   placeholder="&#xf023;{{__('frontend.confirm_password')}}" class="form-control"
                                   name="password_confirmation"
                                   required autocomplete="new-password">
                        </div>

                        <input hidden name="user"
                               value="{{ $user->id ??'' }}">

                        <div class="signup-link">{{__('frontend.not_remember')}} <a
                                href="{{route('user.register')}}">{{__('frontend.signup_now')}}</a></div>
                        {{-- <a href="#" type="submit" class="btn" type="submit" value="Login">Login</a> --}}
                        <button class="btn" type="submit"> {{__('frontend.change')}}</button>
                    </div>
                </form>


            </div>
        </div>
    </div>
</x-u-master>
