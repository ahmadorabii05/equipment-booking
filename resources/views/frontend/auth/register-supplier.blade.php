<x-u-master :title="__('frontend.register')">
    <div class="container mt-5">
        <h2 class="text-orange">{{__('frontend.create_new_account_contractor')}}</h2>
        <div class="cart-login w-75 mx-auto mt-5">
            <form class=" box" id="register_form" method="POST"
                  action="{{route('user.register')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="form-label">{{__('frontend.name')}}</label>
                        <input id="name" type="text"
                               class="form-control @error('name') is-invalid @enderror"
                               name="name" value="{{ old('name') }}" required autocomplete="name"
                               placeholder="&#xf007; {{__('frontend.name')}}" autofocus>
                        @error('name')
                        <x-error-message :message="$message"/>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-label">{{__('frontend.tax_number')}}</label>
                        <input id="tax_number" type="text"
                               class="form-control @error('tax_number') is-invalid @enderror"
                               name="tax_number" value="{{ old('tax_number') }}" required autocomplete="tax_number"
                               placeholder="&#xf007; {{__('frontend.tax_number')}}" autofocus>
                        @error('tax_number')
                        <x-error-message :message="$message"/>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6 p-0">
                        <label class="form-label">{{__('frontend.company_name')}}</label>
                        <input id="company_name" type="text"
                               class="form-control @error('company_name') is-invalid @enderror"
                               name="company_name" value="{{ old('company_name') }}" required
                               autocomplete="company_name"
                               placeholder="&#xf007; {{__('frontend.company_name')}}" autofocus>
                        @error('company_name')
                        <x-error-message :message="$message"/>
                        @enderror
                    </div>
                    <div class="form-group col-md-6 position-relative p-0">
                        <label for="password">{{__('frontend.password')}}</label>
                        <input type="password" class="form-control w-75 ml-2
                     @error('password') is-invalid @enderror"
                               id="password" name="password"
                               placeholder="{{__('frontend.password')}}">
                        <button type="button" class="btn btn-secondary mt-3 btn-generate btn-change ">Generate</button>
                    </div>
                </div>
                <div class="form-row row">
                    <div class="form-group col-md-6 p-0">
                        <label class="form-label">{{__('frontend.commercial_registration')}}</label>
                        <input id="commercial_registration" type="text"
                               class="form-control @error('commercial_registration') is-invalid @enderror"
                               name="commercial_registration" value="{{ old('commercial_registration') }}" required
                               autocomplete="commercial_registration"
                               placeholder="&#xf007; {{__('frontend.commercial_registration')}}" autofocus>
                        @error('commercial_registration')
                        <x-error-message :message="$message"/>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-label">{{__('frontend.email')}}</label>
                        <br>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email"
                               placeholder="{{__('frontend.email')}}">
                        @error('email')
                        <x-error-message :message="$message"/>
                        @enderror

                    </div>
                </div>
                <div class="form-row row">
                    <div class="form-group col-md-6 p-0">
                        <label class="form-label">{{__('frontend.phone_number')}}</label>
                        <br>
                        <input type="text" name="phone_number" id="phone_number"
                               placeholder="{{__('frontend.phone_number')}}*"
                               class="form-control w-100 @error('phone_number') is-invalid @enderror"
                               value="{{old('phone_number','+966')}}">
                        @error('phone_number')
                        <x-error-message :message="$message"/>
                        @enderror
                    </div>
                    <div class="form-group col-md-6 p-0">
                        <label class="form-label">{{__('frontend.city')}}</label>
                        <x-u-select-ajax-data :name="'city_id'"
                                              url="{{route('user.citiesAutoComplete')}}"
                                              :displayName="'name'"
                                              :oldValue="null"
                                              :byClass="true">
                        </x-u-select-ajax-data>
                    </div>
                </div>
                <div class="w-75">
                    <input name="type" type="hidden" value="{{request()->get('type' , 'supplier')}}">
                    <label class="form-label">{{__('frontend.file')}}</label>
                    <x-u-image-dropify :name="'images'" :multiple="true" :oldValue="null"></x-u-image-dropify>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="subscribeNewsletter">
                    <label class="form-check-label" for="subscribeNewsletter">
                        Subscribe To Our Newsletter
                    </label>
                    {!! NoCaptcha::display() !!}
                    @error('g-recaptcha-response')
                    <x-error-message :message="$message"/>
                    @enderror
                    <div class="signup-link">{{__('frontend.have_account')}}
                        <a class="signup-link-a text-warning"
                           href="{{route('user.login' ,['type'=> request()->get('type' , 'contactor')])}}">{{__('frontend.login-user')}}</a>
                    </div>
                </div>
                <button type="submit" class="btn btn-orange mt-3">{{__('frontend.register')}}</button>
            </form>
        </div>
    </div>
</x-u-master>
