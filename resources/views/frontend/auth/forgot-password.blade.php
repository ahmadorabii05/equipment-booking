<x-u-master :title="__('frontend.forgot_password')">
    <div class="container">
        <div class="cart-login">
            <form class="login-form" id="register_form" method="POST" action="{{route('user.forgot.password')}}">
                @csrf
                <div class="middle-form">
                    <div class="title-login"><span>{{__('frontend.forget_password')}}</span></div>

                    <div class="form-group">
                        <label class="form-label">{{__('frontend.email')}}</label>
                        <input id="email" type="text"
                               class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email"
                        placeholder="&#xf007; {{__('frontend.email')}}" autofocus>
                        @error('email')
                        <x-error-message :message="$message"/>
                        @enderror
                    </div>
                    <div class="signup-link">{{__('frontend.have_account')}} <a href="{{route('user.login')}}">{{__('frontend.login')}}</a>
                    </div>

                    <div class="form-group button">
                        <button type="submit" class="btn">
                            {{ __('frontend.submit') }}
                        </button>
                    </div>

                </div>
            </form>
        </div>
    </div>
    @section('scripts')
        <script src="{{ asset('custom/js/jquery.validate.min.js') }}"></script>
        <script >
            jQuery(document).ready(function () {
                let $registerForm = $('#register_form');
                if ($registerForm.length) {
                    $registerForm.validate({
                        rules: {
                            'email': {
                                required: true
                            },
                        },
                    });
                }
            });
        </script>
    @endsection
</x-u-master>
