<!-- jQuery -->
<script src="http://thecodeplayer.com/uploads/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" type="text/javescript"></script>

<script src="{{asset('assets/plugins/global/plugins.bundle.js')}}"></script>
<!--Mobile Navbar-->
<script src="{{asset('assets/assets/js/navbar.js')}}"></script>
<!--Bootstrap-->
<script src="{{asset('assets/assets/js/intlTelInput.js')}}"></script>

<script src="{{asset('assets/plugins/custom/dropify/dist/js/dropify.min.js')}}"></script>

<script src="{{asset('assets/assets/js/dropify.js')}}"></script>

<script src="{{ asset('js/default.js') }}"></script>

<script src="{{asset('js/toaster.js')}}"></script>

<script src="{{ asset('js/app.js') }}"></script>
<!--UPLOAD BUTTON-->
<script>
    $('.dropdown-toggle').dropdown()
    $('.fancybox').fancybox({});
    const countryData = window.intlTelInputGlobals.getCountryData();
    const input = document.querySelector("#phone_number")
    const addressDropdown = document.querySelector("#address-country");
    const iti = window.intlTelInput(input, {
        utilsScript: "{{asset('assets/js/utils.js')}}",
    });
</script>
