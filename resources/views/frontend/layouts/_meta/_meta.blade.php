<meta charset="utf-8">
<title>{{$title}}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{csrf_token()}}">
<meta name="locale" id="locale" content="{{app()->getLocale()}}">

<!---------Logo Icon------------->
<link href="{{asset('custom/logo-icon.png')}}" rel="icon" type="image/x-icon">
<!------------------------------->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"/>
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
<link href="{{asset('assets/plugins/custom/leaflet/leaflet.bundle.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/assets/css/dropify.css')}}" />
<link rel="stylesheet" href="{{asset('assets/assets/css/intlTelInput.css')}}">

<!--begin::Global Theme Styles(used by all pages)-->
<!--end::Global Theme Styles-->

<link href="{{asset('assets/assets/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/assets/css/bootstrap.css')}}" rel="stylesheet">
 <link href="{{asset('assets/assets/css/style.css')}}" rel="stylesheet">

<style>
    .error{
        color: #F64E60 !important;
        font-size: 0.85rem !important;
        font-weight: 400 !important;
    }
    .is-invalid{
        border-color:#dc3545 !important;
    }
    .filter-option{
        padding:0 12px !important;
    }
</style>

