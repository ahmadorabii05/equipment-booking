<nav class="navbar navbar-expand-lg navbar-light bg-light">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="d-flex align-items-center">
        @if(!auth('user')->check())
            <div class="navbar-text">
                <a href="{{route('user.login')}}" class="nav-link">{{__('frontend.login-user')}}</a>
            </div>
        @else
            <div class="navbar-text">
                <a href="{{route('user.logout')}}" class="nav-link">{{__('frontend.logout')}}</a>
            </div>
        @endif
        <div class="" id="navbarSupportedContent"> <!-- removed 2 classes: collapse navbar-collapse -->
            <div class="navbar-nav mr-auto">
                <div class="d-flex align-items-center location-dropdown">
                    <i class="fa-solid fa-location-dot"></i>
                    <select class="form-control">
                        <option>Riyadh</option>
                        <option>Jeddah</option>
                        <option>Dammam</option>
                    </select>
                </div>

            </div>

        </div>
        <div class="logo-img d-flex align-items-center">
            <a href="{{route('user.home')}}">
            <img src="{{asset('custom/logo-emdadat.png')}}" alt="logo">
            </a>
        </div>
    </div>
    <div class="links-icons">
        <a href="{{route('user.cart.index')}}">
            <i class="fa-solid fa-bag-shopping"></i>
        </a>
        <a href="#">
            <i class="fa-regular fa-heart"></i>
        </a>
        @if(auth('user')->check())
            <a href="#">
                <i class="fa-regular fa-user"></i>
            </a>
        @endif
        <a href="#">
            <i class="fa-solid fa-magnifying-glass"></i>
        </a>

    </div>
</nav>
