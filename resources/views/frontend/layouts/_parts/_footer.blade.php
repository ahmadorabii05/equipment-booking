<footer class="footer mt-5">
    <div class="row mb-4 newsletter-container">
        <div class="col-md-12 row p-4">
            <div class="col-6 m-auto text-center">
                <h5 class="footer-title uppercase newsletter-title text-warning">SUBSCRIBE TO OUR NEWSLETTER</h5>
            </div>
            <form class="form-inline col-6 mb-3 newsletter">
                <input type="email" class="form-control mb-2 mr-sm-2" placeholder="Your Email Address">
                <button type="submit" class="btn btn-warning mb-2">Submit</button>
            </form>
        </div>
    </div>
    <div class="container">
        <!-- Subscribe to Newsletter -->

        <div class="row">
            <!-- Contact Information -->
            <div class="col-md-3 text-center">
                <h5 class="footer-title">EMDADAT</h5>
                <div class="logo-img d-flex align-items-center text-center mx-auto">
                    <img class="mx-auto text-center w-100" src="{{asset('custom/logo2.jpeg')}}" alt="logo">
                </div>
                <ul class="footer-info">
                    <li>Working hours from 0.00 AM to 0.00 AM, 7 days per week</li>
                    <li><a href="mailto:EM@emdadat.com">EM@emdadat.com</a></li>
                    <li>Hotline 15442</li>
                </ul>
            </div>
            <!-- Shopping & Orders -->
            <div class="col-md-3 text-center">
                <h5 class="footer-title">SHOPPING & ORDERS</h5>
                <ul class="footer-links">
                    <li><a href="#">About US</a></li>
                    <li><a href="#">Privacy</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Shipment Terms</a></li>
                    <li><a href="#">Return & Exchange Policy</a></li>
                    <li><a href="#">Care & Maintenance</a></li>
                </ul>
            </div>
            <!-- Information -->
            <div class="col-md-3 text-center">
                <h5 class="footer-title">INFORMATION</h5>
                <ul class="footer-links">
                    <li><a href="#">About US</a></li>
                    <li><a href="#">Privacy</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Shipment Terms</a></li>
                    <li><a href="#">Return & Exchange Policy</a></li>
                    <li><a href="#">Care & Maintenance</a></li>
                </ul>
            </div>
            <!-- Subscribe to Our Newsletter -->
            <div class="col-md-3 text-center">
                <h5 class="footer-title">SUBSCRIBE TO OUR NEWSLETTER</h5>
                <form class="form-inline newsletter-join d-flex justify-content-between">
                    <input type="email" class="form-control mb-2 mr-sm-2" placeholder="Your Email Address">
                    <button type="submit" class="btn btn-warning mb-2">Submit</button>
                </form>
                <div class="social-icons">
                    <a href="#"><i class="fa-brands fa-facebook-f"></i></a>
                    <a href="#"><i class="fa-brands fa-twitter"></i></a>
                    <a href="#"><i class="fa-brands fa-instagram"></i></a>
                </div>
            </div>
        </div>

    </div>
    <div class="row mt-3 copy-right">
        <div class="col-md-12">
            <p>&copy; COPYRIGHT 2024</p>
        </div>

    </div>
</footer>
