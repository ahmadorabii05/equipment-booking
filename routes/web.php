<?php

use App\Http\Controllers\Frontend\EquipmentController;
use App\Http\Controllers\Frontend\HomeController;
use App\Models\Equipment;
use App\Models\User;
use App\Repositories\CartRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'auth:web'], static function () {
    require_once base_path('routes/admin.php');
});

Route::group(['prefix' => 'user', 'as' => 'user.'], static function () {
    require_once base_path('routes/user.php');
});

Route::get('/change-lang',     [HomeController::class, 'changeLang'])->name('change-lang');

Route::get('/',                [HomeController::class, 'index'])->name('user.home');

Route::get('/get-shift-times', [EquipmentController::class, 'getShiftTimes']);

Route::get('/test', static function () {

    $startDate = '2023-01-01';
    $endDate = '2023-01-07';
    $shiftsPerDay = 3;

    $equipment = Equipment::find(1);
    $user = User::find(1);
    $cart = (new CartRepository())->getUserCart(1);

    $equipmentCartItem = $cart->items()->where('equipment_id', $equipment->id)->first();
    $equipmentShiftIds = $equipment->shifts()->pluck('shifts.id')->toArray();
    $userShiftIds      = $user->shifts()->pluck('shifts.id')->toArray();
    $userEquipmentIds  = $user->shifts()->wherePivot('equipment_id', $equipment->id)->pluck('shifts.id')->toArray();

    return json_decode(json_encode(geAdminShifts()));
    return $equipment->shifts;
    $start = Carbon::parse($startDate);
    $end = Carbon::parse($endDate);
    $allShifts = [];

    // Define time slots for each shift (example: 8-hour shifts)
    $shiftDuration = 24 / $shiftsPerDay;

    while ($start->lte($end)) {
        for ($i = 0; $i < $shiftsPerDay; $i++) {
            $shiftStart = $start->copy()->addHours($i * $shiftDuration);
            $shiftEnd = $shiftStart->copy()->addHours($shiftDuration);

            $allShifts[] = [
                'date' => $start->toDateString(),
                'shift_number' => $i + 1,
                'shift_start' => $shiftStart->format('Y-m-d H:i'),
                'shift_end' => $shiftEnd->format('Y-m-d H:i')
            ];
        }
        $start->addDay();
    }

    return $allShifts;
});

Route::get('new-design-homepage', static function () {
    return view('frontend.pages.new.home');
});

require __DIR__ . '/auth.php';
