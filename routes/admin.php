<?php


use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\EquipmentController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\ShiftController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\RoleController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

// GENERAL & AUTOCOMPLETE ROUTES
Route::get('/',                  [HomeController::class, 'dashboard'])
    ->name('dashboard');

Route::get('/search',            [HomeController::class, 'search'])
    ->name('search');

Route::post('/create-shift',       [ShiftController::class, 'create-shift'])->name('create-shift');

Route::get('/usersAutoComplete',    [UserController::class, 'usersAutoComplete'])
    ->name('usersAutoComplete');

Route::get('/citiesAutoComplete',   [CityController::class, 'citiesAutoComplete'])
    ->name('citiesAutoComplete');

Route::get('/supplierAutoComplete', [AdminController::class, 'supplierAutoComplete'])
    ->name('supplierAutoComplete');

Route::get('remove-images',         [EquipmentController::class, 'removeImage'])->name('removeImage');

Route::prefix('datatables')->name('datatables.')->group(function () {

    Route::post('/getEquipments',   [EquipmentController::class, 'getEquipments'])
        ->name('getEquipments');

    Route::post('/getAdmins',       [AdminController::class, 'getAdmins'])
        ->name('getAdmins');

    Route::post('/getCategories',   [CategoryController::class, 'getCategories'])
        ->name('getCategories');

    Route::post('/getShifts',        [ShiftController::class, 'getShifts'])
        ->name('getShifts');

    Route::post('/getContacts',     [ContactController::class, 'getContacts'])
        ->name('getContacts');

    Route::post('/getUsers',        [UserController::class, 'getUsers'])
        ->name('getUsers');

    Route::post('/getCities',       [CityController::class, 'getCities'])
        ->name('getCities');

    Route::GET('/setStatus/{order}',[AdminController::class, 'setStatus'])
        ->name('setStatus');
});

Route::prefix('orders')->name('orders.')->group(function (){
    Route::POST('/getOrders',            [OrderController::class, 'getOrders'])->name('getOrders');
    Route::GET('/',                      [OrderController::class, 'index'])->name('index');
    Route::GET('/{order}',               [OrderController::class, 'show'])->name('show');
    Route::GET('/setOrderStatus/{id}',   [OrderController::class, 'setOrderStatus'])->name('setOrderStatus');
    Route::GET('/setOrderBranch/{order}',[OrderController::class, 'setOrderBranch'])->name('setOrderBranch');
});

Route::resource('settings',       SettingController::class);
Route::resource('categories',     CategoryController::class);
Route::resource('users',          UserController::class);
Route::resource('orders',         OrderController::class);
Route::resource('shifts',         ShiftController::class);
Route::resource('admins',         AdminController::class);
Route::resource('cities',         CityController::class);
Route::resource('roles',          RoleController::class);
Route::resource('equipment',      EquipmentController::class);
Route::resource('contacts',       ContactController::class);

