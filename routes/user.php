<?php


use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\CheckoutController;
use App\Http\Controllers\Frontend\EquipmentController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\OrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
|
| Here is where you can register user routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "user" middleware group. Now create something great!
|
*/
// GENERAL
Route::get('/citiesAutoComplete', [HomeController::class, 'citiesAutoComplete'])->name('citiesAutoComplete');


Route::group(['middleware' => 'auth:user'], static function () {
    Route::group(['prefix' => 'equipments', 'as' => 'equipments.'], static function () {
        Route::get('/',             [EquipmentController::class, 'index'])->name('index');
        Route::get('/create',       [EquipmentController::class, 'create'])->name('create');
        Route::get('/{equipment}' , [EquipmentController::class, 'show'])->name('show');
    });

    Route::group(['prefix' => 'cart', 'as' => 'cart.'], static function () {
        Route::post('/',                      [CartController::class, 'addToCart'])->name('addToCart');
        Route::get('/',                       [CartController::class, 'index'])->name('index');
        Route::get('/deleteItem/{equipment}', [CartController::class, 'deleteItem'])->name('deleteItem');
    });

    Route::group(['prefix' => 'checkout', 'as' => 'checkout.'], static function () {
        Route::get('/',  [CheckoutController::class, 'proceedToCheckout'])->name('index');
        Route::get('/completeCheckoutAction',  [CheckoutController::class, 'completeCheckoutAction'])->name('completeCheckoutAction');
        Route::get('/completeToCheckout',  [CheckoutController::class, 'completeCheckoutAction'])->name('completeToCheckout');
    });
    Route::get('/invoice/{orderId}', [OrderController::class, 'showInvoice'])->name('invoice.show');
});




