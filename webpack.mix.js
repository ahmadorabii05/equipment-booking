const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/default.js', 'public/js/default.js')
    .postCss('resources/css/app.css', 'public/css/app.css')
    .sass('public/assets/assets/css/style.scss', 'public/assets/assets/css/style.css')
    .postCss('resources/css/default.css', 'public/css/default.css', [
        require('postcss-import'),
        require('tailwindcss'),
        require('autoprefixer'),
    ]).js('resources/js/admins.js', 'public/js/admins.js')
    .js('resources/js/city.js', 'public/js/city.js')
    .js('resources/js/company.js', 'public/js/company.js')
    .js('resources/js/toaster.js', 'public/js/toaster.js')
    .js('resources/js/users.js', 'public/js/users.js')
    .js('resources/js/order.js', 'public/js/order.js')
    .js('resources/js/category.js', 'public/js/category.js')
    .js('resources/js/contacts.js', 'public/js/contacts.js')
    .js('resources/js/shift.js', 'public/js/shift.js')
    .js('resources/js/frontend/booking.js', 'public/js/frontend/booking.js')
    .js('resources/js/bootstrap.js', 'public/js/bootstrap.js');
